package stateMachine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.mygdx.game.Game;

import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import graphic.AtlasManager;
import music.BackgroundMusic;
import music.SoundEffect;
import userInterface.gameScreens.BuySkill;
import userInterface.gameScreens.Fly;
import userInterface.gameScreens.InPlay;
import userInterface.gameScreens.Prediction;
import userInterface.gameScreens.UseSkill;
import userInterface.menus.Credits;
import userInterface.menus.Help;
import userInterface.menus.LoadGame;
import userInterface.menus.MainMenu;
import userInterface.menus.Options;
import userInterface.menus.SaveGame;

public enum GameState implements State<Game> {
    BUY_SKILL {
        private BuySkill buySkill;

        @Override
        public void enter(Game entity) {
            if (entity.getBuySkill() == null) {
                buySkill = new BuySkill(entity);
                entity.setBuySkill(buySkill);
            } else {
                buySkill = entity.getBuySkill();
                buySkill.updateInfoInViews();
                Gdx.input.setInputProcessor(buySkill.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            buySkill.render();
        }

        @Override
        public void exit(Game entity) {
//            buySkill.dispose();
        }
    },
    IN_PLAY {
        private InPlay inPlay;

        @Override
        public void enter(Game entity) {
            if (entity.getInPlay() == null) {
                inPlay = new InPlay(entity);
                entity.setInPlay(inPlay);
            } else {
                inPlay.getDraw().createBatch();
                inPlay = entity.getInPlay();
                inPlay.updateInfoInViews();
                inPlay.setInputProcessor();
            }
            BackgroundMusic.getInstance().initializeSong(Const.zone(PlayerData.getInstance().getCurrentFloor()));
        }

        @Override
        public void update(Game entity) {
            inPlay.render();
        }

        @Override
        public void exit(Game entity) {
//            inPlay.dispose();
            inPlay.getDraw().dispose();
        }
    },
    LOAD_GAME {
        private LoadGame loadGame;

        @Override
        public void enter(Game entity) {
            if (entity.getLoadGame() == null) {
                loadGame = new LoadGame(entity);
                entity.setLoadGame(loadGame);
            } else {
                loadGame = entity.getLoadGame();
                loadGame.updateInfoInViews();
                Gdx.input.setInputProcessor(loadGame.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            loadGame.render();
        }

        @Override
        public void exit(Game entity) {
//            loadGame.dispose();
        }
    },
    MAIN_MENU {

        private MainMenu mainMenu;

        @Override
        public void enter(Game entity) {
            BackgroundMusic.getInstance().initializeSong(0);
            SoundEffect.getInstance();

            if (entity.getMainMenu() == null) {
                AtlasManager.getInstance();
                AtlasManager.getInstance().createArrays();
                Message.createMessages();
                PlayerData.getInstance();
                mainMenu = new MainMenu(entity);
                entity.setMainMenu(mainMenu);
            } else {
                mainMenu = entity.getMainMenu();
                Gdx.input.setInputProcessor(mainMenu.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            mainMenu.render();
        }

        @Override
        public void exit(Game entity) {
//            mainMenu.dispose();
        }
    },
    CREDITS {
        private Credits credits;

        @Override
        public void enter(Game entity) {
            if (entity.getCredits() == null) {
                credits = new Credits(entity);
                entity.setCredits(credits);
            } else {
                credits = entity.getCredits();
                Gdx.input.setInputProcessor(credits.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            credits.render();
        }

        @Override
        public void exit(Game entity) {
//            credits.dispose();
        }
    },
    OPTIONS {
        private Options options;

        @Override
        public void enter(Game entity) {
            if (entity.getOptions() == null) {
                options = new Options(entity);
                entity.setOptions(options);
            } else {
                options = entity.getOptions();
                options.updateInfoInViews();
                Gdx.input.setInputProcessor(options.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            options.render();
        }

        @Override
        public void exit(Game entity) {
//            options.dispose();
        }
    },
    PREDICTION {
        private Prediction prediction;

        @Override
        public void enter(Game entity) {
            if (entity.getPrediction() == null) {
                prediction = new Prediction(entity);
                entity.setPrediction(prediction);
            } else {
                prediction = entity.getPrediction();
                prediction.updateInfoInViews();
                Gdx.input.setInputProcessor(prediction.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            prediction.render();
        }

        @Override
        public void exit(Game entity) {
//            prediction.dispose();
        }
    },
    SAVE_GAME {
        private SaveGame saveGame;

        @Override
        public void enter(Game entity) {
            if (entity.getSaveGame() == null) {
                saveGame = new SaveGame(entity);
                entity.setSaveGame(saveGame);
            } else {
                saveGame = entity.getSaveGame();
                saveGame.updateInfoInViews();
                Gdx.input.setInputProcessor(saveGame.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            saveGame.render();
        }

        @Override
        public void exit(Game entity) {
//            saveGame.dispose();
        }
    },
    FLY {
        private Fly fly;

        @Override
        public void enter(Game entity) {
            if (entity.getFly() == null) {
                fly = new Fly(entity);
                entity.setFly(fly);
            } else {
                fly = entity.getFly();
                fly.updateInfoInViews();
                Gdx.input.setInputProcessor(fly.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            fly.render();
        }

        @Override
        public void exit(Game entity) {
//            fly.dispose();
        }
    },
    USE_SKILL {
        private UseSkill useSkill;

        @Override
        public void enter(Game entity) {
            if (entity.getUseSkill() == null) {
                useSkill = new UseSkill(entity);
                entity.setUseSkill(useSkill);
            } else {
                useSkill = entity.getUseSkill();
                useSkill.updateInfoInViews();
                Gdx.input.setInputProcessor(useSkill.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            useSkill.render();
        }

        @Override
        public void exit(Game entity) {
//            useSkill.dispose();
        }
    },
    HELP {
        private Help help;

        @Override
        public void enter(Game entity) {
            if (entity.getHelp() == null) {
                help = new Help(entity);
                entity.setHelp(help);
            } else {
                help = entity.getHelp();
                help.updateInfoInViews();
                Gdx.input.setInputProcessor(help.getStage());
            }
        }

        @Override
        public void update(Game entity) {
            help.render();
        }

        @Override
        public void exit(Game entity) {
//            help.dispose();
        }
    };

    @Override
    public void enter(Game entity) {
    }

    @Override
    public void update(Game entity) {
    }

    @Override
    public void exit(Game entity) {

    }

    @Override
    public boolean onMessage(Game entity, Telegram telegram) {
        return false;
    }
}
