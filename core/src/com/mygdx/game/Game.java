package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ai.fsm.DefaultStateMachine;

import stateMachine.GameState;
import userInterface.gameScreens.BuySkill;
import userInterface.gameScreens.Fly;
import userInterface.gameScreens.InPlay;
import userInterface.gameScreens.Prediction;
import userInterface.gameScreens.UseSkill;
import userInterface.menus.Credits;
import userInterface.menus.Help;
import userInterface.menus.LoadGame;
import userInterface.menus.MainMenu;
import userInterface.menus.Options;
import userInterface.menus.SaveGame;

public class Game extends ApplicationAdapter {

    private DefaultStateMachine<Game, GameState> stateMachine;
    private InPlay inPlay;
    private BuySkill buySkill;
    private Fly fly;
    private Prediction prediction;
    private UseSkill useSkill;
    private LoadGame loadGame;
    private SaveGame saveGame;
    private Options options;
    private MainMenu mainMenu;
    private Credits credits;
    private Help help;

    @Override
    public void create() {
        stateMachine = new DefaultStateMachine<Game, GameState>();
        stateMachine.setOwner(this);
        stateMachine.changeState(GameState.MAIN_MENU);
    }

    @Override
    public void render() {
        stateMachine.update();
    }

    public InPlay getInPlay() {
        return inPlay;
    }

    public void setInPlay(InPlay inPlay) {
        this.inPlay = inPlay;
    }

    public BuySkill getBuySkill() {
        return buySkill;
    }

    public LoadGame getLoadGame() {
        return loadGame;
    }

    public void setLoadGame(LoadGame loadGame) {
        this.loadGame = loadGame;
    }

    public SaveGame getSaveGame() {
        return saveGame;
    }

    public void setSaveGame(SaveGame saveGame) {
        this.saveGame = saveGame;
    }

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }

    public void setMainMenu(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    public void setBuySkill(BuySkill buySkill) {
        this.buySkill = buySkill;
    }

    public Fly getFly() {
        return fly;
    }

    public void setFly(Fly fly) {
        this.fly = fly;
    }

    public Prediction getPrediction() {
        return prediction;
    }

    public void setPrediction(Prediction prediction) {
        this.prediction = prediction;
    }

    public UseSkill getUseSkill() {
        return useSkill;
    }

    public void setUseSkill(UseSkill useSkill) {
        this.useSkill = useSkill;
    }

    public Credits getCredits() {
        return credits;
    }

    public void setCredits(Credits credits) {
        this.credits = credits;
    }

    public Help getHelp() {
        return help;
    }

    public void setHelp(Help help) {
        this.help = help;
    }

    public DefaultStateMachine<Game, GameState> getStateMachine() {
        return stateMachine;
    }
}
