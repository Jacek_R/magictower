package graphic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import java.util.ArrayList;

public class AtlasManager {

    private TextureAtlas doors;
    private TextureAtlas walls;
    private TextureAtlas monsters;
    private TextureAtlas keys;
    private TextureAtlas ui;
    private TextureAtlas dialogs;
    private TextureAtlas buttons;
    private TextureAtlas potions;
    private TextureAtlas items;
    private TextureAtlas traps;
    private TextureAtlas empty;
    private TextureAtlas obstacles;

    protected ArrayList<Skin> dialogSkinsLarge;
    protected ArrayList<Skin> dialogSkinsSmall;
    protected ArrayList<Skin> smallButtonsSkin;
    protected ArrayList<Skin> mediumButtonsSkin;
    protected ArrayList<Skin> largeButtonsSkin;
    protected ArrayList<SpriteDrawable> screenBackgrounds;

    private static AtlasManager instance;

    private AtlasManager() {
        doors = new TextureAtlas("atlasDoors.pack");
        walls = new TextureAtlas("atlasWalls.pack");
        buttons = new TextureAtlas("buttons.pack");
        potions = new TextureAtlas("atlasPotions.pack");
        keys = new TextureAtlas("atlasKeys.pack");
        items = new TextureAtlas("atlasItems.pack");
        dialogs = new TextureAtlas("dialogBg.pack");
        traps = new TextureAtlas("atlasTraps.pack");
        ui = new TextureAtlas("atlasUi.pack");
        monsters = new TextureAtlas("atlasMonsters.pack");
        empty = new TextureAtlas("atlasNone.pack");
        obstacles = new TextureAtlas("atlasObstacles.pack");
    }

    public static AtlasManager getInstance() {
        if (instance == null) {
            instance = new AtlasManager();
        }
        return instance;
    }

    public void createArrays() {
        dialogSkinsSmall = new ArrayList<Skin>();
        dialogSkinsLarge = new ArrayList<Skin>();
        smallButtonsSkin = new ArrayList<Skin>();
        mediumButtonsSkin = new ArrayList<Skin>();
        largeButtonsSkin = new ArrayList<Skin>();
        screenBackgrounds = new ArrayList<SpriteDrawable>();

        for (int i = 0; i < 5; i++) {
            dialogSkinsSmall.add(dialogSkin(true, i + 1));
        }

        for (int i = 0; i < 5; i++) {
            dialogSkinsLarge.add(dialogSkin(false, i + 1));
        }
        for (int i = 0; i < 5; i++) {
            smallButtonsSkin.add(textButtonSkin("buttonSmall", i + 1));
        }
        for (int i = 0; i < 5; i++) {
            mediumButtonsSkin.add(textButtonSkin("buttonMedium", i + 1));
        }
        for (int i = 0; i < 5; i++) {
            largeButtonsSkin.add(textButtonSkin("buttonLarge", i + 1));
        }
        for (int i = 0; i < 5; i++) {
            screenBackgrounds.add(new SpriteDrawable(new Sprite(new Texture("back_" + (i + 1) + ".png"))));
        }
    }

    private Skin dialogSkin(boolean smallDialog, int zone) {
        Skin skin = new Skin();
        BitmapFont font = new BitmapFont();
        skin.add("default", font);
        AtlasManager manager = AtlasManager.getInstance();

        Pixmap pixmap = new Pixmap(200, 160, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.argb8888(0, 0, 0, 0));
        pixmap.fill();

        skin.add("label", new Texture(pixmap));

        SpriteDrawable background;

        if (smallDialog) {
            background = new SpriteDrawable(new Sprite(manager.getDialogs().findRegion("dialogSmall", zone)));
        } else {
            background = new SpriteDrawable(new Sprite(manager.getDialogs().findRegion("dialog", zone)));
        }

        Window.WindowStyle windowStyle = new Window.WindowStyle();
        windowStyle.background = skin.newDrawable(background);
        windowStyle.titleFont = skin.getFont("default");

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.background = skin.newDrawable("label");
        labelStyle.font = skin.getFont("default");

        SpriteDrawable buttonBg;
        if (smallDialog) {
            buttonBg = new SpriteDrawable(new Sprite(manager.getButtons().findRegion("buttonMedium", zone)));
        } else {
            buttonBg = new SpriteDrawable(new Sprite(manager.getButtons().findRegion("buttonLarge", zone)));
        }

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable(buttonBg);
        textButtonStyle.down = skin.newDrawable(buttonBg, Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable(buttonBg, Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");

        skin.add("default", textButtonStyle);
        skin.add("default", labelStyle);
        skin.add("default", windowStyle);

        return skin;
    }

    private Skin textButtonSkin(String texture, int zone) {
        BitmapFont font = new BitmapFont();
        Skin skin = new Skin();
        AtlasManager manager = AtlasManager.getInstance();

        skin.add("default", font);

        SpriteDrawable drawable = new SpriteDrawable(new Sprite(manager.getButtons().findRegion(texture, zone)));
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();

        textButtonStyle.up = skin.newDrawable(drawable);
        textButtonStyle.down = skin.newDrawable(drawable, Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable(drawable, Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");

        skin.add("default", textButtonStyle);
        return skin;
    }

    public TextureAtlas getButtons() {
        return buttons;
    }

    public TextureAtlas getDoors() {
        return doors;
    }

    public TextureAtlas getWalls() {
        return walls;
    }

    public TextureAtlas getMonsters() {
        return monsters;
    }

    public TextureAtlas getKeys() {
        return keys;
    }

    public TextureAtlas getPotions() {
        return potions;
    }

    public TextureAtlas getItems() {
        return items;
    }

    public TextureAtlas getEmpty() {
        return empty;
    }

    public TextureAtlas getObstacles() {
        return obstacles;
    }

    public TextureAtlas getUi() {
        return ui;
    }

    public TextureAtlas getDialogs() {
        return dialogs;
    }

    public ArrayList<Skin> getDialogSkinsLarge() {
        return dialogSkinsLarge;
    }

    public ArrayList<Skin> getDialogSkinsSmall() {
        return dialogSkinsSmall;
    }

    public ArrayList<Skin> getSmallButtonsSkin() {
        return smallButtonsSkin;
    }

    public ArrayList<Skin> getMediumButtonsSkin() {
        return mediumButtonsSkin;
    }

    public ArrayList<Skin> getLargeButtonsSkin() {
        return largeButtonsSkin;
    }

    public ArrayList<SpriteDrawable> getScreenBackgrounds() {
        return screenBackgrounds;
    }

    public TextureAtlas getTraps() {
        return traps;
    }
}
