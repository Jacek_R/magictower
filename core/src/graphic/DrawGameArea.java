package graphic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;

import gameObjects.Content;
import gameObjects.actors.Player;
import gamePlay.Movement;
import globalVariables.Const;
import globalVariables.PlayerData;
import music.SoundEffect;

public class DrawGameArea {

    private SpriteBatch batch;
    private TextureRegion stageBackground;
    private Player player;
    private Table gameMap;
    private Image[][] images;
    private Animation bloodAnimation;
    private Movement move;
    private TextureRegion bloodFrame;
    private ArrayList<TextureRegion> backgrounds;

    private boolean animateBlood;
    private boolean removeMonster;
    private static final int offset_x = 10;
    private static final int offest_y = 15;
    private int bloodX;
    private int bloodY;
    private float time;
    private PlayerData data;

    private static final String ATLAS_NAME = "atlasBloodAnimation.pack";

    public DrawGameArea() {
        data = PlayerData.getInstance();
        batch = new SpriteBatch();
        player = new Player();
        images = new Image[Const.MAP_X][Const.MAP_Y];
        bloodAnimation = createAnimation();
        backgrounds = new ArrayList<TextureRegion>();
        gameMap = new Table();

        backgrounds.add(new TextureRegion(new Texture("back_1.png")));
        backgrounds.add(new TextureRegion(new Texture("back_2.png")));
        backgrounds.add(new TextureRegion(new Texture("back_3.png")));
        backgrounds.add(new TextureRegion(new Texture("back_4.png")));
        backgrounds.add(new TextureRegion(new Texture("back_5.png")));

        changeBackground();
        createTableWithImages();
    }

    public void renderBlood() {
        if (animateBlood) {
            updateBlood(Gdx.graphics.getDeltaTime());
            batch.begin();
            batch.draw(
                    bloodFrame,
                    (bloodX * Const.TILE_SIZE + offset_x + 10),
                    (bloodY * Const.TILE_SIZE + offest_y + 15)
            );
            batch.end();
        }
    }

    public void renderPlayer() {
        player.update(Gdx.graphics.getDeltaTime());

        batch.begin();
        batch.draw(
                player.getFrame(),
                (data.getPosX() * Const.TILE_SIZE + offset_x + 5),
                (data.getPosY() * Const.TILE_SIZE + offest_y + 10)
        );
        batch.end();
    }

    public void renderBackground() {
        batch.begin();
        batch.draw(stageBackground, 0, 0);
        batch.end();
    }

    private void updateBlood(float dt) {
        time += dt;
        bloodFrame = bloodAnimation.getKeyFrame(time);

        if (bloodAnimation.isAnimationFinished(time)) {
            time = 0f;
            animateBlood = false;
            if (removeMonster) {
                move.removeObject(bloodX, bloodY);
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.DIE_MONSTER);
            }
        }
    }

    public void startBloodAnimation(int x, int y, Movement move, boolean removeMonster) {
        animateBlood = true;
        bloodX = x;
        bloodY = y;
        this.move = move;
        this.removeMonster = removeMonster;
    }

    private Animation createAnimation() {
        Array<TextureAtlas.AtlasRegion> regions = new TextureAtlas(ATLAS_NAME).findRegions("blood");
        return new Animation(0.1f, regions, Animation.PlayMode.NORMAL);
    }

    public void createTableWithImages() {
        int x = 0;
        int y = Const.MAP_Y - 1;
        gameMap.clearChildren();
        int currentZone = Const.zone(data.getCurrentFloor());
        Texture mapBackground = new Texture("background_" + currentZone + ".png");
        gameMap.setBackground(new SpriteDrawable(new Sprite(mapBackground)));

        while (y >= 0) {
            while (x < Const.MAP_X) {
                images[x][y] = new Image(createImage(data.getCurrentMap()[x][y]));
                gameMap.add(images[x][y]).size(50, 50);
                x++;
                if (x == Const.MAP_X) {
                    gameMap.row();
                }
            }
            y--;
            x = 0;
        }
    }

    private TextureRegion createImage(Content object) {
        TextureRegion region;
        AtlasManager manager = AtlasManager.getInstance();

        Content.AtlasType atlas = object.getAtlasType();
        String imageName = object.getImage();
        int index = object.getIndexInAtlas();

        switch (atlas) {
            case EMPTY:
                region = manager.getEmpty().findRegion(imageName);
                break;
            case MONSTERS:
                region = manager.getMonsters().findRegion(imageName);
                break;
            case OBSTACLES:
                region = manager.getObstacles().findRegion(imageName);
                break;
            case DOORS:
                region = manager.getDoors().findRegion(imageName, index);
                break;
            case ITEMS:
                region = manager.getItems().findRegion(imageName);
                break;
            case WALLS:
                region = manager.getWalls().findRegion(imageName, index);
                break;
            case TRAPS:
                region = manager.getTraps().findRegion(imageName);
                break;
            case POTIONS:
                region = manager.getPotions().findRegion(imageName);
                break;
            case KEYS:
                region = manager.getKeys().findRegion(imageName);
                break;
            default:
                region = new TextureRegion();
                break;
        }
        return region;
    }

    public void changeBackground() {
        int currentZone = Const.zone(data.getCurrentFloor());
        stageBackground = backgrounds.get(currentZone - 1);
    }

    public void createBatch() {
        batch = new SpriteBatch();
    }

    public void dispose() {
        batch.dispose();
    }

    public Image[][] getImages() {
        return images;
    }

    public Table getGameMap() {
        return gameMap;
    }

    public boolean isAnimateBlood() {
        return animateBlood;
    }

    public Player getPlayer() {
        return player;
    }
}
