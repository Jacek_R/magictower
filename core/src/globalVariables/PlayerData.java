package globalVariables;

import java.util.ArrayList;
import java.util.Calendar;

import gameObjects.Content;
import levels.MapLoader;
import utils.DataManager;
import utils.Encrypter;

public class PlayerData {

    private Content[][] currentMap;
    private ArrayList<Content[][]> levels;
    private int currentFloor;
    private int maxFloor;
    private int posX;
    private int posY;
    private int skillsBought;
    private int statsBought;
    private ArrayList<Integer> keys;
    private ArrayList<Integer> buffs;
    private ArrayList<Integer> stats;
    private ArrayList<Integer> items;
    private ArrayList<Boolean> skills;
    private ArrayList<Boolean> shields;

    public static final String date = "iopLKJ";
    private static final String sLevels = "xY7Mfs";
    private static final String sCurrentFloor = "kkiSfs";
    private static final String sMaxFloor = "3fgOD2";
    private static final String sPosX = "@kg8Ds";
    private static final String sPosY = "VBeyds";
    private static final String sSkillsBought = "POdsev";
    private static final String sStatsBought = "NurySd";
    private static final String sKeys = "LKJdsa";
    private static final String sBuffs = "IMedsA";
    private static final String sStats = "GmDedS";
    private static final String sItems = "*&^%$s";
    private static final String sSkills = "VHYT6%";
    private static final String sShields = "<>Qe@#";

    private static PlayerData instance;

    private PlayerData() {
        levels = new ArrayList<Content[][]>();
        keys = new ArrayList<Integer>();
        buffs = new ArrayList<Integer>();
        stats = new ArrayList<Integer>();
        items = new ArrayList<Integer>();
        skills = new ArrayList<Boolean>();
        shields = new ArrayList<Boolean>();
    }

    public static PlayerData getInstance() {
        if (instance == null) {
            instance = new PlayerData();
        }
        return instance;
    }

    public void loadMap(int levelNumber) {
        String mapName = "level_" + levelNumber + ".tmx";
        currentMap = MapLoader.loadMap(mapName, Const.MAP_X, Const.MAP_Y);
        levels.add(currentMap);
    }

    public void createInitialData() {
        skillsBought = 0;
        statsBought = 0;
        posY = 0;
        posX = 5;
        maxFloor = 1;
        currentFloor = 1;

        keys.add(Const.KEY_BRONZE, 0);
        keys.add(Const.KEY_SILVER, 0);
        keys.add(Const.KEY_GOLD, 0);
        keys.add(Const.KEY_RED, 0);

        buffs.add(Const.BLESS, 0);
        buffs.add(Const.INSTANT_STRIKE, 0);
        buffs.add(Const.PROTECT, 0);
        buffs.add(Const.VAMPIRE_EDGE, 0);

        buffs.add(Const.SILENCED, 0);
        buffs.add(Const.BURNED, 0);
        buffs.add(Const.POISONED, 0);
        buffs.add(Const.CURSED, 0);

        stats.add(Const.ATTACK, 10);
        stats.add(Const.DEFENSE, 10);
        stats.add(Const.HEALTH, 400);
        stats.add(Const.MANA, 50);
        stats.add(Const.EXP, 0);
        stats.add(Const.GOLD, 0);

        skills.add(Const.BLESS, false);
        skills.add(Const.INSTANT_STRIKE, false);
        skills.add(Const.PROTECT, false);
        skills.add(Const.VAMPIRE_EDGE, false);
        skills.add(Const.CLEANSE, false);

        items.add(Const.BOMB, 0);
        items.add(Const.MASTER_KEY, 0);
        items.add(Const.POTION, 0);

        shields.add(Const.SHIELD_FINAL, false);
        shields.add(Const.SHIELD_SILENCE, false);
        shields.add(Const.SHIELD_CURSE, false);
        shields.add(Const.SHIELD_POISON, false);
        shields.add(Const.SHIELD_BURN, false);
    }

    public void resetData() {
        levels.clear();
        keys.clear();
        buffs.clear();
        stats.clear();
        items.clear();
        skills.clear();
        shields.clear();

        skillsBought = 0;
        statsBought = 0;
        posY = 0;
        posX = 5;
        maxFloor = 1;
        currentFloor = 1;
    }

    public void saveGame(String slot) {
        levels.set(currentFloor - 1, currentMap);
        Calendar cal = Calendar.getInstance();
        String day = String.valueOf(cal.get(Calendar.DATE));
        String month = String.valueOf((cal.get(Calendar.MONTH) + 1));
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String hour;
        String minute;
        int h = cal.get(Calendar.HOUR_OF_DAY);
        int m = cal.get(Calendar.MINUTE);

        if (h < 10) {
            hour = "0" + String.valueOf(h);
        } else {
            hour = String.valueOf(h);
        }
        if (m < 10) {
            minute = "0" + String.valueOf(m);
        } else {
            minute = String.valueOf(m);
        }

        String currentDate = day + "-" + month + "-" + year + " , " + hour + ":" + minute;
        try {
            DataManager.savePreferences(slot, date, Encrypter.encrypt(DataManager.save(currentDate)));
            DataManager.savePreferences(slot, sPosY, Encrypter.encrypt(DataManager.save(posY)));
            DataManager.savePreferences(slot, sPosX, Encrypter.encrypt(DataManager.save(posX)));
            DataManager.savePreferences(slot, sCurrentFloor, Encrypter.encrypt(DataManager.save(currentFloor)));
            DataManager.savePreferences(slot, sMaxFloor, Encrypter.encrypt(DataManager.save(maxFloor)));
            DataManager.savePreferences(slot, sSkillsBought, Encrypter.encrypt(DataManager.save(skillsBought)));
            DataManager.savePreferences(slot, sStatsBought, Encrypter.encrypt(DataManager.save(statsBought)));
            DataManager.savePreferences(slot, sStats, Encrypter.encrypt(DataManager.save(stats)));
            DataManager.savePreferences(slot, sKeys, Encrypter.encrypt(DataManager.save(keys)));
            DataManager.savePreferences(slot, sBuffs, Encrypter.encrypt(DataManager.save(buffs)));
            DataManager.savePreferences(slot, sItems, Encrypter.encrypt(DataManager.save(items)));
            DataManager.savePreferences(slot, sSkills, Encrypter.encrypt(DataManager.save(skills)));
            DataManager.savePreferences(slot, sShields, Encrypter.encrypt(DataManager.save(shields)));
            DataManager.savePreferences(slot, sLevels, Encrypter.encrypt(DataManager.save(
                    levels
            )));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void loadGame(String slot) {
        try {
            posY = DataManager.loadInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sPosY)));
            posX = DataManager.loadInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sPosX)));
            currentFloor = DataManager.loadInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sCurrentFloor)));
            maxFloor = DataManager.loadInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sMaxFloor)));
            skillsBought = DataManager.loadInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sSkillsBought)));
            statsBought = DataManager.loadInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sStatsBought)));
            stats = DataManager.loadArrayListInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sStats)));
            keys = DataManager.loadArrayListInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sKeys)));
            buffs = DataManager.loadArrayListInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sBuffs)));
            items = DataManager.loadArrayListInteger(Encrypter.decrypt(DataManager.loadPreferences(slot, sItems)));
            shields = DataManager.loadArrayListBool(Encrypter.decrypt(DataManager.loadPreferences(slot, sShields)));
            skills = DataManager.loadArrayListBool(Encrypter.decrypt(DataManager.loadPreferences(slot, sSkills)));
            levels = DataManager.loadArrayListContent(Encrypter.decrypt(DataManager.loadPreferences(slot, sLevels)));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        currentMap = levels.get(currentFloor - 1);
    }

    public Content[][] getCurrentMap() {
        return currentMap;
    }

    public void setCurrentMap(Content[][] currentMap) {
        this.currentMap = currentMap;
    }

    public ArrayList<Content[][]> getLevels() {
        return levels;
    }

    public void setLevels(ArrayList<Content[][]> levels) {
        this.levels = levels;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public int getMaxFloor() {
        return maxFloor;
    }

    public void setMaxFloor(int maxFloor) {
        this.maxFloor = maxFloor;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getSkillsBought() {
        return skillsBought;
    }

    public void setSkillsBought(int skillsBought) {
        this.skillsBought = skillsBought;
    }

    public int getStatsBought() {
        return statsBought;
    }

    public void setStatsBought(int statsBought) {
        this.statsBought = statsBought;
    }

    public ArrayList<Integer> getKeys() {
        return keys;
    }

    public ArrayList<Integer> getBuffs() {
        return buffs;
    }

    public ArrayList<Integer> getStats() {
        return stats;
    }

    public ArrayList<Integer> getItems() {
        return items;
    }

    public void setItems(ArrayList<Integer> items) {
        this.items = items;
    }

    public ArrayList<Boolean> getSkills() {
        return skills;
    }

    public ArrayList<Boolean> getShields() {
        return shields;
    }

}
