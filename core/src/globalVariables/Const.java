package globalVariables;

public class Const {

    public static final int BLESS_COST = 20;
    public static final int CLEANSE_COST = 15;
    public static final int INSTANT_STRIKE_COST = 15;
    public static final int PROTECT_COST = 25;
    public static final int VAMPIRE_COST = 20;

    public static final int BLESS_DURATION = 2;
    public static final int CLEANSE_DURATION = 0;
    public static final int INSTANT_STRIKE_DURATION = 1;
    public static final int PROTECT_DURATION = 2;
    public static final int VAMPIRE_DURATION = 1;

    public static final int POTION_HEAL = 20;

    public static final int BLESS = 0;
    public static final int INSTANT_STRIKE = 1;
    public static final int PROTECT = 2;
    public static final int VAMPIRE_EDGE = 3;
    public static final int CLEANSE = 4;

    public static final String REGION_BLESS = "bless";
    public static final String REGION_CLEANSE = "cleanse";
    public static final String REGION_INSTANT_STRIKE = "instant";
    public static final String REGION_PROTECT = "protect";
    public static final String REGION_VAMPIRE_EDGE = "vampire";

    public static final int BOMB = 0;
    public static final int MASTER_KEY = 1;
    public static final int POTION = 2;

    public static final String REGION_BOMB = "bomb";
    public static final String REGION_MASTER_KEY = "key";
    public static final String REGION_POTION = "potion";

    public static final int SILENCED = 4;
    public static final int BURNED = 5;
    public static final int POISONED = 6;
    public static final int CURSED = 7;

    public static final String REGION_SILENCED = "silence";
    public static final String REGION_BURNED = "burn";
    public static final String REGION_POISONED = "poison";
    public static final String REGION_CURSED = "curse";

    public static final int ATTACK = 0;
    public static final int DEFENSE = 1;
    public static final int HEALTH = 2;
    public static final int MANA = 3;
    public static final int EXP = 4;
    public static final int GOLD = 5;

    public static final String REGION_ATTACK = "attack";
    public static final String REGION_DEFENSE = "defense";
    public static final String REGION_HEALTH = "health";
    public static final String REGION_MANA = "mana";
    public static final String REGION_EXP = "experience";
    public static final String REGION_GOLD = "gold";

    public static final int KEY_BRONZE = 0;
    public static final int KEY_SILVER = 1;
    public static final int KEY_GOLD = 2;
    public static final int KEY_RED = 3;

    public static final int SHIELD_FINAL = 0;
    public static final int SHIELD_SILENCE = 1;
    public static final int SHIELD_CURSE = 2;
    public static final int SHIELD_POISON = 3;
    public static final int SHIELD_BURN = 4;

    public static final String REGION_SHIELD_FINAL = "silver";
    public static final String REGION_SHIELD_SILENCE = "blue";
    public static final String REGION_SHIELD_CURSE = "yellow";
    public static final String REGION_SHIELD_POISON = "green";
    public static final String REGION_SHIELD_BURN = "red";

    public static final int MANA_BASIC_INCREASE = 10;
    public static final int HEALTH_BASIC_INCREASE = 100;

    public static final double ATK_BASE_INCREASE = 1;
    public static final double DEF_BASE_INCREASE = 1.6;
    public static final double STAT_INCREASE = 1.15;
    public static final double BASIC_STAT_COST = 30;
    public static final double BASIC_SKILL_COST = 100;
    public static final double STAT_COST_INCREASE = 1.15;
    public static final double SKILL_COST_INCREASE = 2;

    public static final int MAP_X = 11;
    public static final int MAP_Y = 11;
    public static final int TILE_SIZE = 50;

    public static int zone(int floor) {
        return (floor - 1) / 10 + 1;
    }
}
