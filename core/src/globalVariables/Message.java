package globalVariables;

import java.util.HashMap;
import java.util.Map;

public class Message {

    public static Map<Integer, String> messagesElder;
    public static Map<Integer, String> messagesMerchant;

    public static final String NAME_INSTANT_STRIKE = "Instant strike";
    public static final String NAME_PROTECT = "Protect";
    public static final String NAME_CLEANSE = "Cleanse";
    public static final String NAME_BLESS = "Bless";
    public static final String NAME_VAMPIRE_EDGE = "Vampire edge";

    public static final String NAME_BOMB = "Bomb";
    public static final String NAME_POTION = "Potion";
    public static final String NAME_MASTER_KEY = "Master key";

    public static final String DIALOG_CONFIRM = "Ok";
    public static final String DIALOG_CANCEL = "Cancel";
    public static final String DIALOG_CLOSE = "Close";
    public static final String DIALOG_BUY = "Buy";
    public static final String DIALOG_DEAD = "You are dead";
    public static final String DIALOG_NO_ITEM = "You don't have this item";
    public static final String DIALOG_NO_MANA = "You don't have enough\nmana to use this skill";
    public static final String DIALOG_NO_GOLD = "You don't have enough\ngold";
    public static final String DIALOG_NO_EXP = "You don't have enough\nexperience";
    public static final String DIALOG_NO_KEY = "You don't have a key";
    public static final String DIALOG_IRON_DOOR_LOCKED = "The door is locked.\n Kill the guards.";
    public static final String DIALOG_MESSAGE_SILENCE = "You are silenced.\nYou can't use skills.";
    public static final String DIALOG_MESSAGE_DONT_HAVE_SKILL = "You don't have\n this skill.";
    public static final String DIALOG_SKILL_IS_IN_EFFECT = "This skill is active";
    public static final String DIALOG_CLEANSE_NOT_NECESSARY = "There is no need to use this";
    public static final String DIALOG_SKILL_MAXED = "This skill is at\nmaximum level";
    public static final String DIALOG_CANT_ATTACK = "You can't attack\nthis monster";
    public static final String DIALOG_QUIT = "Quit game?";

    public static final String AMOUNT = "Amount : ";
    public static final String ATTACK = "Attack : ";
    public static final String DEFENSE = "Defense : ";
    public static final String HEALTH = "Health : ";
    public static final String MANA = "Mana : ";
    public static final String EXPERIENCE = "Experience : ";

    public static final String COST = "Cost : ";
    public static final String COST_STAT = "Cost to\nbuy stat:";
    public static final String COST_SKILL = "Cost to\nbuy skill:";
    public static final String INCREASE_ATK = "Atk\nincrease";
    public static final String INCREASE_DEF = "Def\nincrease";
    public static final String INCREASE_HEALTH = "Health\nincrease";
    public static final String INCREASE_MANA = "Mana\nincrease";

    public static final String USE = "Use";
    public static final String FLOOR = "F: ";
    public static final String FLY_MESSAGE = "Select to which floor you want to fly";
    public static final String CANT_ATTACK = "Can't attack";
    public static final String LOAD_MESSAGE = "Select from where you want to load";
    public static final String SAVE_MESSAGE = "Select where do you want to save";
    public static final String EMPTY = "Empty";

    public static void createMessages() {
        messagesElder = new HashMap<Integer, String>();
        messagesMerchant = new HashMap<Integer, String>();

        messagesElder.put(1, "This game is inspired by\nTower of the sorcerer.\nFirst 10 floors are\nvery similar.");
        messagesElder.put(2, "I don't know what to say. Yet!");
        messagesElder.put(3, "You can get stuck, so save\noften on different slots.");
        messagesElder.put(4, "This tower have 50 floors.\nAfter each 10 floors\nthere is a different zone.");
        messagesElder.put(5, "Attack power is generally\nmore important than\ndefense power.");

        messagesMerchant.put(1, "I have a 1000 gold for you.\nPlease accept");
        messagesMerchant.put(2, "I will sell you a silver key\n for 50 gold");
        messagesMerchant.put(3, "I will sell you 5 bronze keys\n for 50 gold");

    }
}
