package utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;

import java.util.ArrayList;
import java.util.HashMap;

import gameObjects.Content;

public class DataManager {

    public static String save(Object toSave) {
        Json json = new Json();
        return json.toJson(toSave);
    }

    @SuppressWarnings(value = "unchecked")
    public static HashMap<String, Integer> loadHashMapInt(String string) {
        Json json = new Json();
        return json.fromJson(HashMap.class, string);
    }

    @SuppressWarnings(value = "unchecked")
    public static HashMap<String, Boolean> loadHashMapBool(String string) {
        Json json = new Json();
        return json.fromJson(HashMap.class, string);
    }

    @SuppressWarnings(value = "unchecked")
    public static ArrayList<Content[][]> loadArrayListContent(String string) {
        ArrayList<Content[][]> list;
        Json json = new Json();
        list = json.fromJson(ArrayList.class, Content[][].class, string);
        return list;
    }

    @SuppressWarnings(value = "unchecked")
    public static ArrayList<Integer> loadArrayListInteger(String string) {
        ArrayList<Integer> list;
        Json json = new Json();
        list = json.fromJson(ArrayList.class, Integer.class, string);
        return list;
    }

    @SuppressWarnings(value = "unchecked")
    public static ArrayList<Boolean> loadArrayListBool(String string) {
        ArrayList<Boolean> list;
        Json json = new Json();
        list = json.fromJson(ArrayList.class, Boolean.class, string);
        return list;
    }

    public static Integer loadInteger(String string) {
        Json json = new Json();
        return json.fromJson(Integer.class, string);
    }

    public static Float loadFloat(String string) {
        Json json = new Json();
        return json.fromJson(Float.class, string);
    }

    public static void savePreferences(String prefName, String varName, String jsonToSave) {
        Preferences preferences = Gdx.app.getPreferences(prefName);
        preferences.putString(varName, jsonToSave);
        preferences.flush();
    }

    public static String loadPreferences(String prefName, String varName) {
        Preferences preferences = Gdx.app.getPreferences(prefName);
        return preferences.getString(varName);
    }
}
