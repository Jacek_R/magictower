package utils;

import java.util.ArrayList;

import gameObjects.Content;
import gameObjects.actors.Monster;
import gameObjects.actors.Npc;
import gameObjects.pickUps.Gem;
import gameObjects.pickUps.Item;
import gameObjects.pickUps.Key;
import gameObjects.pickUps.Potion;
import gameObjects.terrain.Door;
import gameObjects.terrain.Stairs;
import gameObjects.terrain.Trap;
import gameObjects.terrain.Wall;
import globalVariables.Const;
import levels.Container;

public class ContentObjectsConverter {

    public static ArrayList<Container[][]> convertToContainer(ArrayList<Content[][]> levels) {
        ArrayList<Container[][]> map = new ArrayList<Container[][]>();
        int x = 0;
        int y = 0;
        int z = 0;
        Content[][] currentLevel;

        Content obj;
        Container cont;

        while (z < levels.size()) {
            currentLevel = levels.get(z);
            Container[][] currentLevelContainer = new Container[Const.MAP_X][Const.MAP_Y];
            while (x < Const.MAP_X) {
                while (y < Const.MAP_Y) {
                    obj = currentLevel[x][y];

                    if (obj instanceof Monster) {
                        cont = new Container(Container.Type.MONSTER, ((Monster) obj).getType());
                    } else if (obj instanceof Npc) {
                        cont = new Container(
                                Container.Type.NPC, ((Npc) obj).getType(), ((Npc) obj).getMessage(), ((Npc) obj).getPrice(),
                                ((Npc) obj).getAmount(), ((Npc) obj).getItemToSell(), ((Npc) obj).getX(), ((Npc) obj).getY()
                        );
                    } else if (obj instanceof Gem) {
                        cont = new Container(Container.Type.GEM, ((Gem) obj).getType());
                    } else if (obj instanceof Item) {
                        cont = new Container(Container.Type.ITEM, ((Item) obj).getType());
                    } else if (obj instanceof Key) {
                        cont = new Container(Container.Type.KEY, ((Key) obj).getType());
                    } else if (obj instanceof Potion) {
                        cont = new Container(Container.Type.POTION, ((Potion) obj).getType());
                    } else if (obj instanceof Door) {
                        cont = new Container(Container.Type.DOOR, ((Door) obj).getType(), ((Door) obj).getGuard_1(), ((Door) obj).getGuard_2());
                    } else if (obj instanceof Stairs) {
                        cont = new Container(Container.Type.STAIRS, ((Stairs) obj).getType());
                    } else if (obj instanceof Trap) {
                        cont = new Container(Container.Type.TRAP, ((Trap) obj).getType());
                    } else if (obj instanceof Wall) {
                        cont = new Container(Container.Type.WALL);
                    } else {
                        cont = new Container(Container.Type.EMPTY);
                    }
                    currentLevelContainer[x][y] = cont;
                    y++;
                }
                x++;
                y = 0;
            }
            map.add(currentLevelContainer);
            z++;
            x = 0;
            y = 0;
        }
        return map;
    }

    public static ArrayList<Content[][]> readFromContainer(ArrayList<Container[][]> levels) {
        ArrayList<Content[][]> map = new ArrayList<Content[][]>();
        int x = 0;
        int y = 0;
        int z = 0;
        Container[][] currentLevelContainer;

        Content obj;
        Container cont;
        while (z < levels.size()) {
            currentLevelContainer = levels.get(z);
            Content[][] currentLevel = new Content[Const.MAP_X][Const.MAP_Y];

            while (x < Const.MAP_X) {
                while (y < Const.MAP_Y) {
                    cont = currentLevelContainer[x][y];
                    if (cont.getType() == Container.Type.MONSTER) {
                        obj = new Monster(cont.getMonsterType());
                    } else if (cont.getType() == Container.Type.NPC) {
                        Npc.Type type = cont.getNpcType();
                        if (type == Npc.Type.ELDER) {
                            obj = new Npc(type, cont.getMessage());
                        } else {
                            obj = new Npc(type, cont.getMessage(), cont.getPrice(), cont.getAmount(), cont.getItemToSell(), cont.getX(), cont.getY());
                        }
                    } else if (cont.getType() == Container.Type.GEM) {
                        obj = new Gem(cont.getGemType());
                    } else if (cont.getType() == Container.Type.ITEM) {
                        obj = new Item(cont.getItemType());
                    } else if (cont.getType() == Container.Type.KEY) {
                        obj = new Key(cont.getKeyType());
                    } else if (cont.getType() == Container.Type.POTION) {
                        obj = new Potion(cont.getPotionType());
                    } else if (cont.getType() == Container.Type.DOOR) {
                        Door.Type type = cont.getDoorType();
                        if (type == Door.Type.IRON) {
                            obj = new Door(type, cont.getGuard_1(), cont.getGuard_2());
                        } else {
                            obj = new Door(type);
                        }
                    } else if (cont.getType() == Container.Type.STAIRS) {
                        obj = new Stairs(cont.getStairsType());
                    } else if (cont.getType() == Container.Type.TRAP) {
                        obj = new Trap(cont.getTrapType());
                    } else if (cont.getType() == Container.Type.WALL) {
                        obj = new Wall();
                    } else {
                        obj = new Content();
                    }
                    currentLevel[x][y] = obj;
                    y++;
                }
                x++;
                y = 0;
            }
            map.add(currentLevel);
            z++;
            x = 0;
            y = 0;
        }
        return map;
    }

}
