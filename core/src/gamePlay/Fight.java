package gamePlay;

import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.utils.Timer;

import gameObjects.actors.Monster;
import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.gameScreens.InPlay;

public class Fight {

    private int monsterHealth;
    private int playerHealth;
    private int playerMana;
    private boolean endure;
    private PlayerData data;

    public Fight() {
        data = PlayerData.getInstance();
    }

    public boolean battle(Monster monster, InPlay inPlay) {
        monsterHealth = monster.getHealth();
        playerHealth = data.getStats().get(Const.HEALTH);
        playerMana = data.getStats().get(Const.MANA);
        endure = monster.isEndure();

        boolean monsterIsDead = false;

        playerHealth -= calculateDamage(monster);
        if (data.getBuffs().get(Const.VAMPIRE_EDGE) > 0) {
            playerHealth += Math.round(monster.getHealth() / 10f);
        }
        if (playerMana < 0) {
            playerMana = 0;
        }
        data.getStats().set(Const.HEALTH, playerHealth);
        data.getStats().set(Const.MANA, playerMana);

        if (monsterHealth <= 0) {
            monsterIsDead = true;
            int gold = data.getStats().get(Const.GOLD);
            int exp = data.getStats().get(Const.EXP);
            data.getStats().set(Const.GOLD, gold + monster.getGold());
            data.getStats().set(Const.EXP, exp + monster.getExperience());
            removeBuffs(inPlay);
        }
        if (playerHealth < 1) {
            killPlayer(inPlay);
        }
        return monsterIsDead;
    }

    private void removeBuffs(InPlay inPlay) {
        int bless = data.getBuffs().get(Const.BLESS);
        int instant = data.getBuffs().get(Const.INSTANT_STRIKE);
        int vampire = data.getBuffs().get(Const.VAMPIRE_EDGE);

        int curse = data.getBuffs().get(Const.CURSED);
        int burn = data.getBuffs().get(Const.BURNED);
        int silence = data.getBuffs().get(Const.SILENCED);
        int poison = data.getBuffs().get(Const.POISONED);

        if (bless > 0) {
            updateBuffs(Const.BLESS, Const.REGION_BLESS, inPlay, bless - 1);
            inPlay.getBless_dur().setText(String.valueOf(bless - 1));
        }
        if (instant > 0) {
            updateBuffs(Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE, inPlay, instant - 1);
            inPlay.getInstant_dur().setText(String.valueOf(instant - 1));
        }
        if (vampire > 0) {
            updateBuffs(Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE, inPlay, vampire - 1);
            inPlay.getVampire_dur().setText(String.valueOf(vampire - 1));
        }
        if (curse > 0) {
            updateBuffs(Const.CURSED, Const.REGION_CURSED, inPlay, curse - 1);
            inPlay.getCurse_dur().setText(String.valueOf(curse - 1));
        }
        if (burn > 0) {
            updateBuffs(Const.BURNED, Const.REGION_BURNED, inPlay, burn - 1);
            inPlay.getBurn_dur().setText(String.valueOf(burn - 1));
        }
        if (silence > 0) {
            updateBuffs(Const.SILENCED, Const.REGION_SILENCED, inPlay, silence - 1);
            inPlay.getSilence_dur().setText(String.valueOf(silence - 1));
        }
        if (poison > 0) {
            updateBuffs(Const.POISONED, Const.REGION_POISONED, inPlay, poison - 1);
            inPlay.getPoison_dur().setText(String.valueOf(poison - 1));
        }
    }

    private void updateBuffs(int buffName, String regionName, InPlay inPlay, int amount) {
        data.getBuffs().set(buffName, amount);
        inPlay.updateImage(buffName, regionName);
    }

    public int battleSimulation(Monster monster) {
        int damage = 0;

        if (!canAttackMonster(monster)) {
            return -99999999;
        } else {
            monsterHealth = monster.getHealth();
            playerHealth = data.getStats().get(Const.HEALTH);
            endure = monster.isEndure();

            damage += calculateDamageSimulation(monster);

            return damage;
        }
    }

    private boolean canAttackMonster(Monster monster) {
        boolean canAttack = true;
        int dmg = playerIsAttacking(monster);
        if (dmg <= 0) {
            canAttack = false;
        }
        if (monster.getRegeneration() >= dmg && dmg < monster.getHealth()) {
            canAttack = false;
        }
        return canAttack;
    }

    private int calculateDamageSimulation(Monster monster) {
        int receivedDamage = 0;

        boolean playerIsFirst = checkIfPlayerIsFirst(monster);

        while (monsterHealth > 0 && playerHealth > 0) {
            if (playerIsFirst) {
                receivedDamage += playerTurn(monster);
                receivedDamage += monsterTurn(monster);
            } else {
                receivedDamage += monsterTurn(monster);
                receivedDamage += playerTurn(monster);
            }
        }
        return receivedDamage;
    }

    private int calculateDamage(Monster monster) {
        int receivedDamage = 0;
        int roundDamage;

        boolean playerIsFirst = checkIfPlayerIsFirst(monster);

        while (monsterHealth > 0 && playerHealth > 0) {
            if (playerIsFirst) {
                receivedDamage += playerTurn(monster);
                roundDamage = monsterTurn(monster);
                receivedDamage += roundDamage;
                playerHealth -= roundDamage;
            } else {
                roundDamage = monsterTurn(monster);
                receivedDamage += roundDamage;
                playerHealth -= roundDamage;
                receivedDamage += playerTurn(monster);
            }
        }
        return receivedDamage;
    }

    private int playerTurn(Monster monster) {
        int dmg = 0;
        dmg += monster.getReflect();
        if (data.getBuffs().get(Const.BURNED) > 0) {
            dmg += data.getMaxFloor() * 2;
        }
        if (playerHealth > 0) {
            monsterHealth -= playerIsAttacking(monster);
        }
        return dmg;
    }

    private int monsterTurn(Monster monster) {
        int dmg = 0;
        if (monsterHealth > 0) {
            dmg += monsterIsAttacking(monster);
        } else if (endure) {
            endure = false;
            monsterHealth = 1;
            dmg += monsterIsAttacking(monster);
        }
        if (monsterHealth > 0 && monsterHealth < monster.getHealth() && monster.getRegeneration() > 0) {
            monsterHealth += monster.getRegeneration();
            if (monsterHealth > monster.getHealth()) {
                monsterHealth = monster.getHealth();
            }
        }
        return dmg;
    }

    private int monsterIsAttacking(Monster monster) {
        float mod = 1f - monster.getPenetration();
        if (data.getBuffs().get(Const.BLESS) > 0) {
            mod += 0.2f;
        }
        int dmg = monster.getAttack() - Math.round(data.getStats().get(Const.DEFENSE) * mod);
        if (data.getBuffs().get(Const.CURSED) > 0) {
            dmg *= 2;
        }
        if (data.getBuffs().get(Const.BURNED) > 0) {
            dmg += data.getMaxFloor() * 2;
        }
        playerMana -= monster.getManaDrain();
        if (dmg < 0) {
            dmg = 0;
        }
        return dmg;
    }

    private int playerIsAttacking(Monster monster) {
        float mod = 1f;
        if (data.getBuffs().get(Const.BLESS) > 0) {
            mod += 0.2f;
        }
        int dmg = Math.round(data.getStats().get(Const.ATTACK) * mod) - monster.getDefense();
        if (dmg < 0) {
            dmg = 0;
        }
        return dmg;
    }

    private boolean checkIfPlayerIsFirst(Monster monster) {
        return data.getBuffs().get(Const.INSTANT_STRIKE) > 0 || !monster.isFirstStrike();
    }

    private void killPlayer(final InPlay inPlay) {
        inPlay.getControl().setLockMovement(true);
        int zone = Const.zone(data.getCurrentFloor());
        Dialog dialog = new Dialog("", AtlasManager.getInstance().getDialogSkinsSmall().get(zone - 1)) {
            public void result(Object object) {
                if (object.equals(true)) {
                    SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                    Timer timer = new Timer();
                    Timer.Task task = new Timer.Task() {
                        @Override
                        public void run() {
                            inPlay.getMain().getStateMachine().changeState(GameState.MAIN_MENU);
                            inPlay.getDraw().getPlayer().resetPlayer();
                            data.resetData();
                            inPlay.getControl().setLockMovement(false);
                        }
                    };
                    timer.scheduleTask(task, 1);
                }
            }
        };
        dialog.text(Message.DIALOG_DEAD);
        dialog.button(Message.DIALOG_CONFIRM, true);
        dialog.show(inPlay.getStage());
        inPlay.getDraw().getPlayer().setDying(true);
        inPlay.getDraw().getPlayer().setChangeAnimation(true);
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.SCREAM);
    }
}
