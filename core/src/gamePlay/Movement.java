package gamePlay;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import control.ControlInPlay;
import gameObjects.Content;
import gameObjects.actors.Monster;
import gameObjects.actors.Npc;
import gameObjects.pickUps.Gem;
import gameObjects.pickUps.Item;
import gameObjects.pickUps.Key;
import gameObjects.pickUps.Potion;
import gameObjects.terrain.Door;
import gameObjects.terrain.Trap;
import gameObjects.terrain.Wall;
import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

import static globalVariables.Message.DIALOG_CANT_ATTACK;

public class Movement {

    private InPlay inPlay;
    private PlayerData data;

    public Movement(InPlay inPlay) {
        this.inPlay = inPlay;
        data = PlayerData.getInstance();
    }

    public void move(ControlInPlay.Direction direction) {
        boolean moved = false;
        int curX = data.getPosX();
        int curY = data.getPosY();
        int newX = curX;
        int newY = curY;
        switch (direction) {
            case LEFT:
                newX = curX - 1;
                if (checkMapEnd(newX, curY) && checkCanWalk(newX, curY) && checkTrap(curX, curY, direction)/*&& checkWall(newX, curY) && checkNpc(newX, curY) && checkDoor(newX, curY) && checkTrap(curX, curY, direction) && checkMonster(newX, curY)*/) {
                    data.setPosX(newX);
                    moved = true;
                }
                break;
            case RIGHT:
                newX = curX + 1;
                if (checkMapEnd(newX, curY) && checkCanWalk(newX, curY) && checkTrap(curX, curY, direction)/*&& checkWall(newX, curY) && checkNpc(newX, curY) && checkDoor(newX, curY) && checkTrap(curX, curY, direction) && checkMonster(newX, curY)*/) {
                    data.setPosX(newX);
                    moved = true;
                }
                break;
            case UP:
                newY = curY + 1;
                if (checkMapEnd(curX, newY) && checkCanWalk(curX, newY) && checkTrap(curX, curY, direction)/*&& checkWall(curX, newY) && checkNpc(curX, newY) && checkDoor(curX, newY) && checkTrap(curX, curY, direction) && checkMonster(curX, newY)*/) {
                    data.setPosY(newY);
                    moved = true;
                }
                break;
            case DOWN:
                newY = curY - 1;
                if (checkMapEnd(curX, newY) && checkCanWalk(curX, newY) && checkTrap(curX, curY, direction)/*&& checkWall(curX, newY) && checkNpc(curX, newY) && checkDoor(curX, newY) && checkTrap(curX, curY, direction) && checkMonster(curX, newY)*/) {
                    data.setPosY(newY);
                    moved = true;
                }
                break;
        }

        if (moved) {
            SoundEffect.getInstance().playSound(SoundEffect.Sounds.MOVE);
            if (data.getCurrentMap()[newX][newY].interact(inPlay)) {
                removeObject(newX, newY);
            }
        } else {
            if (checkMapEnd(newX, newY)) {
                if (!checkWall(newX, newY)) {
                    SoundEffect.getInstance().playSound(SoundEffect.Sounds.WALL);
                } else {
                    if (!checkNpc(newX, newY)) {
                        data.getCurrentMap()[newX][newY].interact(inPlay);
                    } else if (!checkDoor(newX, newY)) {
                        if (((Door) data.getCurrentMap()[newX][newY]).getType() == Door.Type.IRON) {
                            SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLOSED_DOOR);
                            inPlay.showConfirmDialog(Message.DIALOG_IRON_DOOR_LOCKED, Const.zone(data.getCurrentFloor()) - 1);
                        } else {
                            SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLOSED_DOOR);
                            inPlay.showConfirmDialog(Message.DIALOG_NO_KEY, Const.zone(data.getCurrentFloor()) - 1);
                        }
                    } else if (!checkMonster(newX, newY)) {
                        if (canAttackMonster(((Monster) data.getCurrentMap()[newX][newY]))) {
                            SoundEffect sound = SoundEffect.getInstance();
                            sound.playSound(SoundEffect.Sounds.ATTACK);
                            boolean removeMonster = data.getCurrentMap()[newX][newY].interact(inPlay);
                            inPlay.getDraw().getPlayer().setAttacking(true);
                            inPlay.getDraw().getPlayer().setChangeAnimation(true);
                            inPlay.getDraw().startBloodAnimation(newX, newY, this, removeMonster);
                        } else {
                            inPlay.showConfirmDialog(DIALOG_CANT_ATTACK, Const.zone(data.getCurrentFloor()) - 1);
                        }
                    } else if (checkPickUp(newX, newY) && !(data.getCurrentMap()[newX][newY] instanceof Trap)) {
                        if (data.getCurrentMap()[newX][newY].interact(inPlay)) {
                            removeObject(newX, newY);
                        }
                    }
                }
            } else {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.WALL);
            }
        }
    }

    public void removeObject(int x, int y) {
        data.getCurrentMap()[x][y] = new Content();
        inPlay.getDraw().getImages()[x][y].addAction(Actions.fadeOut(0.6f));
    }

    private boolean checkMapEnd(int X, int Y) {
        return X >= 0 && X < Const.MAP_X && Y >= 0 && Y < Const.MAP_Y;
    }

    private boolean checkWall(int X, int Y) {
        return !(data.getCurrentMap()[X][Y] instanceof Wall);
    }

    private boolean checkPickUp(int X, int Y) {
        return !(data.getCurrentMap()[X][Y] instanceof Item) || !(data.getCurrentMap()[X][Y] instanceof Key) ||
                !(data.getCurrentMap()[X][Y] instanceof Potion) || !(data.getCurrentMap()[X][Y] instanceof Gem);
    }

    private boolean checkNpc(int X, int Y) {
        return !(data.getCurrentMap()[X][Y] instanceof Npc);
    }

    private boolean checkDoor(int X, int Y) {
        int amount = 0;
        if (data.getCurrentMap()[X][Y] instanceof Door) {
            Door.Type type = ((Door) data.getCurrentMap()[X][Y]).getType();
            switch (type) {
                case BRONZE:
                    amount = data.getKeys().get(Const.KEY_BRONZE);
                    break;
                case SILVER:
                    amount = data.getKeys().get(Const.KEY_SILVER);
                    break;
                case GOLD:
                    amount = data.getKeys().get(Const.KEY_GOLD);
                    break;
                case RED:
                    amount = data.getKeys().get(Const.KEY_RED);
                    break;
                case IRON:
                    int[] guard_1 = ((Door) data.getCurrentMap()[X][Y]).getGuard_1();
                    int[] guard_2 = ((Door) data.getCurrentMap()[X][Y]).getGuard_2();

                    return !(data.getCurrentMap()[guard_1[0]][guard_1[1]] instanceof Monster ||
                            data.getCurrentMap()[guard_2[0]][guard_2[1]] instanceof Monster);
            }
            return !(amount == 0);
        } else {
            return true;
        }
    }

    private boolean checkTrap(int X, int Y, ControlInPlay.Direction direction) {
        if (data.getCurrentMap()[X][Y] instanceof Trap) {
            Trap.Type type = ((Trap) data.getCurrentMap()[X][Y]).getType();
            switch (type) {
                case DOWN:
                    return direction == ControlInPlay.Direction.DOWN;
                case LEFT:
                    return direction == ControlInPlay.Direction.LEFT;
                case RIGHT:
                    return direction == ControlInPlay.Direction.RIGHT;
                case UP:
                    return direction == ControlInPlay.Direction.UP;
                default:
                    return true;
            }
        } else {
            return true;
        }
    }

    private boolean checkCanWalk(int X, int Y) {
        Content obj = data.getCurrentMap()[X][Y];

        return !(obj instanceof Wall || obj instanceof Monster || obj instanceof Npc || obj instanceof Gem ||
                obj instanceof Item || obj instanceof Key || obj instanceof Potion || obj instanceof Door);
    }

    private boolean checkMonster(int X, int Y) {
        return !(data.getCurrentMap()[X][Y] instanceof Monster);
    }

    private boolean canAttackMonster(Monster monster) {
        boolean canAttack = true;
        int dmg = playerDamageOutput(monster);
        if (dmg <= 0) {
            canAttack = false;
        }
        if (monster.getRegeneration() >= dmg && dmg < monster.getHealth()) {
            canAttack = false;
        }
        return canAttack;
    }

    private int playerDamageOutput(Monster monster) {
        float mod = 1f;
        if (data.getBuffs().get(Const.BLESS) > 0) {
            mod += 0.2f;
        }
        int dmg = Math.round(data.getStats().get(Const.ATTACK) * mod) - monster.getDefense();

        if (dmg < 0) {
            dmg = 0;
        }
        return dmg;
    }
}
