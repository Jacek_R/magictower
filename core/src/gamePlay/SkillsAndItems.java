package gamePlay;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import gameObjects.Content;
import gameObjects.actors.Monster;
import gameObjects.terrain.Door;
import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import stateMachine.GameState;
import userInterface.gameScreens.UseSkill;

public class SkillsAndItems {

    private UseSkill useSkill;
    private PlayerData data;

    public SkillsAndItems(UseSkill useSkill) {
        this.useSkill = useSkill;
        data = PlayerData.getInstance();
    }

    public void useSkill(int skillName, int skillCost, int skillDuration) {
        int mana = data.getStats().get(Const.MANA);
        if (mana < skillCost) {
            useSkill.showConfirmDialog(Message.DIALOG_NO_MANA, Const.zone(data.getCurrentFloor()) - 1);
        } else {
            data.getStats().set(Const.MANA, mana - skillCost);
            useSkill.getAmountMana().setText(Message.MANA + String.valueOf(mana - skillCost));
            if (skillName == Const.CLEANSE) {
                useCleanse();
            } else {
                data.getBuffs().set(skillName, skillDuration);
            }
            useSkill.getMain().getStateMachine().changeState(GameState.IN_PLAY);
        }
    }

    public void useItem(int itemName) {
        int amount = data.getItems().get(itemName);
        if (amount < 1) {
            useSkill.showConfirmDialog(Message.DIALOG_NO_ITEM, Const.zone(data.getCurrentFloor()) - 1);
        } else {
            data.getItems().set(itemName, amount - 1);
            setAmountLabel(itemName, amount - 1);
            if (itemName == Const.BOMB) {
                useBomb();
            } else if (itemName == Const.MASTER_KEY) {
                useKey();
            } else if (itemName == Const.POTION) {
                usePotion();
            }
            useSkill.getMain().getStateMachine().changeState(GameState.IN_PLAY);
        }
    }

    private void useCleanse() {
        int currentCurse = data.getBuffs().get(Const.CURSED);
        int currentPoison = data.getBuffs().get(Const.POISONED);
        int currentBurn = data.getBuffs().get(Const.BURNED);
        int currentSilence = data.getBuffs().get(Const.SILENCED);

        if (currentCurse > 0) {
            data.getBuffs().set(Const.CURSED, currentCurse - 1);
        }
        if (currentBurn > 0) {
            data.getBuffs().set(Const.BURNED, currentBurn - 1);
        }
        if (currentPoison > 0) {
            data.getBuffs().set(Const.POISONED, currentPoison - 1);
        }
        if (currentSilence > 0) {
            data.getBuffs().set(Const.SILENCED, currentSilence - 1);
        }
    }

    private void useBomb() {
        int x = data.getPosX();
        int y = data.getPosY();

        int[] left = {x - 1, y};
        int[] right = {x + 1, y};
        int[] up = {x, y - 1};
        int[] down = {x, y + 1};

        if (!(left[0] < 0)) {
            if (data.getCurrentMap()[left[0]][left[1]] instanceof Monster) {
                if (!((Monster) data.getCurrentMap()[left[0]][left[1]]).isBombResistant()) {
                    data.getCurrentMap()[left[0]][left[1]] = new Content();
                }
            }
        }

        if (!(right[0] > 10)) {
            if (data.getCurrentMap()[right[0]][right[1]] instanceof Monster) {
                if (!((Monster) data.getCurrentMap()[right[0]][right[1]]).isBombResistant()) {
                    data.getCurrentMap()[right[0]][right[1]] = new Content();
                }
            }
        }
        if (!(up[1] < 0)) {
            if (data.getCurrentMap()[up[0]][up[1]] instanceof Monster) {
                if (!((Monster) data.getCurrentMap()[up[0]][up[1]]).isBombResistant()) {
                    data.getCurrentMap()[up[0]][up[1]] = new Content();
                }
            }
        }
        if (!(down[1] > 10)) {
            if (data.getCurrentMap()[down[0]][down[1]] instanceof Monster) {
                if (!((Monster) data.getCurrentMap()[down[0]][down[1]]).isBombResistant()) {
                    data.getCurrentMap()[down[0]][down[1]] = new Content();
                }
            }
        }
    }

    private void useKey() {
        int x = 0;
        int y = 0;

        while (x < Const.MAP_X) {
            while (y < Const.MAP_Y) {
                if (data.getCurrentMap()[x][y] instanceof Door) {
                    Door.Type type = ((Door) data.getCurrentMap()[x][y]).getType();
                    if (type == Door.Type.BRONZE) {
                        data.getCurrentMap()[x][y] = new Content();
                    }
                }
                y++;
            }
            x++;
            y = 0;
        }
    }

    private void usePotion() {
        int healAmount = data.getMaxFloor() * Const.POTION_HEAL;
        int currentHealth = data.getStats().get(Const.HEALTH);

        data.getStats().set(Const.HEALTH, currentHealth + healAmount);
    }

    private void setAmountLabel(int item, int amount) {
        Label label;
        if (item == Const.BOMB) {
            label = useSkill.getAmount_bomb();
        } else if (item == Const.MASTER_KEY) {
            label = useSkill.getAmount_key();
        } else {
            label = useSkill.getAmount_potion();
        }
        label.setText(Message.AMOUNT + String.valueOf(amount));
    }
}
