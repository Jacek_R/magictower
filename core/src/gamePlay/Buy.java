package gamePlay;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.ArrayList;
import java.util.Map;

import globalVariables.Const;
import globalVariables.PlayerData;
import userInterface.gameScreens.InPlay;

public class Buy {

    private static final int KEY_BRONZE = 1;
    private static final int KEY_SILVER = 2;
    private static final int KEY_GOLD = 3;
    private static final int KEY_RED = 4;

    private static final int ATTACK = 5;
    private static final int DEFENSE = 6;
    private static final int HEALTH = 7;
    private static final int MANA = 8;

    private static final int EXPERIENCE = 9;
    private static final int BOMB = 10;
    private static final int MASTER_KEY = 11;
    private static final int POTION = 12;

    private static final int GOLD = 13;

    public void item(int itemToBuy, int amount, int price, InPlay inPlay) {
        PlayerData data = PlayerData.getInstance();
        addItem(data.getStats(), Const.GOLD, -price, inPlay.getGold());

        switch (itemToBuy) {
            case KEY_BRONZE:
                addItem(data.getKeys(), Const.KEY_BRONZE, amount, inPlay.getKeys_bronze());
                break;
            case KEY_SILVER:
                addItem(data.getKeys(), Const.KEY_SILVER, amount, inPlay.getKeys_silver());
                break;
            case KEY_GOLD:
                addItem(data.getKeys(), Const.KEY_GOLD, amount, inPlay.getKeys_gold());
                break;
            case KEY_RED:
                addItem(data.getKeys(), Const.KEY_RED, amount, inPlay.getKeys_red());
                break;
            case ATTACK:
                addItem(data.getStats(), Const.ATTACK, amount, inPlay.getAttack());
                break;
            case DEFENSE:
                addItem(data.getStats(), Const.DEFENSE, amount, inPlay.getDefense());
                break;
            case HEALTH:
                addItem(data.getStats(), Const.HEALTH, amount, inPlay.getHealth());
                break;
            case MANA:
                addItem(data.getStats(), Const.MANA, amount, inPlay.getMana());
                break;
            case EXPERIENCE:
                addItem(data.getStats(), Const.EXP, amount, inPlay.getExperience());
                break;
            case BOMB:
                addItem(data.getItems(), Const.BOMB, amount, null);
                break;
            case MASTER_KEY:
                addItem(data.getItems(), Const.MASTER_KEY, amount, null);
                break;
            case POTION:
                addItem(data.getItems(), Const.POTION, amount, null);
                break;
            case GOLD:
                addItem(data.getStats(), Const.GOLD, amount, inPlay.getGold());
                break;
        }
    }

//    private void addKey(ArrayList<Integer> array, int color, int additionalAmount, Label label) {
//        int summedAmount = array.get(color) + additionalAmount;
//        array.set(color, summedAmount);
//        if (label != null) {
//            label.setText(String.valueOf(summedAmount));
//        }
//    }
//
//    private void addStat(ArrayList<Integer> array, int type, int additionalAmount, Label label) {
//        int summedAmount = array.get(type) + additionalAmount;
//        array.set(type, summedAmount);
//        if (label != null) {
//            label.setText(String.valueOf(summedAmount));
//        }
//    }

    private void addItem(ArrayList<Integer> array, int type, int additionalAmount, Label label) {
        int summedAmount = array.get(type) + additionalAmount;
        array.set(type, summedAmount);
        if (label != null) {
            label.setText(String.valueOf(summedAmount));
        }
    }

//    private void addItem(Map<String, Integer> map, String key, int additionalAmount, Label label) {
//        int summedAmount = map.get(key) + additionalAmount;
//        map.put(key, summedAmount);
//        if (label != null) {
//            label.setText(String.valueOf(summedAmount));
//        }
//    }
}
