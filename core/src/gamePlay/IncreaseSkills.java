package gamePlay;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import userInterface.gameScreens.BuySkill;

public class IncreaseSkills {

    private BuySkill buySkill;
    private PlayerData data;

    public IncreaseSkills(BuySkill buySkill) {
        this.buySkill = buySkill;
        data = PlayerData.getInstance();
    }

    public void buyStat(int stat, int cost, double baseIncrease, Label statValue) {
        int skillValue = data.getStats().get(stat);
        int expValue = data.getStats().get(Const.EXP);

        if (expValue >= cost) {
            data.getStats().set(Const.EXP, expValue - cost);
            data.getStats().set(stat, skillValue + calculateStatIncrease(data.getStatsBought(), baseIncrease));
            statValue.setText(String.valueOf(skillValue + calculateStatIncrease(data.getStatsBought(), baseIncrease)));
            int statsBoughtNew = data.getStatsBought();
            data.setStatsBought(statsBoughtNew + 1);
            buySkill.getCost_stat_value().setText(String.valueOf(calculateStatCost(data.getStatsBought())));
            buySkill.getExperience_value().setText(String.valueOf(expValue - cost));
            updateStatsIncrease();
        } else {
            buySkill.showConfirmDialog(Message.DIALOG_NO_EXP, Const.zone(data.getCurrentFloor()) - 1);
        }
    }

    public void buySkill(int skill, String skillRegion, int cost, Image image) {
        int expValue = data.getStats().get(Const.EXP);
        boolean haveSkill = data.getSkills().get(skill);

        if (expValue >= cost && !haveSkill) {
            data.getStats().set(Const.EXP, expValue - cost);
            data.getSkills().set(skill, true);
            int skillsBought = data.getSkillsBought();
            data.setSkillsBought(skillsBought + 1);
            buySkill.getCost_skill_value().setText(String.valueOf(calculateSkillCost(data.getSkillsBought())));
            buySkill.getExperience_value().setText(String.valueOf(expValue - cost));
            image.setDrawable(new SpriteDrawable(new Sprite(buySkill.findImage(skill, skillRegion))));
        } else if (haveSkill) {
            buySkill.showConfirmDialog(Message.DIALOG_SKILL_MAXED, Const.zone(data.getCurrentFloor()) - 1);
        } else {
            buySkill.showConfirmDialog(Message.DIALOG_NO_EXP, Const.zone(data.getCurrentFloor()) - 1);
        }
    }

    private void updateStatsIncrease() {
        buySkill.getAttack_increase_value().setText(
                String.valueOf(calculateStatIncrease(data.getStatsBought(), Const.ATK_BASE_INCREASE))
        );
        buySkill.getDefense_increase_value().setText(
                String.valueOf(calculateStatIncrease(data.getStatsBought(), Const.DEF_BASE_INCREASE))
        );
        buySkill.getHealth_increase_value().setText(
                String.valueOf(calculateStatIncrease(data.getStatsBought(), Const.HEALTH_BASIC_INCREASE))
        );
        buySkill.getMana_increase_value().setText(
                String.valueOf(calculateStatIncrease(data.getStatsBought(), Const.MANA_BASIC_INCREASE))
        );
    }

    public int calculateStatIncrease(int boughtStats, double baseIncrease) {
        int h = 0;
        while (h < boughtStats) {
            baseIncrease = baseIncrease * Const.STAT_INCREASE;
            h++;
        }
        return (int) baseIncrease;
    }

    public int calculateSkillCost(int boughtSkills) {
        double cost = Const.BASIC_SKILL_COST;
        int m = 0;

        while (m < boughtSkills) {
            cost = cost * Const.SKILL_COST_INCREASE;
            m++;
        }
        return (int) cost;
    }

    public int calculateStatCost(int boughtStats) {
        double cost = Const.BASIC_STAT_COST;
        int m = 0;

        while (m < boughtStats) {
            cost = cost * Const.STAT_COST_INCREASE;
            m++;
        }
        return (int) cost;
    }
}
