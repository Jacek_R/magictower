package control;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import gameObjects.actors.Player;
import gamePlay.Movement;
import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.gameScreens.InPlay;

public class ControlInPlay implements InputProcessor {

    private InPlay inPlay;
    private boolean lockMovement;

    public enum Direction {
        LEFT, RIGHT, UP, DOWN
    }

    private ClickListener left;
    private ClickListener right;
    private ClickListener up;
    private ClickListener down;
    private ClickListener options;
    private ClickListener save;
    private ClickListener load;
    private ClickListener help;
    private ClickListener prediction;
    private ClickListener fly;
    private ClickListener buySkill;
    private ClickListener useSkill;

    public ControlInPlay(InPlay inPlay) {
        this.inPlay = inPlay;
        lockMovement = false;
        left = listenerLeft();
        right = listenerRight();
        up = listenerUp();
        down = listenerDown();
        options = listenerOptions();
        save = listenerSave();
        load = listenerLoad();
        help = listenerHelp();
        prediction = listenerPrediction();
        fly = listenerFly();
        buySkill = listenerBuySkill();
        useSkill = listenerUseSkill();
    }

    private ClickListener listenerLeft() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                left();
            }
        };
    }

    private ClickListener listenerRight() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                right();
            }
        };
    }

    private ClickListener listenerUp() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                up();
            }
        };
    }

    private ClickListener listenerDown() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                down();
            }
        };
    }

    private ClickListener listenerOptions() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.OPTIONS);
            }
        };
    }

    private ClickListener listenerHelp() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.HELP);
            }
        };
    }

    private ClickListener listenerSave() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.SAVE_GAME);
            }
        };
    }

    private ClickListener listenerLoad() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.LOAD_GAME);
            }
        };
    }

    private ClickListener listenerPrediction() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.PREDICTION);
            }
        };
    }

    private ClickListener listenerFly() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.FLY);
            }
        };
    }

    private ClickListener listenerBuySkill() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.BUY_SKILL);
            }
        };
    }

    private ClickListener listenerUseSkill() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                inPlay.getMain().getStateMachine().changeState(GameState.USE_SKILL);
            }
        };
    }

    private void left() {
        Player.Direction direction = inPlay.getDraw().getPlayer().getDir();
        if (!inPlay.getDraw().isAnimateBlood() && !lockMovement) {
            movement(Direction.LEFT);
            if (direction != Player.Direction.LEFT) {
                inPlay.getDraw().getPlayer().setDir(Player.Direction.LEFT);
                inPlay.getDraw().getPlayer().setChangeAnimation(true);
            }
        }
    }

    private void right() {
        Player.Direction direction = inPlay.getDraw().getPlayer().getDir();
        if (!inPlay.getDraw().isAnimateBlood() && !lockMovement) {
            movement(Direction.RIGHT);
            if (direction != Player.Direction.RIGHT) {
                inPlay.getDraw().getPlayer().setDir(Player.Direction.RIGHT);
                inPlay.getDraw().getPlayer().setChangeAnimation(true);
            }
        }
    }

    private void up() {
        Player.Direction direction = inPlay.getDraw().getPlayer().getDir();
        if (!inPlay.getDraw().isAnimateBlood() && !lockMovement) {
            movement(Direction.UP);
            if (direction != Player.Direction.UP) {
                inPlay.getDraw().getPlayer().setDir(Player.Direction.UP);
                inPlay.getDraw().getPlayer().setChangeAnimation(true);
            }
        }
    }

    private void down() {
        Player.Direction direction = inPlay.getDraw().getPlayer().getDir();
        if (!inPlay.getDraw().isAnimateBlood() && !lockMovement) {
            movement(Direction.DOWN);
            if (direction != Player.Direction.DOWN) {
                inPlay.getDraw().getPlayer().setDir(Player.Direction.DOWN);
                inPlay.getDraw().getPlayer().setChangeAnimation(true);
            }
        }
    }

    private void movement(Direction direction) {
        Movement move = new Movement(inPlay);
        move.move(direction);
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.LEFT:
                left();
                break;
            case Input.Keys.RIGHT:
                right();
                break;
            case Input.Keys.UP:
                up();
                break;
            case Input.Keys.DOWN:
                down();
                break;
            case Input.Keys.Y:
                inPlay.getMain().getStateMachine().changeState(GameState.SAVE_GAME);
                break;
            case Input.Keys.U:
                inPlay.getMain().getStateMachine().changeState(GameState.LOAD_GAME);
                break;
            case Input.Keys.I:
                inPlay.getMain().getStateMachine().changeState(GameState.HELP);
                break;
            case Input.Keys.O:
                inPlay.getMain().getStateMachine().changeState(GameState.OPTIONS);
                break;
            case Input.Keys.H:
                inPlay.getMain().getStateMachine().changeState(GameState.PREDICTION);
                break;
            case Input.Keys.J:
                inPlay.getMain().getStateMachine().changeState(GameState.FLY);
                break;
            case Input.Keys.K:
                inPlay.getMain().getStateMachine().changeState(GameState.BUY_SKILL);
                break;
            case Input.Keys.L:
                inPlay.getMain().getStateMachine().changeState(GameState.USE_SKILL);
                break;
            case Input.Keys.ESCAPE:
                quitDialog(Message.DIALOG_QUIT);
                break;
        }
        return false;
    }

    private void quitDialog(String message) {
        int zone = Const.zone(PlayerData.getInstance().getCurrentFloor());
        Dialog dialog = new Dialog("", AtlasManager.getInstance().getDialogSkinsSmall().get(zone - 1)) {
            @Override
            protected void result(Object object) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                if (((Boolean) object)) {
                    Gdx.app.exit();
                }
            }
        };
        dialog.text(message);
        dialog.key(Input.Keys.SPACE, false);
        dialog.key(Input.Keys.ENTER, false);
        dialog.button(Message.DIALOG_CONFIRM, true);
        dialog.button(Message.DIALOG_CANCEL, false);
        dialog.show(inPlay.getStageForDialog());
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        switch (character) {
            case 'a':
                left();
                break;
            case 'd':
                right();
                break;
            case 'w':
                up();
                break;
            case 's':
                down();
                break;
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


    public ClickListener getLeft() {
        return left;
    }

    public ClickListener getRight() {
        return right;
    }

    public ClickListener getUp() {
        return up;
    }

    public ClickListener getDown() {
        return down;
    }

    public ClickListener getOptions() {
        return options;
    }

    public ClickListener getSave() {
        return save;
    }

    public ClickListener getLoad() {
        return load;
    }

    public ClickListener getHelp() {
        return help;
    }

    public ClickListener getPrediction() {
        return prediction;
    }

    public ClickListener getFly() {
        return fly;
    }

    public ClickListener getBuySkill() {
        return buySkill;
    }

    public ClickListener getUseSkill() {
        return useSkill;
    }

    public void setLockMovement(boolean lockMovement) {
        this.lockMovement = lockMovement;
    }
}
