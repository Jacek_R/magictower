package control;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import gamePlay.IncreaseSkills;
import globalVariables.Const;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.gameScreens.BuySkill;

public class ControlBuySkill {

    private BuySkill buySkill;
    private IncreaseSkills increaseSkills;

    private ClickListener buyAttack;
    private ClickListener buyDefense;
    private ClickListener buyHealth;
    private ClickListener buyMana;

    private ClickListener buyBless;
    private ClickListener buyCleanse;
    private ClickListener buyInstant;
    private ClickListener buyProtect;
    private ClickListener buyVampire;
    private ClickListener closeButton;

    public ControlBuySkill(BuySkill buySkill) {
        this.buySkill = buySkill;
        increaseSkills = new IncreaseSkills(buySkill);

        buyAttack = listenerBuyAttack();
        buyDefense = listenerBuyDefense();
        buyHealth = listenerBuyHealth();
        buyMana = listenerBuyMana();

        buyBless = listenerBuyBless();
        buyCleanse = listenerBuyCleanse();
        buyInstant = listenerBuyInstant();
        buyProtect = listenerBuyProtect();
        buyVampire = listenerBuyVampire();
        closeButton = listenerClose();
    }

    private ClickListener listenerBuyAttack() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buyStat(
                        Const.ATTACK, Integer.parseInt(buySkill.getCost_stat_value().getText().toString()),
                        Const.ATK_BASE_INCREASE, buySkill.getAttack_value()
                );
            }
        };
    }

    private ClickListener listenerBuyDefense() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buyStat(
                        Const.DEFENSE, Integer.parseInt(buySkill.getCost_stat_value().getText().toString()),
                        Const.DEF_BASE_INCREASE, buySkill.getDefense_value()
                );
            }
        };
    }

    private ClickListener listenerBuyHealth() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buyStat(
                        Const.HEALTH, Integer.parseInt(buySkill.getCost_stat_value().getText().toString()),
                        Const.HEALTH_BASIC_INCREASE, buySkill.getHealth_value()
                );
            }
        };
    }

    private ClickListener listenerBuyMana() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buyStat(
                        Const.MANA, Integer.parseInt(buySkill.getCost_stat_value().getText().toString()),
                        Const.MANA_BASIC_INCREASE, buySkill.getMana_value()
                );
            }
        };
    }

    private ClickListener listenerBuyBless() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buySkill(
                        Const.BLESS, Const.REGION_BLESS, Integer.parseInt(buySkill.getCost_skill_value().getText().toString()),
                        buySkill.getBless()
                );
            }
        };
    }

    private ClickListener listenerBuyCleanse() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buySkill(
                        Const.CLEANSE, Const.REGION_CLEANSE, Integer.parseInt(buySkill.getCost_skill_value().getText().toString()),
                        buySkill.getCleanse()
                );
            }
        };
    }

    private ClickListener listenerBuyInstant() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buySkill(
                        Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE, Integer.parseInt(buySkill.getCost_skill_value().getText().toString()),
                        buySkill.getInstant()
                );
            }
        };
    }

    private ClickListener listenerBuyProtect() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buySkill(
                        Const.PROTECT, Const.REGION_PROTECT, Integer.parseInt(buySkill.getCost_skill_value().getText().toString()),
                        buySkill.getProtect()
                );
            }
        };
    }

    private ClickListener listenerBuyVampire() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                increaseSkills.buySkill(
                        Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE, Integer.parseInt(buySkill.getCost_skill_value().getText().toString()),
                        buySkill.getVampire()
                );
            }
        };
    }

    private ClickListener listenerClose() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                buySkill.getMain().getStateMachine().changeState(GameState.IN_PLAY);
            }
        };
    }

    public ClickListener getBuyAttack() {
        return buyAttack;
    }

    public ClickListener getBuyDefense() {
        return buyDefense;
    }

    public ClickListener getBuyHealth() {
        return buyHealth;
    }

    public ClickListener getBuyMana() {
        return buyMana;
    }

    public ClickListener getBuyBless() {
        return buyBless;
    }

    public ClickListener getBuyCleanse() {
        return buyCleanse;
    }

    public ClickListener getBuyInstant() {
        return buyInstant;
    }

    public ClickListener getBuyProtect() {
        return buyProtect;
    }

    public ClickListener getBuyVampire() {
        return buyVampire;
    }

    public ClickListener getCloseButton() {
        return closeButton;
    }

    public IncreaseSkills getIncreaseSkills() {
        return increaseSkills;
    }
}
