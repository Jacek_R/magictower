package control;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import gamePlay.SkillsAndItems;
import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.gameScreens.UseSkill;

public class ControlUseSkill {

    private UseSkill useSkill;
    private SkillsAndItems skillsAndItems;

    private ClickListener instantStrike;
    private ClickListener cleanse;
    private ClickListener protect;
    private ClickListener bless;
    private ClickListener vampire;
    private ClickListener potion;
    private ClickListener key;
    private ClickListener bomb;
    private ClickListener cancel;

    private boolean canUseSkills;

    public ControlUseSkill(UseSkill useSkill) {
        this.useSkill = useSkill;
        skillsAndItems = new SkillsAndItems(useSkill);

        instantStrike = listenerInstantStrike();
        cleanse = listenerCleanse();
        protect = listenerProtect();
        bless = listenerBless();
        vampire = listenerVampire();
        potion = listenerPotion();
        key = listenerKey();
        bomb = listenerBomb();
        cancel = listenerCancel();

        canUseSkills = !(PlayerData.getInstance().getBuffs().get(Const.SILENCED) > 0);
    }

    private ClickListener listenerInstantStrike() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                checkIfSkillIsUsable(Const.INSTANT_STRIKE, Const.INSTANT_STRIKE_COST, Const.INSTANT_STRIKE_DURATION);
            }
        };
    }

    private ClickListener listenerProtect() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                checkIfSkillIsUsable(Const.PROTECT, Const.PROTECT_COST, Const.PROTECT_DURATION);
            }
        };
    }

    private ClickListener listenerBless() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                checkIfSkillIsUsable(Const.BLESS, Const.BLESS_COST, Const.BLESS_DURATION);
            }
        };
    }

    private ClickListener listenerVampire() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                checkIfSkillIsUsable(Const.VAMPIRE_EDGE, Const.VAMPIRE_COST, Const.VAMPIRE_DURATION);
            }
        };
    }

    private ClickListener listenerCleanse() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                checkIfSkillIsUsable(Const.CLEANSE, Const.CLEANSE_COST, Const.CLEANSE_DURATION);
            }
        };
    }

    private ClickListener listenerKey() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                skillsAndItems.useItem(Const.MASTER_KEY);
            }
        };
    }

    private ClickListener listenerPotion() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                skillsAndItems.useItem(Const.POTION);
            }
        };
    }

    private ClickListener listenerBomb() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                skillsAndItems.useItem(Const.BOMB);
            }
        };
    }

    private ClickListener listenerCancel() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                useSkill.getMain().getStateMachine().changeState(GameState.IN_PLAY);
            }
        };
    }

    private void checkIfSkillIsUsable(int skillName, int skillCost, int skillDuration) {
        PlayerData data = PlayerData.getInstance();
        boolean haveThisSkill = data.getSkills().get(skillName);
        boolean skillIsActive;
        if (skillName == Const.CLEANSE) {
            skillIsActive = (data.getBuffs().get(Const.POISONED) == 0) && (data.getBuffs().get(Const.BURNED) == 0) && (data.getBuffs().get(Const.CURSED) == 0);
        } else {
            skillIsActive = data.getBuffs().get(skillName) > 0;
        }
        if (canUseSkills && haveThisSkill && !skillIsActive) {
            skillsAndItems.useSkill(skillName, skillCost, skillDuration);
        } else {
            if (!canUseSkills) {
                useSkill.showConfirmDialog(Message.DIALOG_MESSAGE_SILENCE, Const.zone(data.getCurrentFloor()) - 1);
            } else if (!haveThisSkill) {
                useSkill.showConfirmDialog(Message.DIALOG_MESSAGE_DONT_HAVE_SKILL, Const.zone(data.getCurrentFloor()) - 1);
            } else {
                if (skillName == Const.CLEANSE) {
                    useSkill.showConfirmDialog(Message.DIALOG_CLEANSE_NOT_NECESSARY, Const.zone(data.getCurrentFloor()) - 1);
                } else {
                    useSkill.showConfirmDialog(Message.DIALOG_SKILL_IS_IN_EFFECT, Const.zone(data.getCurrentFloor()) - 1);
                }
            }
        }
    }

    public ClickListener getCancel() {
        return cancel;
    }

    public ClickListener getBomb() {
        return bomb;
    }

    public ClickListener getKey() {
        return key;
    }

    public ClickListener getPotion() {
        return potion;
    }

    public ClickListener getVampire() {
        return vampire;
    }

    public ClickListener getBless() {
        return bless;
    }

    public ClickListener getProtect() {
        return protect;
    }

    public ClickListener getCleanse() {
        return cleanse;
    }

    public ClickListener getInstantStrike() {
        return instantStrike;
    }
}
