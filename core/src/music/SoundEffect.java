package music;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;

import utils.DataManager;

public class SoundEffect {

    private Sound attack;
    private Sound move;
    private Sound stairs;
    private Sound key;
    private Sound openDoor;
    private Sound scream;
    private Sound dieMonster;
    private Sound gem;
    private Sound potion;
    private Sound item;
    private Sound trap;
    private Sound closedDoor;
    private Sound wall;
    private Sound buy;
    private Sound click;
    private float volume;

    public enum Sounds {
        ATTACK, MOVE, STAIRS, KEY, OPEN_DOOR, SCREAM, DIE_MONSTER, POTION, GEM, ITEM, TRAP, WALL, BUY, CLICK, CLOSED_DOOR
    }

    private static SoundEffect instance;

    private SoundEffect() {
        loadSounds();
        if (prefExist("options", "sound")) {
            volume = DataManager.loadInteger(DataManager.loadPreferences("options", "sound")) / 10f;
        } else {
            volume = 1f;
            DataManager.savePreferences("options", "music", DataManager.save(10));
            DataManager.savePreferences("options", "sound", DataManager.save(10));
        }
    }

    public static SoundEffect getInstance() {
        if (instance == null) {
            instance = new SoundEffect();
        }
        return instance;
    }

    private boolean prefExist(String prefname, String keyInPrefName) {
        Preferences pref = Gdx.app.getPreferences(prefname);
        String x = pref.getString(keyInPrefName);
        return !x.isEmpty();
    }

    private void loadSounds() {
        attack = Gdx.audio.newSound(Gdx.files.internal("sound/stab.wav"));
        move = Gdx.audio.newSound(Gdx.files.internal("sound/walk.wav"));
        stairs = Gdx.audio.newSound(Gdx.files.internal("sound/stairs.wav"));
        key = Gdx.audio.newSound(Gdx.files.internal("sound/pickKey.wav"));
        openDoor = Gdx.audio.newSound(Gdx.files.internal("sound/openDoor.wav"));
        closedDoor = Gdx.audio.newSound(Gdx.files.internal("sound/closedDoor.wav"));
        dieMonster = Gdx.audio.newSound(Gdx.files.internal("sound/dieMonster.wav"));
        scream = Gdx.audio.newSound(Gdx.files.internal("sound/scream.wav"));
        gem = Gdx.audio.newSound(Gdx.files.internal("sound/pickGem.wav"));
        item = Gdx.audio.newSound(Gdx.files.internal("sound/pickItem.wav"));
        trap = Gdx.audio.newSound(Gdx.files.internal("sound/trap.wav"));
        wall = Gdx.audio.newSound(Gdx.files.internal("sound/wall.wav"));
        click = Gdx.audio.newSound(Gdx.files.internal("sound/click.wav"));
        potion = Gdx.audio.newSound(Gdx.files.internal("sound/pickPotion.wav"));
        buy = Gdx.audio.newSound(Gdx.files.internal("sound/buy.wav"));
    }

    public void playSound(Sounds id) {
        switch (id) {
            case ATTACK:
                attack.play(volume);
                break;
            case MOVE:
                move.play(volume);
                break;
            case STAIRS:
                stairs.play(volume);
                break;
            case KEY:
                key.play(volume);
                break;
            case OPEN_DOOR:
                openDoor.play(volume);
                break;
            case SCREAM:
                scream.play(volume);
                break;
            case DIE_MONSTER:
                dieMonster.play(volume);
                break;
            case POTION:
                potion.play(volume);
                break;
            case GEM:
                gem.play(volume);
                break;
            case ITEM:
                item.play(volume);
                break;
            case TRAP:
                trap.play(volume);
                break;
            case WALL:
                wall.play(volume);
                break;
            case BUY:
                buy.play(volume);
                break;
            case CLICK:
                click.play(volume);
                break;
            case CLOSED_DOOR:
                closedDoor.play(volume);
                break;
        }
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }
}
