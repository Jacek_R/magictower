package music;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;

import java.util.ArrayList;

import utils.DataManager;

public class BackgroundMusic {

    private int currentlyPlayed;
    private ArrayList<Music> songs;

    private static final String BGM_1 = "sound/cycles.ogg";
    private static final String BGM_2 = "sound/darkerHeart.ogg";
    private static final String BGM_3 = "sound/cycles.ogg";
    private static final String BGM_4 = "sound/cycles.ogg";
    private static final String BGM_5 = "sound/cycles.ogg";
    private static final String TITLE = "sound/epicTvTheme.ogg";

    private static BackgroundMusic instance;

    private BackgroundMusic() {
        currentlyPlayed = -1;
        songs = new ArrayList<Music>();

        songs.add(Gdx.audio.newMusic(Gdx.files.internal(TITLE)));
        songs.add(Gdx.audio.newMusic(Gdx.files.internal(BGM_1)));
        songs.add(Gdx.audio.newMusic(Gdx.files.internal(BGM_2)));
        songs.add(Gdx.audio.newMusic(Gdx.files.internal(BGM_3)));
        songs.add(Gdx.audio.newMusic(Gdx.files.internal(BGM_4)));
        songs.add(Gdx.audio.newMusic(Gdx.files.internal(BGM_5)));

        for (Music song : songs) {
            song.setLooping(true);
        }
    }

    public static BackgroundMusic getInstance() {
        if (instance == null) {
            instance = new BackgroundMusic();
        }
        return instance;
    }

    public void initializeSong(int song) {
        if (song != currentlyPlayed) {
            if (currentlyPlayed != -1) {
                songs.get(currentlyPlayed).stop();
                songs.get(currentlyPlayed).dispose();
            }
            currentlyPlayed = song;
            volume();
            songs.get(song).play();
        }
    }

    private void volume() {
        float volume;
        if (prefExist("options", "music")) {
            volume = DataManager.loadInteger(DataManager.loadPreferences("options", "music")) / 10f;
        } else {
            volume = 1f;
            DataManager.savePreferences("options", "music", DataManager.save(10));
            DataManager.savePreferences("options", "sound", DataManager.save(10));
        }
        songs.get(currentlyPlayed).setVolume(volume);
    }

    private boolean prefExist(String prefname, String keyInPrefName) {
        Preferences pref = Gdx.app.getPreferences(prefname);
        String x = pref.getString(keyInPrefName);
        return !x.isEmpty();
    }

    public int getCurrentlyPlayed() {
        return currentlyPlayed;
    }

    public ArrayList<Music> getSongs() {
        return songs;
    }
}
