package levels;

enum Typ {
    DOOR_BRONZE, DOOR_SILVER, DOOR_GOLD, DOOR_RED, GEM_RED, GEM_BLUE, GEM_GREEN,
    SWORD_1, SWORD_2, SWORD_3, SWORD_4, SWORD_5, ARMOR_1, ARMOR_2, ARMOR_3, ARMOR_4, ARMOR_5, SHIELD_SILENCE,
    SHIELD_FIRE, SHIELD_CURSE, SHIELD_POISON, SHIELD_FINAL, KEY_BRONZE, KEY_SILVER, KEY_GOLD, KEY_RED,
    MASTER_KEY, BOMB, POTION, POTION_RED_S, POTION_RED_L, POTION_BLUE_S, POTION_BLUE_L,
    POTION_GREEN_S, POTION_GREEN_L, WALL, TRAP_LEFT, TRAP_RIGHT, TRAP_UP, TRAP_DOWN, TRAP_SILENCE,
    TRAP_SPEAR, TRAP_POISON, TRAP_MAGIC_FLAME, TRAP_FLAME, TRAP_CURSE, TRAP_PIT, SLIME_GREEN, SLIME_RED, BAT,
    PRIEST, GOLEM, SKELETON, SKELETON_WARRIOR, SKELETON_BOSS, SNAKE_BLACK, BEE, MOSQUITO, GNOLL,
    SPIDER, OGRE, TROLL, DRAGON_GREEN, SKELETON_DOG, SKELETON_FISH, SKELETON_LARGE, SKELETON_SNAKE,
    LICH, SKELETON_HYDRA, LICH_POWER, SKELETON_DRAGON, IMP, SNAKE_RED, EFREET, DRAGON_RED, SALAMANDER,
    BALRUG, FIEND, SPHINX, FLESH_MASS, ABOMINATION, EYE, ABOMINATION_LARGE, EYE_RED, EYES_ORB, GUARDIAN,
    REAPER, SCEPTER, RING, HELMET, COINS, APPLE, BOOK, STAFF, ORB, STAIRS_UP, STAIRS_DOWN
}
