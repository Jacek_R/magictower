package levels;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

import gameObjects.Content;
import gameObjects.actors.Npc;
import gameObjects.terrain.Door;
import globalVariables.Message;

public class MapLoader {

    private static Content[][] map;

    private static final int MONSTER_GREEN_SLIME = 1;
    private static final int MONSTER_RED_SLIME = 2;
    private static final int MONSTER_BAT = 3;
    private static final int MONSTER_PRIEST = 4;
    private static final int MONSTER_GOLEM = 5;
    private static final int MONSTER_SKELETON = 6;
    private static final int MONSTER_SKELETON_WARRIOR = 7;
    private static final int MONSTER_SKELETON_BOSS = 8;
    private static final int MONSTER_SNAKE_BLACK = 9;
    private static final int MONSTER_BEE = 10;
    private static final int MONSTER_MOSQUITO = 11;
    private static final int MONSTER_GNOLL = 12;
    private static final int MONSTER_SPIDER = 13;
    private static final int MONSTER_OGRE = 14;
    private static final int MONSTER_TROLL = 15;
    private static final int MONSTER_DRAGON_GREEN = 16;
    private static final int MONSTER_SKELETON_DOG = 17;
    private static final int MONSTER_SKELETON_FISH = 18;
    private static final int MONSTER_SKELETON_LARGE = 19;
    private static final int MONSTER_SKELETON_SNAKE = 20;
    private static final int MONSTER_LICH = 21;
    private static final int MONSTER_SKELETON_HYDRA = 22;
    private static final int MONSTER_POWER_LICH = 23;
    private static final int MONSTER_SKELETON_DRAGON = 24;
    private static final int MONSTER_IMP = 25;
    private static final int MONSTER_SNAKE_RED = 26;
    private static final int MONSTER_EFREET = 27;
    private static final int MONSTER_DRAGON_RED = 28;
    private static final int MONSTER_SALAMANDER = 29;
    private static final int MONSTER_BALRUG = 30;
    private static final int MONSTER_FIEND = 31;
    private static final int MONSTER_SPHINX = 32;
    private static final int MONSTER_FLESH = 33;
    private static final int MONSTER_ABOMINATION = 34;
    private static final int MONSTER_EYE = 35;
    private static final int MONSTER_ABOMINATION_LARGE = 36;
    private static final int MONSTER_EYE_RED = 37;
    private static final int MONSTER_EYES_ORB = 38;
    private static final int MOSNTER_GUARDIAN = 39;
    private static final int MONSTER_REAPER = 40;
    private static final int GEM_RED = 41;
    private static final int GEM_BLUE = 42;
    private static final int GEM_GREEN = 43;
    private static final int ITEM_POTION = 44;
    private static final int ITEM_MASTER_KEY = 45;
    private static final int ITEM_BOMB = 46;
    private static final int ITEM_SCEPTER = 47;
    private static final int POTION_BLUE_S = 48;
    private static final int POTION_BLUE_L = 49;
    private static final int POTION_RED_S = 50;
    private static final int POTION_RED_L = 51;
    private static final int POTION_GREEN_S = 52;
    private static final int POTION_GREEN_L = 53;
    private static final int ITEM_RING = 54;
    private static final int ARMOR_1 = 55;
    private static final int ARMOR_2 = 56;
    private static final int ARMOR_3 = 57;
    private static final int ARMOR_4 = 58;
    private static final int ARMOR_5 = 59;
    private static final int ITEM_HELMET = 60;
    private static final int ITEM_COINS = 61;
    private static final int SHIELD_SILENCE = 62;
    private static final int SHIELD_FIRE = 63;
    private static final int SHIELD_POISON = 64;
    private static final int SHIELD_CURSE = 65;
    private static final int SHIELD_FINAL = 66;
    private static final int ITEM_BOOK = 67;
    private static final int ITEM_APPLE = 68;
    private static final int SWORD_1 = 69;
    private static final int SWORD_2 = 70;
    private static final int SWORD_3 = 71;
    private static final int SWORD_4 = 72;
    private static final int SWORD_5 = 73;
    private static final int ITEM_STAFF = 74;
    private static final int ITEM_ORB = 75;
    private static final int KEY_BRONZE = 76;
    private static final int KEY_SILVER = 77;
    private static final int KEY_GOLD = 78;
    private static final int KEY_RED = 79;
    private static final int WALL = 83;
    private static final int DOOR_RED = 84;
    private static final int DOOR_GOLD = 85;
    private static final int STAIRS_DOWN = 86;
    private static final int DOOR_SILVER = 89;
    private static final int STAIRS_UP = 90;
    private static final int DOOR_BRONZE = 93;
    private static final int TRAP_CURSE = 95;
    private static final int TRAP_SPEAR = 96;
    private static final int TRAP_SILENCE = 97;
    private static final int TRAP_FLAME = 99;
    private static final int TRAP_MAGIC_FLAME = 100;
    private static final int TRAP_PIT = 101;
    private static final int TRAP_POISON = 102;
    private static final int TRAP_UP = 103;
    private static final int TRAP_DOWN = 104;
    private static final int TRAP_LEFT = 105;
    private static final int TRAP_RIGHT = 106;


    private static final String ELDER = "elder";
    private static final String MERCHANT = "merchant";
    private static final String DOOR_IRON = "door_iron";
    private static final String TELEPORT = "teleport";


    public static Content[][] loadMap(String mapName, int mapSizeX, int mapSizeY) {
        map = new Content[mapSizeX][mapSizeY];
        TmxMapLoader mapLoader = new TmxMapLoader();
        TiledMap tiledMap = mapLoader.load(mapName);
        if (tiledMap == null) {
            System.out.println("No map with name " + mapName + " found.");
            return map;
        }

        TiledMapTileLayer tiles = (TiledMapTileLayer) tiledMap.getLayers().get("Tiles");
        if (tiles == null) {
            System.out.println("No 'Tiles' layer in map with name " + mapName + " found.");
            return map;
        }
        loadTiles(tiles);

        MapLayer objects = tiledMap.getLayers().get("Objects");
        if (objects == null) {
            System.out.println("No 'Objects' layer in map with name " + mapName + " found.");
            return map;
        }
        loadObjects(objects);

        return map;
    }

    private static void loadTiles(TiledMapTileLayer layer) {

        for (int i = 0; i < layer.getWidth(); i++) {
            for (int j = 0; j < layer.getHeight(); j++) {
                TiledMapTileLayer.Cell cell = layer.getCell(i, j);
                if (cell == null) {
                    map[i][j] = new Content();
                    continue;
                }
                switch (cell.getTile().getId()) {
                    case MONSTER_GREEN_SLIME:
                        map[i][j] = CreateTileObject.putObject(Typ.SLIME_GREEN);
                        break;
                    case MONSTER_RED_SLIME:
                        map[i][j] = CreateTileObject.putObject(Typ.SLIME_RED);
                        break;
                    case MONSTER_BAT:
                        map[i][j] = CreateTileObject.putObject(Typ.BAT);
                        break;
                    case MONSTER_PRIEST:
                        map[i][j] = CreateTileObject.putObject(Typ.PRIEST);
                        break;
                    case MONSTER_GOLEM:
                        map[i][j] = CreateTileObject.putObject(Typ.GOLEM);
                        break;
                    case MONSTER_SKELETON:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON);
                        break;
                    case MONSTER_SKELETON_WARRIOR:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_WARRIOR);
                        break;
                    case MONSTER_SKELETON_BOSS:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_BOSS);
                        break;
                    case MONSTER_SNAKE_BLACK:
                        map[i][j] = CreateTileObject.putObject(Typ.SNAKE_BLACK);
                        break;
                    case MONSTER_BEE:
                        map[i][j] = CreateTileObject.putObject(Typ.BEE);
                        break;
                    case MONSTER_MOSQUITO:
                        map[i][j] = CreateTileObject.putObject(Typ.MOSQUITO);
                        break;
                    case MONSTER_GNOLL:
                        map[i][j] = CreateTileObject.putObject(Typ.GNOLL);
                        break;
                    case MONSTER_SPIDER:
                        map[i][j] = CreateTileObject.putObject(Typ.SPIDER);
                        break;
                    case MONSTER_OGRE:
                        map[i][j] = CreateTileObject.putObject(Typ.OGRE);
                        break;
                    case MONSTER_TROLL:
                        map[i][j] = CreateTileObject.putObject(Typ.TROLL);
                        break;
                    case MONSTER_DRAGON_GREEN:
                        map[i][j] = CreateTileObject.putObject(Typ.DRAGON_GREEN);
                        break;
                    case MONSTER_SKELETON_DOG:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_DOG);
                        break;
                    case MONSTER_SKELETON_FISH:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_FISH);
                        break;
                    case MONSTER_SKELETON_LARGE:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_LARGE);
                        break;
                    case MONSTER_SKELETON_SNAKE:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_SNAKE);
                        break;
                    case MONSTER_LICH:
                        map[i][j] = CreateTileObject.putObject(Typ.LICH);
                        break;
                    case MONSTER_SKELETON_HYDRA:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_HYDRA);
                        break;
                    case MONSTER_POWER_LICH:
                        map[i][j] = CreateTileObject.putObject(Typ.LICH_POWER);
                        break;
                    case MONSTER_SKELETON_DRAGON:
                        map[i][j] = CreateTileObject.putObject(Typ.SKELETON_DRAGON);
                        break;
                    case MONSTER_IMP:
                        map[i][j] = CreateTileObject.putObject(Typ.IMP);
                        break;
                    case MONSTER_SNAKE_RED:
                        map[i][j] = CreateTileObject.putObject(Typ.SNAKE_RED);
                        break;
                    case MONSTER_EFREET:
                        map[i][j] = CreateTileObject.putObject(Typ.EFREET);
                        break;
                    case MONSTER_DRAGON_RED:
                        map[i][j] = CreateTileObject.putObject(Typ.DRAGON_RED);
                        break;
                    case MONSTER_SALAMANDER:
                        map[i][j] = CreateTileObject.putObject(Typ.SALAMANDER);
                        break;
                    case MONSTER_BALRUG:
                        map[i][j] = CreateTileObject.putObject(Typ.BALRUG);
                        break;
                    case MONSTER_FIEND:
                        map[i][j] = CreateTileObject.putObject(Typ.FIEND);
                        break;
                    case MONSTER_SPHINX:
                        map[i][j] = CreateTileObject.putObject(Typ.SPHINX);
                        break;
                    case MONSTER_FLESH:
                        map[i][j] = CreateTileObject.putObject(Typ.FLESH_MASS);
                        break;
                    case MONSTER_ABOMINATION:
                        map[i][j] = CreateTileObject.putObject(Typ.ABOMINATION);
                        break;
                    case MONSTER_EYE:
                        map[i][j] = CreateTileObject.putObject(Typ.EYE);
                        break;
                    case MONSTER_ABOMINATION_LARGE:
                        map[i][j] = CreateTileObject.putObject(Typ.ABOMINATION_LARGE);
                        break;
                    case MONSTER_EYE_RED:
                        map[i][j] = CreateTileObject.putObject(Typ.EYE_RED);
                        break;
                    case MONSTER_EYES_ORB:
                        map[i][j] = CreateTileObject.putObject(Typ.EYES_ORB);
                        break;
                    case MOSNTER_GUARDIAN:
                        map[i][j] = CreateTileObject.putObject(Typ.GUARDIAN);
                        break;
                    case MONSTER_REAPER:
                        map[i][j] = CreateTileObject.putObject(Typ.REAPER);
                        break;
                    case GEM_RED:
                        map[i][j] = CreateTileObject.putObject(Typ.GEM_RED);
                        break;
                    case GEM_BLUE:
                        map[i][j] = CreateTileObject.putObject(Typ.GEM_BLUE);
                        break;
                    case GEM_GREEN:
                        map[i][j] = CreateTileObject.putObject(Typ.GEM_GREEN);
                        break;
                    case ITEM_POTION:
                        map[i][j] = CreateTileObject.putObject(Typ.POTION);
                        break;
                    case ITEM_MASTER_KEY:
                        map[i][j] = CreateTileObject.putObject(Typ.MASTER_KEY);
                        break;
                    case ITEM_BOMB:
                        map[i][j] = CreateTileObject.putObject(Typ.BOMB);
                        break;
                    case ITEM_SCEPTER:
                        map[i][j] = CreateTileObject.putObject(Typ.SCEPTER);
                        break;
                    case POTION_BLUE_S:
                        map[i][j] = CreateTileObject.putObject(Typ.POTION_BLUE_S);
                        break;
                    case POTION_BLUE_L:
                        map[i][j] = CreateTileObject.putObject(Typ.POTION_BLUE_L);
                        break;
                    case POTION_RED_S:
                        map[i][j] = CreateTileObject.putObject(Typ.POTION_RED_S);
                        break;
                    case POTION_RED_L:
                        map[i][j] = CreateTileObject.putObject(Typ.POTION_RED_L);
                        break;
                    case POTION_GREEN_S:
                        map[i][j] = CreateTileObject.putObject(Typ.POTION_GREEN_S);
                        break;
                    case POTION_GREEN_L:
                        map[i][j] = CreateTileObject.putObject(Typ.POTION_GREEN_L);
                        break;
                    case ITEM_RING:
                        map[i][j] = CreateTileObject.putObject(Typ.RING);
                        break;
                    case ARMOR_1:
                        map[i][j] = CreateTileObject.putObject(Typ.ARMOR_1);
                        break;
                    case ARMOR_2:
                        map[i][j] = CreateTileObject.putObject(Typ.ARMOR_2);
                        break;
                    case ARMOR_3:
                        map[i][j] = CreateTileObject.putObject(Typ.ARMOR_3);
                        break;
                    case ARMOR_4:
                        map[i][j] = CreateTileObject.putObject(Typ.ARMOR_4);
                        break;
                    case ARMOR_5:
                        map[i][j] = CreateTileObject.putObject(Typ.ARMOR_5);
                        break;
                    case ITEM_HELMET:
                        map[i][j] = CreateTileObject.putObject(Typ.HELMET);
                        break;
                    case ITEM_COINS:
                        map[i][j] = CreateTileObject.putObject(Typ.COINS);
                        break;
                    case SHIELD_SILENCE:
                        map[i][j] = CreateTileObject.putObject(Typ.SHIELD_SILENCE);
                        break;
                    case SHIELD_FIRE:
                        map[i][j] = CreateTileObject.putObject(Typ.SHIELD_FIRE);
                        break;
                    case SHIELD_POISON:
                        map[i][j] = CreateTileObject.putObject(Typ.SHIELD_POISON);
                        break;
                    case SHIELD_CURSE:
                        map[i][j] = CreateTileObject.putObject(Typ.SHIELD_CURSE);
                        break;
                    case SHIELD_FINAL:
                        map[i][j] = CreateTileObject.putObject(Typ.SHIELD_FINAL);
                        break;
                    case ITEM_BOOK:
                        map[i][j] = CreateTileObject.putObject(Typ.BOOK);
                        break;
                    case ITEM_APPLE:
                        map[i][j] = CreateTileObject.putObject(Typ.APPLE);
                        break;
                    case SWORD_1:
                        map[i][j] = CreateTileObject.putObject(Typ.SWORD_1);
                        break;
                    case SWORD_2:
                        map[i][j] = CreateTileObject.putObject(Typ.SWORD_2);
                        break;
                    case SWORD_3:
                        map[i][j] = CreateTileObject.putObject(Typ.SWORD_3);
                        break;
                    case SWORD_4:
                        map[i][j] = CreateTileObject.putObject(Typ.SWORD_4);
                        break;
                    case SWORD_5:
                        map[i][j] = CreateTileObject.putObject(Typ.SWORD_5);
                        break;
                    case ITEM_STAFF:
                        map[i][j] = CreateTileObject.putObject(Typ.STAFF);
                        break;
                    case ITEM_ORB:
                        map[i][j] = CreateTileObject.putObject(Typ.ORB);
                        break;
                    case KEY_BRONZE:
                        map[i][j] = CreateTileObject.putObject(Typ.KEY_BRONZE);
                        break;
                    case KEY_SILVER:
                        map[i][j] = CreateTileObject.putObject(Typ.KEY_SILVER);
                        break;
                    case KEY_GOLD:
                        map[i][j] = CreateTileObject.putObject(Typ.KEY_GOLD);
                        break;
                    case KEY_RED:
                        map[i][j] = CreateTileObject.putObject(Typ.KEY_RED);
                        break;
                    case WALL:
                        map[i][j] = CreateTileObject.putObject(Typ.WALL);
                        break;
                    case DOOR_RED:
                        map[i][j] = CreateTileObject.putObject(Typ.DOOR_RED);
                        break;
                    case DOOR_GOLD:
                        map[i][j] = CreateTileObject.putObject(Typ.DOOR_GOLD);
                        break;
                    case STAIRS_DOWN:
                        map[i][j] = CreateTileObject.putObject(Typ.STAIRS_DOWN);
                        break;
                    case DOOR_SILVER:
                        map[i][j] = CreateTileObject.putObject(Typ.DOOR_SILVER);
                        break;
                    case STAIRS_UP:
                        map[i][j] = CreateTileObject.putObject(Typ.STAIRS_UP);
                        break;
                    case DOOR_BRONZE:
                        map[i][j] = CreateTileObject.putObject(Typ.DOOR_BRONZE);
                        break;
                    case TRAP_CURSE:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_CURSE);
                        break;
                    case TRAP_SPEAR:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_SPEAR);
                        break;
                    case TRAP_SILENCE:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_SILENCE);
                        break;
                    case TRAP_FLAME:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_FLAME);
                        break;
                    case TRAP_MAGIC_FLAME:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_MAGIC_FLAME);
                        break;
                    case TRAP_PIT:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_PIT);
                        break;
                    case TRAP_POISON:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_POISON);
                        break;
                    case TRAP_UP:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_UP);
                        break;
                    case TRAP_DOWN:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_DOWN);
                        break;
                    case TRAP_LEFT:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_LEFT);
                        break;
                    case TRAP_RIGHT:
                        map[i][j] = CreateTileObject.putObject(Typ.TRAP_RIGHT);
                        break;
                    default:
                        map[i][j] = new Content();
                        break;
                }
            }
        }
    }

    private static void loadObjects(MapLayer objects) {
        final String propertyX = "x";
        final String propertyY = "y";
        final String propertyMessage = "message";
        final String propertyPrice = "price";
        final String propertyAmount = "amount";
        final String propertyItemToSell = "selling";
        final String propertyFirstGuardPositionX = "mon1_x";
        final String propertyFirstGuardPositionY = "mon1_y";
        final String propertySecondGuardPositionX = "mon2_x";
        final String propertySecondGuardPositionY = "mon2_y";

        int numberOfObjects = objects.getObjects().getCount();
        int o = 0;
        while (o < numberOfObjects) {
            MapObject object = objects.getObjects().get(o);
            String name = object.getName();
            int x = Integer.parseInt(object.getProperties().get(propertyX, String.class));
            int y = Integer.parseInt(object.getProperties().get(propertyY, String.class));

            if (name.equals(ELDER)) {
                int message = Integer.parseInt(object.getProperties().get(propertyMessage, String.class));
                String s = Message.messagesElder.get(message);

                map[x][y] = new Npc(Npc.Type.ELDER, s);

            } else if (name.equals(MERCHANT)) {
                int message = Integer.parseInt(object.getProperties().get(propertyMessage, String.class));
                String s = Message.messagesMerchant.get(message);

                int price = Integer.parseInt(object.getProperties().get(propertyPrice, String.class));
                int amount = Integer.parseInt(object.getProperties().get(propertyAmount, String.class));
                String selling = object.getProperties().get(propertyItemToSell, String.class);

                map[x][y] = new Npc(Npc.Type.MERCHANT, s, price, amount, selling, x, y);

            } else if (name.equals(DOOR_IRON)) {
                int mon1_x = Integer.parseInt(object.getProperties().get(propertyFirstGuardPositionX, String.class));
                int mon1_y = Integer.parseInt(object.getProperties().get(propertyFirstGuardPositionY, String.class));
                int mon2_x = Integer.parseInt(object.getProperties().get(propertySecondGuardPositionX, String.class));
                int mon2_y = Integer.parseInt(object.getProperties().get(propertySecondGuardPositionY, String.class));

                map[x][y] = new Door(Door.Type.IRON, new int[]{mon1_x, mon1_y}, new int[]{mon2_x, mon2_y});
            }
            o++;
        }
    }
}
