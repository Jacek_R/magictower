package levels;

import gameObjects.actors.Monster;
import gameObjects.actors.Npc;
import gameObjects.pickUps.Gem;
import gameObjects.pickUps.Item;
import gameObjects.pickUps.Key;
import gameObjects.pickUps.Potion;
import gameObjects.terrain.Door;
import gameObjects.terrain.Stairs;
import gameObjects.terrain.Trap;

public class Container {
    public enum Type {MONSTER, NPC, GEM, ITEM, KEY, POTION, DOOR, STAIRS, TRAP, WALL, EMPTY}

    private Type type;

    private Monster.Type monsterType;
    private Npc.Type npcType;
    private Gem.Type gemType;
    private Item.Type itemType;
    private Key.Type keyType;
    private Potion.Type potionType;
    private Door.Type doorType;
    private Stairs.Type stairsType;
    private Trap.Type trapType;

    private int[] guard_1;
    private int[] guard_2;

    private String message;
    private int price;
    private int amount;
    private String itemToSell;
    private int x;
    private int y;

    @SuppressWarnings(value = "unused")
    public Container() {
    }

    public Container(Type type) {
        this.type = type;
    }

    public Container(Type type, Monster.Type monsterType) {
        this.type = type;
        this.monsterType = monsterType;
    }

    public Container(Type type, Npc.Type npcType, String message, int price, int amount, String itemToSell, int x, int y) {
        this.type = type;
        this.npcType = npcType;
        this.message = message;
        this.price = price;
        this.amount = amount;
        this.itemToSell = itemToSell;
        this.x = x;
        this.y = y;
    }

    public Container(Type type, Gem.Type gemType) {
        this.type = type;
        this.gemType = gemType;
    }

    public Container(Type type, Item.Type itemType) {
        this.type = type;
        this.itemType = itemType;
    }

    public Container(Type type, Key.Type keyType) {
        this.type = type;
        this.keyType = keyType;
    }

    public Container(Type type, Potion.Type potionType) {
        this.type = type;
        this.potionType = potionType;
    }

    public Container(Type type, Door.Type doorType, int[] guard_1, int[] guard_2) {
        this.type = type;
        this.doorType = doorType;
        this.guard_1 = guard_1;
        this.guard_2 = guard_2;
    }

    public Container(Type type, Stairs.Type stairsType) {
        this.type = type;
        this.stairsType = stairsType;
    }

    public Container(Type type, Trap.Type trapType) {
        this.type = type;
        this.trapType = trapType;
    }

    public Type getType() {
        return type;
    }

    public Monster.Type getMonsterType() {
        return monsterType;
    }

    public Npc.Type getNpcType() {
        return npcType;
    }

    public Gem.Type getGemType() {
        return gemType;
    }

    public Item.Type getItemType() {
        return itemType;
    }

    public Key.Type getKeyType() {
        return keyType;
    }

    public Potion.Type getPotionType() {
        return potionType;
    }

    public Door.Type getDoorType() {
        return doorType;
    }

    public Stairs.Type getStairsType() {
        return stairsType;
    }

    public Trap.Type getTrapType() {
        return trapType;
    }

    public int[] getGuard_1() {
        return guard_1;
    }

    public int[] getGuard_2() {
        return guard_2;
    }

    public String getMessage() {
        return message;
    }

    public int getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public String getItemToSell() {
        return itemToSell;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
