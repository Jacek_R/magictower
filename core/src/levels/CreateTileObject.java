package levels;

import gameObjects.Content;
import gameObjects.actors.Monster;
import gameObjects.pickUps.Gem;
import gameObjects.pickUps.Item;
import gameObjects.pickUps.Key;
import gameObjects.pickUps.Potion;
import gameObjects.terrain.Door;
import gameObjects.terrain.Stairs;
import gameObjects.terrain.Trap;
import gameObjects.terrain.Wall;

class CreateTileObject {

    static Content putObject(Typ type) {
        Content object;
        switch (type) {
            case DOOR_BRONZE:
                object = new Door(Door.Type.BRONZE);
                break;
            case DOOR_SILVER:
                object = new Door(Door.Type.SILVER);
                break;
            case DOOR_GOLD:
                object = new Door(Door.Type.GOLD);
                break;
            case DOOR_RED:
                object = new Door(Door.Type.RED);
                break;
            case GEM_RED:
                object = new Gem(Gem.Type.RED);
                break;
            case GEM_BLUE:
                object = new Gem(Gem.Type.BLUE);
                break;
            case GEM_GREEN:
                object = new Gem(Gem.Type.GREEN);
                break;
            case SWORD_1:
                object = new Item(Item.Type.SWORD_1);
                break;
            case SWORD_2:
                object = new Item(Item.Type.SWORD_2);
                break;
            case SWORD_3:
                object = new Item(Item.Type.SWORD_3);
                break;
            case SWORD_4:
                object = new Item(Item.Type.SWORD_4);
                break;
            case SWORD_5:
                object = new Item(Item.Type.SWORD_5);
                break;
            case ARMOR_1:
                object = new Item(Item.Type.ARMOR_1);
                break;
            case ARMOR_2:
                object = new Item(Item.Type.ARMOR_2);
                break;
            case ARMOR_3:
                object = new Item(Item.Type.ARMOR_3);
                break;
            case ARMOR_4:
                object = new Item(Item.Type.ARMOR_4);
                break;
            case ARMOR_5:
                object = new Item(Item.Type.ARMOR_5);
                break;
            case SHIELD_SILENCE:
                object = new Item(Item.Type.SHIELD_SILENCE);
                break;
            case SHIELD_FIRE:
                object = new Item(Item.Type.SHIELD_FIRE);
                break;
            case SHIELD_CURSE:
                object = new Item(Item.Type.SHIELD_CURSE);
                break;
            case SHIELD_POISON:
                object = new Item(Item.Type.SHIELD_POISON);
                break;
            case SHIELD_FINAL:
                object = new Item(Item.Type.SHIELD_FINAL);
                break;
            case KEY_BRONZE:
                object = new Key(Key.Type.BRONZE);
                break;
            case KEY_SILVER:
                object = new Key(Key.Type.SILVER);
                break;
            case KEY_GOLD:
                object = new Key(Key.Type.GOLD);
                break;
            case KEY_RED:
                object = new Key(Key.Type.RED);
                break;
            case MASTER_KEY:
                object = new Item(Item.Type.MASTER_KEY);
                break;
            case BOMB:
                object = new Item(Item.Type.BOMB);
                break;
            case POTION:
                object = new Item(Item.Type.POTION);
                break;
            case POTION_RED_S:
                object = new Potion(Potion.Type.RED_S);
                break;
            case POTION_RED_L:
                object = new Potion(Potion.Type.RED_L);
                break;
            case POTION_BLUE_S:
                object = new Potion(Potion.Type.BLUE_S);
                break;
            case POTION_BLUE_L:
                object = new Potion(Potion.Type.BLUE_L);
                break;
            case POTION_GREEN_S:
                object = new Potion(Potion.Type.GREEN_S);
                break;
            case POTION_GREEN_L:
                object = new Potion(Potion.Type.GREEN_L);
                break;
            case WALL:
                object = new Wall();
                break;
            case TRAP_LEFT:
                object = new Trap(Trap.Type.LEFT);
                break;
            case TRAP_RIGHT:
                object = new Trap(Trap.Type.RIGHT);
                break;
            case TRAP_UP:
                object = new Trap(Trap.Type.UP);
                break;
            case TRAP_DOWN:
                object = new Trap(Trap.Type.DOWN);
                break;
            case TRAP_SILENCE:
                object = new Trap(Trap.Type.SILENCE);
                break;
            case TRAP_SPEAR:
                object = new Trap(Trap.Type.SPEAR);
                break;
            case TRAP_POISON:
                object = new Trap(Trap.Type.POISON);
                break;
            case TRAP_MAGIC_FLAME:
                object = new Trap(Trap.Type.MAGIC_FLAME);
                break;
            case TRAP_FLAME:
                object = new Trap(Trap.Type.FLAME);
                break;
            case TRAP_CURSE:
                object = new Trap(Trap.Type.CURSE);
                break;
            case TRAP_PIT:
                object = new Trap(Trap.Type.PIT);
                break;
            case SLIME_GREEN:
                object = new Monster(Monster.Type.SLIME_GREEN);
                break;
            case SLIME_RED:
                object = new Monster(Monster.Type.SLIME_RED);
                break;
            case BAT:
                object = new Monster(Monster.Type.BAT);
                break;
            case PRIEST:
                object = new Monster(Monster.Type.PRIEST);
                break;
            case GOLEM:
                object = new Monster(Monster.Type.GOLEM);
                break;
            case SKELETON:
                object = new Monster(Monster.Type.SKELETON);
                break;
            case SKELETON_WARRIOR:
                object = new Monster(Monster.Type.SKELETON_WARRIOR);
                break;
            case SKELETON_BOSS:
                object = new Monster(Monster.Type.SKELETON_BOSS);
                break;
            case SNAKE_BLACK:
                object = new Monster(Monster.Type.SNAKE_BLACK);
                break;
            case BEE:
                object = new Monster(Monster.Type.BEE);
                break;
            case MOSQUITO:
                object = new Monster(Monster.Type.MOSQUITO);
                break;
            case GNOLL:
                object = new Monster(Monster.Type.GNOLL);
                break;
            case SPIDER:
                object = new Monster(Monster.Type.SPIDER);
                break;
            case OGRE:
                object = new Monster(Monster.Type.OGRE);
                break;
            case TROLL:
                object = new Monster(Monster.Type.TROLL);
                break;
            case DRAGON_GREEN:
                object = new Monster(Monster.Type.DRAGON_GREEN);
                break;
            case SKELETON_DOG:
                object = new Monster(Monster.Type.SKELETON_DOG);
                break;
            case SKELETON_FISH:
                object = new Monster(Monster.Type.SKELETON_FISH);
                break;
            case SKELETON_LARGE:
                object = new Monster(Monster.Type.SKELETON_LARGE);
                break;
            case SKELETON_SNAKE:
                object = new Monster(Monster.Type.SKELETON_SNAKE);
                break;
            case LICH:
                object = new Monster(Monster.Type.LICH);
                break;
            case SKELETON_HYDRA:
                object = new Monster(Monster.Type.SKELETON_HYDRA);
                break;
            case LICH_POWER:
                object = new Monster(Monster.Type.LICH_POWER);
                break;
            case SKELETON_DRAGON:
                object = new Monster(Monster.Type.SKELETON_DRAGON);
                break;
            case IMP:
                object = new Monster(Monster.Type.IMP);
                break;
            case SNAKE_RED:
                object = new Monster(Monster.Type.SNAKE_RED);
                break;
            case EFREET:
                object = new Monster(Monster.Type.EFREET);
                break;
            case DRAGON_RED:
                object = new Monster(Monster.Type.DRAGON_RED);
                break;
            case SALAMANDER:
                object = new Monster(Monster.Type.SALAMANDER);
                break;
            case BALRUG:
                object = new Monster(Monster.Type.BALRUG);
                break;
            case FIEND:
                object = new Monster(Monster.Type.FIEND);
                break;
            case SPHINX:
                object = new Monster(Monster.Type.SPHINX);
                break;
            case FLESH_MASS:
                object = new Monster(Monster.Type.FLESH_MASS);
                break;
            case ABOMINATION:
                object = new Monster(Monster.Type.ABOMINATION);
                break;
            case EYE:
                object = new Monster(Monster.Type.EYE);
                break;
            case ABOMINATION_LARGE:
                object = new Monster(Monster.Type.ABOMINATION_LARGE);
                break;
            case EYE_RED:
                object = new Monster(Monster.Type.EYE_RED);
                break;
            case EYES_ORB:
                object = new Monster(Monster.Type.EYES_ORB);
                break;
            case GUARDIAN:
                object = new Monster(Monster.Type.GUARDIAN);
                break;
            case REAPER:
                object = new Monster(Monster.Type.REAPER);
                break;
            case SCEPTER:
                object = new Item(Item.Type.SCEPTER);
                break;
            case RING:
                object = new Item(Item.Type.RING);
                break;
            case HELMET:
                object = new Item(Item.Type.HELMET);
                break;
            case COINS:
                object = new Item(Item.Type.COINS);
                break;
            case APPLE:
                object = new Item(Item.Type.APPLE);
                break;
            case BOOK:
                object = new Item(Item.Type.BOOK);
                break;
            case STAFF:
                object = new Item(Item.Type.STAFF);
                break;
            case ORB:
                object = new Item(Item.Type.ORB);
                break;
            case STAIRS_UP:
                object = new Stairs(Stairs.Type.UP);
                break;
            case STAIRS_DOWN:
                object = new Stairs(Stairs.Type.DOWN);
                break;
            default:
                object = new Content();
                break;
        }
        return object;
    }
}
