package userInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.Game;

import globalVariables.Message;
import globalVariables.PlayerData;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;

public class Ui {

    protected Game main;
    protected Stage stage;
    protected Stage stageForDialog;

    protected Skin labelSkin;
    protected PlayerData data;

    public Ui(Game main) {
        this.main = main;
        data = PlayerData.getInstance();
        stage = new Stage();
        stageForDialog = new Stage();

    }

    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
        stageForDialog.act();
        stageForDialog.draw();
    }

    public void dispose() {
        stage.dispose();
        stageForDialog.dispose();
    }

    protected void labelSkin(int sizeX, int sizeY) {
        //Create a font
        BitmapFont font = new BitmapFont();
        labelSkin = new Skin();
        labelSkin.add("default", font);

        //Create a texture
        Pixmap pixmap = new Pixmap(Gdx.graphics.getWidth() / sizeX, Gdx.graphics.getHeight() / sizeY, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        labelSkin.add("background", new Texture(pixmap));

        //Create a button style
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.background = labelSkin.newDrawable("background", Color.CLEAR);
        labelStyle.font = labelSkin.getFont("default");
        labelSkin.add("default", labelStyle);
    }


    public void showConfirmDialog(String message, int zone) {
        Dialog dialog = new Dialog("", AtlasManager.getInstance().getDialogSkinsSmall().get(zone)) {
            @Override
            protected void result(Object object) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                if (object != GameState.IN_PLAY) {
                    Gdx.input.setInputProcessor(stage);
                }
            }
        };
        dialog.text(message);
        dialog.key(Input.Keys.SPACE, main.getStateMachine().getCurrentState());
        dialog.key(Input.Keys.ENTER, main.getStateMachine().getCurrentState());
        dialog.button(Message.DIALOG_CONFIRM, main.getStateMachine().getCurrentState());
        if (main.getStateMachine().getCurrentState() != GameState.IN_PLAY) {
            Gdx.input.setInputProcessor(stageForDialog);
        }
        dialog.show(stageForDialog);
    }

    public Stage getStage() {
        return stage;
    }

    public Stage getStageForDialog() {
        return stageForDialog;
    }

}
