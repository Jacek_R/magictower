package userInterface.gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Game;

import java.util.ArrayList;

import control.ControlInPlay;
import gamePlay.Fight;
import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import graphic.DrawGameArea;
import userInterface.Ui;

public class InPlay extends Ui {

    private ControlInPlay control;
    private Game main;
    private Fight fight;
    private ArrayList<Image> buffsDebuffs;
    private Array<Table> tablesInMenu;
    private ArrayList<SpriteDrawable> tableBackgrounds;
    private DrawGameArea draw;
    private AtlasManager atlas;

    private Table table;
    private Table tableMap;

    private Table systemOptions;
    private Table gameOptions;
    private Table controlsUp;
    private Table controlsDown;
    private Table buffs;
    private Table debuffs;
    private Table keys;
    private Table statsUp;
    private Table statsDown;

    private ImageButton btn_options;
    private ImageButton btn_save;
    private ImageButton btn_load;
    private ImageButton btn_help;
    private ImageButton btn_left;
    private ImageButton btn_right;
    private ImageButton btn_down;
    private ImageButton btn_up;
    private ImageButton btn_prediction;
    private ImageButton btn_status;
    private ImageButton btn_useSkill;
    private ImageButton btn_buySkill;

    private Image buff_bless;
    private Image buff_instant;
    private Image buff_protect;
    private Image buff_vampire;

    private Image debuff_curse;
    private Image debuff_burn;
    private Image debuff_poison;
    private Image debuff_silence;

    private Label keys_bronze;
    private Label keys_silver;
    private Label keys_gold;
    private Label keys_red;
    private Label attack;
    private Label defense;
    private Label health;
    private Label mana;
    private Label experience;
    private Label gold;
    private Label floor;

    private Label vampire_dur;
    private Label silence_dur;
    private Label bless_dur;
    private Label protect_dur;
    private Label curse_dur;
    private Label burn_dur;
    private Label poison_dur;
    private Label instant_dur;

    private Image mini_keys_bronze;
    private Image mini_keys_silver;
    private Image mini_keys_gold;
    private Image mini_keys_red;

    private Image mini_attack;
    private Image mini_defense;
    private Image mini_health;
    private Image mini_mana;
    private Image mini_experience;
    private Image mini_gold;

    public InPlay(Game main) {
        super(main);
        this.main = main;
        super.labelSkin(2, 2);

        fight = new Fight();
        control = new ControlInPlay(this);
        draw = new DrawGameArea();

        atlas = AtlasManager.getInstance();

        tableBackgrounds = new ArrayList<SpriteDrawable>();
        tableBackgrounds.add(new SpriteDrawable(new Sprite(atlas.getButtons().findRegion("buttonLarge", 1))));
        tableBackgrounds.add(new SpriteDrawable(new Sprite(atlas.getButtons().findRegion("buttonLarge", 2))));
        tableBackgrounds.add(new SpriteDrawable(new Sprite(atlas.getButtons().findRegion("buttonLarge", 3))));
        tableBackgrounds.add(new SpriteDrawable(new Sprite(atlas.getButtons().findRegion("buttonLarge", 4))));
        tableBackgrounds.add(new SpriteDrawable(new Sprite(atlas.getButtons().findRegion("buttonLarge", 5))));

        buffsDebuffs = new ArrayList<Image>();

        buildScene();

        stage.addActor(table);
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(stageForDialog);
        multiplexer.addProcessor(control);

        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        draw.renderBackground();
        stage.act();
        stage.draw();

        draw.renderPlayer();
        draw.renderBlood();

        stageForDialog.act();
        stageForDialog.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
        stageForDialog.dispose();
        draw.dispose();
    }

    public void useStairs(boolean changeZone) {

        control.setLockMovement(true);
        Timer time = new Timer();
        if (changeZone) {
            table.addAction(Actions.fadeOut(0.75f));
            time.scheduleTask(fadeWholeScene(), 0.85f);
            time.scheduleTask(changeBackground(), 1.6f);
        } else {
            tableMap.addAction(Actions.fadeOut(0.75f));
            time.scheduleTask(fadeMapOnly(), 0.85f);
        }
        time.scheduleTask(enableMovement(), 1.6f);
        time.start();
    }

    private Timer.Task fadeMapOnly() {
        return new Timer.Task() {
            @Override
            public void run() {
                draw.createTableWithImages();
                tableMap.clearChildren();
                tableMap.clearActions();
                tableMap.add(draw.getGameMap());
                tableMap.addAction(Actions.fadeIn(0.75f));
                control.setLockMovement(true);
            }
        };
    }

    private Timer.Task fadeWholeScene() {
        return new Timer.Task() {
            @Override
            public void run() {
                draw.createTableWithImages();
                table.clearActions();
                tableMap.clearChildren();
                tableMap.add(draw.getGameMap());
                setBackgroundsForTables();
                table.addAction(Actions.fadeIn(0.75f));
                control.setLockMovement(true);
            }
        };
    }

    private Timer.Task enableMovement() {
        return new Timer.Task() {
            @Override
            public void run() {
                draw.changeBackground();
                control.setLockMovement(false);
            }
        };
    }

    private Timer.Task changeBackground() {
        return new Timer.Task() {
            @Override
            public void run() {
                draw.changeBackground();
            }
        };
    }

    public void setInputProcessor() {
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(stageForDialog);
        multiplexer.addProcessor(control);

        Gdx.input.setInputProcessor(multiplexer);
    }

    private void buildScene() {
        tablesInMenu = new Array<Table>();

        table = new Table();
        tableMap = new Table();
        Table tableMenus = new Table();

        systemOptions = new Table();
        gameOptions = new Table();
        controlsUp = new Table();
        controlsDown = new Table();
        buffs = new Table();
        debuffs = new Table();
        keys = new Table();
        statsUp = new Table();
        statsDown = new Table();

        createButtons();
        createImages();
        createLabels();
        addImagesToMap();
        addTablesToArray();
        setBackgroundsForTables();

        table.setFillParent(true);

        table.add(tableMap);
        table.add(tableMenus);

        tableMap.add(draw.getGameMap());

        tableMenus.align(Align.topRight);
        tableMenus.defaults().pad(15, 5, 5, 10);

        tableMenus.add(systemOptions);
        tableMenus.row();
        tableMenus.add(gameOptions);
        tableMenus.row();
        tableMenus.add(statsUp);
        tableMenus.row();
        tableMenus.add(statsDown);
        tableMenus.row();
        tableMenus.add(keys);
        tableMenus.row();
        tableMenus.add(debuffs);
        tableMenus.row();
        tableMenus.add(buffs);
        tableMenus.row();
        tableMenus.add(controlsUp);
        tableMenus.row();
        tableMenus.add(controlsDown);

        systemOptions.defaults().padLeft(5);
        systemOptions.add(btn_save, btn_load, btn_help, btn_options);

        gameOptions.defaults().padLeft(5);
        gameOptions.add(btn_prediction, btn_status, btn_buySkill, btn_useSkill);

        controlsUp.add(btn_up).padLeft(50);
        controlsUp.add(floor).padLeft(10);

        controlsDown.defaults().padLeft(5);
        controlsDown.add(btn_left, btn_down, btn_right);

        buffs.defaults().padLeft(5);
        buffs.add(buff_bless, bless_dur, buff_instant, instant_dur, buff_protect, protect_dur, buff_vampire, vampire_dur);

        debuffs.defaults().padLeft(5);
        debuffs.add(debuff_curse, curse_dur, debuff_burn, burn_dur, debuff_poison, poison_dur, debuff_silence, silence_dur);

        keys.defaults().padLeft(5);
        keys.add(mini_keys_bronze, keys_bronze, mini_keys_silver, keys_silver, mini_keys_gold, keys_gold, mini_keys_red, keys_red);

        statsUp.defaults().padLeft(5);
        statsUp.add(mini_attack, attack, mini_defense, defense, mini_experience, experience);

        statsDown.defaults().padLeft(5);
        statsDown.add(mini_health, health, mini_mana, mana, mini_gold, gold);
    }

    public void updateInfoInViews() {
        attack.setText(String.valueOf(data.getStats().get(Const.ATTACK)));
        defense.setText(String.valueOf(data.getStats().get(Const.DEFENSE)));
        health.setText(String.valueOf(data.getStats().get(Const.HEALTH)));
        mana.setText(String.valueOf(data.getStats().get(Const.MANA)));
        experience.setText(String.valueOf(data.getStats().get(Const.EXP)));
        gold.setText(String.valueOf(data.getStats().get(Const.GOLD)));

        floor.setText(String.valueOf(textForFloors()));

        keys_bronze.setText(String.valueOf(data.getKeys().get(Const.KEY_BRONZE)));
        keys_gold.setText(String.valueOf(data.getKeys().get(Const.KEY_GOLD)));
        keys_red.setText(String.valueOf(data.getKeys().get(Const.KEY_RED)));
        keys_silver.setText(String.valueOf(data.getKeys().get(Const.KEY_SILVER)));

        updateImage(Const.BLESS, Const.REGION_BLESS);
        updateImage(Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE);
        updateImage(Const.PROTECT, Const.REGION_PROTECT);
        updateImage(Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE);

        updateImage(Const.POISONED, Const.REGION_POISONED);
        updateImage(Const.CURSED, Const.REGION_CURSED);
        updateImage(Const.BURNED, Const.REGION_BURNED);
        updateImage(Const.SILENCED, Const.REGION_SILENCED);

        bless_dur.setText(String.valueOf(data.getBuffs().get(Const.BLESS)));
        instant_dur.setText(String.valueOf(data.getBuffs().get(Const.INSTANT_STRIKE)));
        protect_dur.setText(String.valueOf(data.getBuffs().get(Const.PROTECT)));
        vampire_dur.setText(String.valueOf(data.getBuffs().get(Const.VAMPIRE_EDGE)));

        burn_dur.setText(String.valueOf(data.getBuffs().get(Const.BURNED)));
        poison_dur.setText(String.valueOf(data.getBuffs().get(Const.POISONED)));
        silence_dur.setText(String.valueOf(data.getBuffs().get(Const.SILENCED)));
        curse_dur.setText(String.valueOf(data.getBuffs().get(Const.CURSED)));

        tableMap.clearChildren();
        draw.createTableWithImages();
        tableMap.add(draw.getGameMap());
        setBackgroundsForTables();
        draw.changeBackground();
        /* lepiej chyba będzie zrobić oddzielną metodę dla uaktualniania rzeczy po statystykich,
        bo są one tylko w pojedynczych przypadkach nieaktualne*/
    }

    private void addTablesToArray() {
        tablesInMenu.add(systemOptions);
        tablesInMenu.add(gameOptions);
        tablesInMenu.add(controlsUp);
        tablesInMenu.add(controlsDown);
        tablesInMenu.add(buffs);
        tablesInMenu.add(debuffs);
        tablesInMenu.add(keys);
        tablesInMenu.add(statsUp);
        tablesInMenu.add(statsDown);
    }

    private void setBackgroundsForTables() {
        int zone = Const.zone(data.getCurrentFloor());

        for (Table table : tablesInMenu) {
            table.setBackground(tableBackgrounds.get(zone - 1));
        }
    }

    private void createButtons() {
        btn_left = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("left")));
        btn_left.addListener(control.getLeft());

        btn_down = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("down")));
        btn_down.addListener(control.getDown());

        btn_up = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("up")));
        btn_up.addListener(control.getUp());

        btn_right = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("right")));
        btn_right.addListener(control.getRight());

        btn_options = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("options")));
        btn_options.addListener(control.getOptions());

        btn_save = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("save")));
        btn_save.addListener(control.getSave());

        btn_load = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("load")));
        btn_load.addListener(control.getLoad());

        btn_help = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("help")));
        btn_help.addListener(control.getHelp());

        btn_prediction = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("prediction")));
        btn_prediction.addListener(control.getPrediction());

        btn_status = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("fly")));
        btn_status.addListener(control.getFly());

        btn_useSkill = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("skills")));
        btn_useSkill.addListener(control.getUseSkill());

        btn_buySkill = new ImageButton(new SpriteDrawable(atlas.getUi().createSprite("buySkill")));
        btn_buySkill.addListener(control.getBuySkill());
    }

    private void createLabels() {
        keys_bronze = new Label(data.getKeys().get(Const.KEY_BRONZE).toString(), labelSkin);
        keys_silver = new Label(data.getKeys().get(Const.KEY_SILVER).toString(), labelSkin);
        keys_gold = new Label(data.getKeys().get(Const.KEY_GOLD).toString(), labelSkin);
        keys_red = new Label(data.getKeys().get(Const.KEY_RED).toString(), labelSkin);
        attack = new Label(data.getStats().get(Const.ATTACK).toString(), labelSkin);
        defense = new Label(data.getStats().get(Const.DEFENSE).toString(), labelSkin);
        health = new Label(data.getStats().get(Const.HEALTH).toString(), labelSkin);
        mana = new Label(data.getStats().get(Const.MANA).toString(), labelSkin);
        experience = new Label(data.getStats().get(Const.EXP).toString(), labelSkin);
        gold = new Label(data.getStats().get(Const.GOLD).toString(), labelSkin);
        floor = new Label(textForFloors(), labelSkin);

        vampire_dur = new Label(data.getBuffs().get(Const.VAMPIRE_EDGE).toString(), labelSkin);
        protect_dur = new Label(data.getBuffs().get(Const.PROTECT).toString(), labelSkin);
        bless_dur = new Label(data.getBuffs().get(Const.BLESS).toString(), labelSkin);
        instant_dur = new Label(data.getBuffs().get(Const.INSTANT_STRIKE).toString(), labelSkin);
        poison_dur = new Label(data.getBuffs().get(Const.POISONED).toString(), labelSkin);
        curse_dur = new Label(data.getBuffs().get(Const.CURSED).toString(), labelSkin);
        burn_dur = new Label(data.getBuffs().get(Const.BURNED).toString(), labelSkin);
        silence_dur = new Label(data.getBuffs().get(Const.SILENCED).toString(), labelSkin);
    }

    private String textForFloors() {
        String text;
        if (data.getCurrentFloor() < 10) {
            text = Message.FLOOR + 0 + data.getCurrentFloor();
        } else {
            text = Message.FLOOR + data.getCurrentFloor();
        }
        return text;
    }

    private void createImages() {
        buff_bless = new Image(atlas.getUi().findRegion("bless_Off"));
        buff_instant = new Image(atlas.getUi().findRegion("instant_Off"));
        buff_protect = new Image(atlas.getUi().findRegion("protect_Off"));
        buff_vampire = new Image(atlas.getUi().findRegion("vampire_Off"));

        debuff_curse = new Image(atlas.getUi().findRegion("curse_Off"));
        debuff_burn = new Image(atlas.getUi().findRegion("burn_Off"));
        debuff_poison = new Image(atlas.getUi().findRegion("poison_Off"));
        debuff_silence = new Image(atlas.getUi().findRegion("silence_Off"));

        mini_keys_bronze = new Image(atlas.getUi().findRegion("keyBronze"));
        mini_keys_silver = new Image(atlas.getUi().findRegion("keySilver"));
        mini_keys_gold = new Image(atlas.getUi().findRegion("keyGold"));
        mini_keys_red = new Image(atlas.getUi().findRegion("keyRed"));

        mini_attack = new Image(atlas.getUi().findRegion("attackSmall"));
        mini_defense = new Image(atlas.getUi().findRegion("defenseSmall"));
        mini_health = new Image(atlas.getUi().findRegion("healthSmall2"));
        mini_mana = new Image(atlas.getUi().findRegion("manaSmall2"));
        mini_experience = new Image(atlas.getUi().findRegion("experienceSmall"));
        mini_gold = new Image(atlas.getUi().findRegion("goldSmall"));
    }

    private void addImagesToMap() {
        buffsDebuffs.add(Const.BLESS, buff_bless);
        buffsDebuffs.add(Const.INSTANT_STRIKE, buff_instant);
        buffsDebuffs.add(Const.PROTECT, buff_protect);
        buffsDebuffs.add(Const.VAMPIRE_EDGE, buff_vampire);

        buffsDebuffs.add(Const.SILENCED, debuff_silence);
        buffsDebuffs.add(Const.BURNED, debuff_burn);
        buffsDebuffs.add(Const.POISONED, debuff_poison);
        buffsDebuffs.add(Const.CURSED, debuff_curse);

        updateImage(Const.BLESS, Const.REGION_BLESS);
        updateImage(Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE);
        updateImage(Const.PROTECT, Const.REGION_PROTECT);
        updateImage(Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE);

        updateImage(Const.POISONED, Const.REGION_POISONED);
        updateImage(Const.CURSED, Const.REGION_CURSED);
        updateImage(Const.BURNED, Const.REGION_BURNED);
        updateImage(Const.SILENCED, Const.REGION_SILENCED);
    }

    public void updateImage(int id, String imageRegion) {
        SpriteDrawable sprt;
        if (data.getBuffs().get(id) > 0) {
            sprt = new SpriteDrawable(new Sprite(atlas.getUi().findRegion(imageRegion + "_On")));
            buffsDebuffs.get(id).setDrawable(sprt);

        } else {
            sprt = new SpriteDrawable(new Sprite(atlas.getUi().findRegion(imageRegion + "_Off")));
            buffsDebuffs.get(id).setDrawable(sprt);
        }
    }

    public Game getMain() {
        return main;
    }

    public Fight getFight() {
        return fight;
    }

    public Label getKeys_bronze() {
        return keys_bronze;
    }

    public Label getKeys_silver() {
        return keys_silver;
    }

    public Label getKeys_gold() {
        return keys_gold;
    }

    public Label getKeys_red() {
        return keys_red;
    }

    public Label getAttack() {
        return attack;
    }

    public Label getDefense() {
        return defense;
    }

    public Label getHealth() {
        return health;
    }

    public Label getMana() {
        return mana;
    }

    public Label getExperience() {
        return experience;
    }

    public Label getGold() {
        return gold;
    }

    public Label getInstant_dur() {
        return instant_dur;
    }

    public Label getPoison_dur() {
        return poison_dur;
    }

    public Label getBurn_dur() {
        return burn_dur;
    }

    public Label getCurse_dur() {
        return curse_dur;
    }

    public Label getProtect_dur() {
        return protect_dur;
    }

    public Label getBless_dur() {
        return bless_dur;
    }

    public Label getSilence_dur() {
        return silence_dur;
    }

    public Label getVampire_dur() {
        return vampire_dur;
    }

    public Label getFloor() {
        return floor;
    }

    public DrawGameArea getDraw() {
        return draw;
    }

    public ControlInPlay getControl() {
        return control;
    }
}
