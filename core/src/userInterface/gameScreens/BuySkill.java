package userInterface.gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.game.Game;

import control.ControlBuySkill;
import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import userInterface.Ui;

public class BuySkill extends Ui {

    private AtlasManager atlas;
    private ControlBuySkill control;
    private Game main;
    private Table table;

    private Image instant;
    private Image protect;
    private Image bless;
    private Image cleanse;
    private Image vampire;
    private Image attack;
    private Image defense;
    private Image health;
    private Image mana;

    private Label attack_name;
    private Label defense_name;
    private Label health_name;
    private Label mana_name;
    private Label experience_name;

    private Label instant_name;
    private Label bless_name;
    private Label vampire_name;
    private Label protect_name;
    private Label cleanse_name;

    private Label cost_skill_name;
    private Label cost_stat_name;
    private Label cost_skill_value;
    private Label cost_stat_value;

    private Label attack_value;
    private Label defense_value;
    private Label health_value;
    private Label mana_value;
    private Label experience_value;

    private Label mana_increase_name;
    private Label mana_increase_value;
    private Label health_increase_name;
    private Label health_increase_value;
    private Label attack_increase_name;
    private Label attack_increase_value;
    private Label defense_increase_name;
    private Label defense_increase_value;

    private TextButton close;
    private TextButton buy_instant;
    private TextButton buy_protect;
    private TextButton buy_vampire;
    private TextButton buy_cleanse;
    private TextButton buy_bless;
    private TextButton buy_attack;
    private TextButton buy_health;
    private TextButton buy_defense;
    private TextButton buy_mana;

    private Table main_table_first;
    private Table main_table_second;
    private Table main_table_third;
    private Table table_instant;
    private Table table_bless;
    private Table table_protect;
    private Table table_vampire;
    private Table table_cleanse;
    private Table table_close;
    private Table table_attack;
    private Table table_defense;
    private Table table_health;
    private Table table_mana;

    public BuySkill(Game main) {
        super(main);
        super.labelSkin(10, 10);

        atlas = AtlasManager.getInstance();
        control = new ControlBuySkill(this);

        this.main = main;

        createButtons();
        createLabels();
        createImages();

        table = new Table();
        table.setFillParent(true);
        int zone = Const.zone(data.getCurrentFloor());
        table.setBackground(atlas.getScreenBackgrounds().get(zone - 1));

        main_table_first = new Table();
        main_table_second = new Table();
        main_table_third = new Table();

        table_instant = new Table();
        table_bless = new Table();
        table_protect = new Table();
        table_vampire = new Table();
        table_cleanse = new Table();
        table_close = new Table();

        table_attack = new Table();
        table_defense = new Table();
        table_health = new Table();
        table_mana = new Table();

        createScene();

        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    private void createScene() {
        table_attack.defaults().pad(5, 5, 5, 5);
        table_defense.defaults().pad(5, 5, 5, 5);
        table_health.defaults().pad(5, 5, 5, 5);
        table_mana.defaults().pad(5, 5, 5, 5);

        table_bless.defaults().pad(0, 5, 5, 5);
        table_cleanse.defaults().pad(0, 5, 5, 5);
        table_instant.defaults().pad(0, 5, 5, 5);
        table_protect.defaults().pad(0, 5, 5, 5);
        table_vampire.defaults().pad(0, 5, 5, 5);
        table_close.defaults().pad(0, 5, 5, 5);

        table.add(main_table_first);
        table.add(main_table_second);
        table.row();
        table.add(main_table_third).colspan(2);

        main_table_first.add(table_attack);
        main_table_first.row();
        main_table_first.add(table_defense);
        main_table_first.row();
        main_table_first.add(table_health);
        main_table_first.row();
        main_table_first.add(table_mana);
        main_table_first.row();

        main_table_second.add(table_bless, table_cleanse);
        main_table_second.row();
        main_table_second.add(table_instant, table_protect);
        main_table_second.row();
        main_table_second.add(table_vampire, table_close);
        main_table_second.row();

        main_table_third.add(cost_skill_name).width(60);
        main_table_third.add(cost_skill_value).width(40);
        main_table_third.add(cost_stat_name).width(60);
        main_table_third.add(cost_stat_value).width(45);
        main_table_third.add(attack_increase_name).width(60);
        main_table_third.add(attack_increase_value).width(25);
        main_table_third.add(defense_increase_name).width(60);
        main_table_third.add(defense_increase_value).width(25);
        main_table_third.add(health_increase_name).width(60);
        main_table_third.add(health_increase_value).width(45);
        main_table_third.add(mana_increase_name).width(60);
        main_table_third.add(mana_increase_value).width(35);

        table_attack.add(attack).width(40);
        table_attack.add(attack_name).width(70);
        table_attack.add(attack_value).width(40);
        table_attack.add(buy_attack).width(40);

        table_defense.add(defense).width(40);
        table_defense.add(defense_name).width(70);
        table_defense.add(defense_value).width(40);
        table_defense.add(buy_defense).width(40);

        table_health.add(health).width(40);
        table_health.add(health_name).width(70);
        table_health.add(health_value).width(40);
        table_health.add(buy_health).width(40);

        table_mana.add(mana).width(40);
        table_mana.add(mana_name).width(70);
        table_mana.add(mana_value).width(40);
        table_mana.add(buy_mana).width(40);

        table_bless.add(bless_name).colspan(2);
        table_bless.row();
        table_bless.add(bless).width(40);
        table_bless.add(buy_bless);

        table_cleanse.add(cleanse_name).colspan(2);
        table_cleanse.row();
        table_cleanse.add(cleanse).width(40);
        table_cleanse.add(buy_cleanse);

        table_instant.add(instant_name).colspan(2);
        table_instant.row();
        table_instant.add(instant).width(40);
        table_instant.add(buy_instant);

        table_protect.add(protect_name).colspan(2);
        table_protect.row();
        table_protect.add(protect).width(40);
        table_protect.add(buy_protect);

        table_vampire.add(vampire_name).colspan(2);
        table_vampire.row();
        table_vampire.add(vampire).width(40);
        table_vampire.add(buy_vampire);

        table_close.add(experience_name).colspan(2);
        table_close.row();
        table_close.add(experience_value).width(40);
        table_close.add(close);
    }

    public void updateInfoInViews() {
        AtlasManager manager = AtlasManager.getInstance();
        int zone = Const.zone(data.getCurrentFloor());

        attack_value.setText(String.valueOf(data.getStats().get(Const.ATTACK)));
        defense_value.setText(String.valueOf(data.getStats().get(Const.DEFENSE)));
        health_value.setText(String.valueOf(data.getStats().get(Const.HEALTH)));
        mana_value.setText(String.valueOf(data.getStats().get(Const.MANA)));

        cost_skill_value.setText(String.valueOf(control.getIncreaseSkills().calculateSkillCost(data.getSkillsBought())));
        cost_stat_value.setText(String.valueOf(control.getIncreaseSkills().calculateStatCost(data.getStatsBought())));

        attack_increase_value.setText(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.ATK_BASE_INCREASE))
        );
        defense_increase_value.setText(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.DEF_BASE_INCREASE))
        );
        health_increase_value.setText(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.HEALTH_BASIC_INCREASE))
        );
        mana_increase_value.setText(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.MANA_BASIC_INCREASE))
        );
        experience_value.setText(String.valueOf(data.getStats().get(Const.EXP)));
        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));

        instant.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE))));
        protect.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.PROTECT, Const.REGION_PROTECT))));
        cleanse.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.CLEANSE, Const.REGION_CLEANSE))));
        vampire.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE))));
        bless.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.BLESS, Const.REGION_BLESS))));

        main_table_first.clearChildren();
        main_table_second.clearChildren();
        main_table_third.clearChildren();

        table_instant.clearChildren();
        table_bless.clearChildren();
        table_protect.clearChildren();
        table_vampire.clearChildren();
        table_cleanse.clearChildren();
        table_close.clearChildren();

        table_attack.clearChildren();
        table_defense.clearChildren();
        table_health.clearChildren();
        table_mana.clearChildren();
        table.clearChildren();
        createButtons();
        createScene();
    }

    private void createLabels() {
        attack_name = new Label(Message.ATTACK, labelSkin);
        defense_name = new Label(Message.DEFENSE, labelSkin);
        health_name = new Label(Message.HEALTH, labelSkin);
        mana_name = new Label(Message.MANA, labelSkin);
        experience_name = new Label(Message.EXPERIENCE, labelSkin);

        instant_name = new Label(Message.NAME_INSTANT_STRIKE, labelSkin);
        bless_name = new Label(Message.NAME_BLESS, labelSkin);
        vampire_name = new Label(Message.NAME_VAMPIRE_EDGE, labelSkin);
        protect_name = new Label(Message.NAME_PROTECT, labelSkin);
        cleanse_name = new Label(Message.NAME_CLEANSE, labelSkin);

        cost_skill_name = new Label(Message.COST_SKILL, labelSkin);
        cost_stat_name = new Label(Message.COST_STAT, labelSkin);
        cost_skill_value = new Label(String.valueOf(control.getIncreaseSkills().calculateSkillCost(data.getSkillsBought())), labelSkin);
        cost_stat_value = new Label(String.valueOf(control.getIncreaseSkills().calculateStatCost(data.getStatsBought())), labelSkin);

        attack_value = new Label(String.valueOf(data.getStats().get(Const.ATTACK)), labelSkin);
        defense_value = new Label(String.valueOf(data.getStats().get(Const.DEFENSE)), labelSkin);
        health_value = new Label(String.valueOf(data.getStats().get(Const.HEALTH)), labelSkin);
        mana_value = new Label(String.valueOf(data.getStats().get(Const.MANA)), labelSkin);
        experience_value = new Label(String.valueOf(data.getStats().get(Const.EXP)), labelSkin);

        attack_increase_name = new Label(Message.INCREASE_ATK, labelSkin);
        attack_increase_value = new Label(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.ATK_BASE_INCREASE)),
                labelSkin
        );
        defense_increase_name = new Label(Message.INCREASE_DEF, labelSkin);
        defense_increase_value = new Label(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.DEF_BASE_INCREASE)),
                labelSkin
        );
        health_increase_name = new Label(Message.INCREASE_HEALTH, labelSkin);
        health_increase_value = new Label(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.HEALTH_BASIC_INCREASE)),
                labelSkin
        );
        mana_increase_name = new Label(Message.INCREASE_MANA, labelSkin);
        mana_increase_value = new Label(
                String.valueOf(control.getIncreaseSkills().calculateStatIncrease(data.getStatsBought(), Const.MANA_BASIC_INCREASE)),
                labelSkin
        );
    }

    private void createImages() {
        instant = new Image(findImage(Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE));
        protect = new Image(findImage(Const.PROTECT, Const.REGION_PROTECT));
        bless = new Image(findImage(Const.BLESS, Const.REGION_BLESS));
        cleanse = new Image(findImage(Const.CLEANSE, Const.REGION_CLEANSE));
        vampire = new Image(findImage(Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE));

        attack = new Image(atlas.getUi().findRegion("attackLarge"));
        defense = new Image(atlas.getUi().findRegion("defenseLarge"));
        health = new Image(atlas.getUi().findRegion("healthLarge2"));
        mana = new Image(atlas.getUi().findRegion("manaLarge2"));
    }

    public TextureAtlas.AtlasRegion findImage(int skill, String skillNameRegion) {
        TextureAtlas.AtlasRegion region;
        if (data.getSkills().get(skill)) {
            region = atlas.getUi().findRegion(skillNameRegion + "On");
        } else {
            region = atlas.getUi().findRegion(skillNameRegion + "Off");
        }
        return region;
    }

    private TextButton makeButton(String message, Skin skin, ClickListener listener) {
        TextButton txtButton = new TextButton(message, skin);
        txtButton.addListener(listener);
        return txtButton;
    }

    private void createButtons() {
        int zone = Const.zone(data.getCurrentFloor());
        Skin skin = atlas.getSmallButtonsSkin().get(zone - 1);

        close = makeButton(Message.DIALOG_CLOSE, skin, control.getCloseButton());
        buy_instant = makeButton(Message.DIALOG_BUY, skin, control.getBuyInstant());
        buy_protect = makeButton(Message.DIALOG_BUY, skin, control.getBuyProtect());
        buy_vampire = makeButton(Message.DIALOG_BUY, skin, control.getBuyVampire());
        buy_cleanse = makeButton(Message.DIALOG_BUY, skin, control.getBuyCleanse());
        buy_bless = makeButton(Message.DIALOG_BUY, skin, control.getBuyBless());
        buy_attack = makeButton(Message.DIALOG_BUY, skin, control.getBuyAttack());
        buy_health = makeButton(Message.DIALOG_BUY, skin, control.getBuyHealth());
        buy_defense = makeButton(Message.DIALOG_BUY, skin, control.getBuyDefense());
        buy_mana = makeButton(Message.DIALOG_BUY, skin, control.getBuyMana());
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public Game getMain() {
        return main;
    }

    public Label getCost_skill_value() {
        return cost_skill_value;
    }

    public Label getCost_stat_value() {
        return cost_stat_value;
    }

    public Label getAttack_value() {
        return attack_value;
    }

    public Label getDefense_value() {
        return defense_value;
    }

    public Label getHealth_value() {
        return health_value;
    }

    public Label getMana_value() {
        return mana_value;
    }

    public Image getInstant() {
        return instant;
    }

    public Image getProtect() {
        return protect;
    }

    public Image getBless() {
        return bless;
    }

    public Image getCleanse() {
        return cleanse;
    }

    public Image getVampire() {
        return vampire;
    }

    public Label getExperience_value() {
        return experience_value;
    }

    public Label getMana_increase_value() {
        return mana_increase_value;
    }

    public Label getHealth_increase_value() {
        return health_increase_value;
    }

    public Label getDefense_increase_value() {
        return defense_increase_value;
    }

    public Label getAttack_increase_value() {
        return attack_increase_value;
    }
}
