package userInterface.gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.Game;

import gameObjects.terrain.Stairs;
import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.Ui;

public class Fly extends Ui {

    private Table table;
    private TextButton close;

    private Game main;
    private Label message;

    public Fly(Game main) {
        super(main);
        this.main = main;

        super.labelSkin(10, 10);

        table = new Table();
        message = new Label(Message.FLY_MESSAGE, labelSkin);

        table.defaults().align(Align.center).pad(5, 5, 5, 5);

        close = createCancelButton();
        stage.addActor(table);
        table.setFillParent(true);
        int zone = Const.zone(data.getCurrentFloor());
        table.setBackground(AtlasManager.getInstance().getScreenBackgrounds().get(zone - 1));

        createButtons();

        table.add(message).colspan(table.getColumns()).align(Align.center);
        table.row();
        table.add(close).colspan(table.getColumns()).align(Align.center);

        Gdx.input.setInputProcessor(stage);
    }

    public void updateInfoInViews() {
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();
        table.clearChildren();
        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));
        close = createCancelButton();
        createButtons();
        table.add(message).colspan(table.getColumns()).align(Align.center);
        table.row();
        table.add(close).colspan(table.getColumns()).align(Align.center);
    }

    private void createButtons() {
        int x = 0;
        int zone = Const.zone(data.getCurrentFloor());
        while (x < data.getMaxFloor()) {
            TextButton butt = new TextButton(String.valueOf(x + 1), AtlasManager.getInstance().getSmallButtonsSkin().get(zone - 1));
            butt.setName(String.valueOf(x + 1));
            butt.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                    int g = Integer.parseInt(event.getListenerActor().getName());
                    flyToFloor(g);
                    main.getStateMachine().changeState(GameState.IN_PLAY);
                }
            });
            if (x % 10 == 0) {
                table.row();
            }
            table.add(butt);
            x++;
        }
        table.row();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    private TextButton createCancelButton() {
        int zone = Const.zone(data.getCurrentFloor());
        TextButton close = new TextButton(Message.DIALOG_CANCEL, AtlasManager.getInstance().getMediumButtonsSkin().get(zone - 1));
        close.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                        main.getStateMachine().changeState(GameState.IN_PLAY);
                    }
                }
        );
        return close;
    }

    private void flyToFloor(int toWhichFloor) {
        data.getLevels().set(data.getCurrentFloor() - 1, data.getCurrentMap());
        data.setCurrentMap(data.getLevels().get(toWhichFloor - 1));
        Stairs.Type type;
        if (toWhichFloor > data.getCurrentFloor()) {
            type = Stairs.Type.DOWN;
        } else if (toWhichFloor == data.getCurrentFloor()) {
            type = Stairs.Type.DOWN;
        } else {
            type = Stairs.Type.UP;
        }
        boolean stairsNotFound = true;
        int x = 0;
        int y = 0;
        while (stairsNotFound && x < Const.MAP_X) {
            while (y < Const.MAP_Y) {
                if (data.getCurrentMap()[x][y] instanceof Stairs) {
                    if (type.equals(((Stairs) data.getCurrentMap()[x][y]).getType())) {
                        stairsNotFound = false;
                        data.setPosX(x);
                        data.setPosY(y);
                    }
                }
                y++;
            }
            x++;
            y = 0;
        }
        data.setCurrentFloor(toWhichFloor);
    }

}
