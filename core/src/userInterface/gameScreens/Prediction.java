package userInterface.gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.game.Game;

import java.util.ArrayList;

import gameObjects.actors.Monster;
import gamePlay.Fight;
import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.Ui;

public class Prediction extends Ui {

    private TextButton close;
    private Game main;
    private Table table;

    public Prediction(Game main) {
        super(main);
        super.labelSkin(10, 10);

        this.main = main;
        table = new Table();
        close = createButton();

        stage.addActor(table);
        table.setFillParent(true);
        int zone = Const.zone(data.getCurrentFloor());
        table.setBackground(AtlasManager.getInstance().getScreenBackgrounds().get(zone - 1));

        checkWhatToShow();

        table.add(close).padTop(5);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public void updateInfoInViews() {
        table.clearChildren();
        table.setBackground(AtlasManager.getInstance().getScreenBackgrounds().get(Const.zone(data.getCurrentFloor()) - 1));
        close = createButton();
        checkWhatToShow();
        table.add(close).padTop(5);
    }

    private void checkWhatToShow() {
        int x = 0;
        int y = 0;

        ArrayList<Monster.Type> monsters = new ArrayList<Monster.Type>();

        while (x < Const.MAP_X) {
            while (y < Const.MAP_Y) {

                if (data.getCurrentMap()[x][y] instanceof Monster) {

                    if (monsters.isEmpty()) {
                        monsters.add(((Monster) data.getCurrentMap()[x][y]).getType());
                    } else {
                        boolean addThisType = true;
                        for (Monster.Type type : monsters) {
                            if (type == ((Monster) data.getCurrentMap()[x][y]).getType()) {
                                addThisType = false;
                            }
                        }
                        if (addThisType) {
                            monsters.add(((Monster) data.getCurrentMap()[x][y]).getType());
                        }
                    }
                }
                y++;
            }
            x++;
            y = 0;
        }
        int z = 0;
        while (z < monsters.size()) {
            table.add(createTable(monsters.get(z))).padTop(5);
            table.row();
            z++;
        }
    }

    private Table createTable(Monster.Type type) {
        Table table = new Table();
        Table tableSkills = new Table();
        Fight fight = new Fight();
        Monster monster = new Monster(type);
        AtlasManager manager = AtlasManager.getInstance();

        int penetration = (int) monster.getPenetration();
        int regeneration = monster.getRegeneration();
        int reflect = monster.getReflect();
        int manaDrain = monster.getManaDrain();
        boolean endure = monster.isEndure();
        boolean firstStrike = monster.isFirstStrike();
        boolean bombResistant = monster.isBombResistant();

        int attack = monster.getAttack();
        int defense = monster.getDefense();
        int health = monster.getHealth();
        int gold = monster.getGold();
        int exp = monster.getExperience();
        int expectedDamage = fight.battleSimulation(monster);

        Image image = new Image(manager.getMonsters().findRegion(monster.getImage()));
        Image attack_icon = new Image(manager.getUi().findRegion("attackSmall"));
        Image defense_icon = new Image(manager.getUi().findRegion("defenseSmall"));
        Image health_icon = new Image(manager.getUi().findRegion("healthSmall2"));
        Image gold_icon = new Image(manager.getUi().findRegion("goldSmall"));
        Image exp_icon = new Image(manager.getUi().findRegion("experienceSmall"));
        Image damage_icon = new Image(manager.getUi().findRegion("damage"));

        String penetrationRegion;
        String firstStrikeRegion;
        String endureRegion;
        String regenerationRegion;
        String reflectRegion;
        String manaDrainRegion;
        String bombResistantRegion;

        if (penetration > 0) {
            penetrationRegion = "penetrationOn";
        } else {
            penetrationRegion = "penetrationOff";
        }
        if (firstStrike) {
            firstStrikeRegion = "firstStrikeOn";
        } else {
            firstStrikeRegion = "firstStrikeOff";
        }
        if (endure) {
            endureRegion = "endureOn";
        } else {
            endureRegion = "endureOff";
        }
        if (regeneration > 0) {
            regenerationRegion = "regenerationOn";
        } else {
            regenerationRegion = "regenerationOff";
        }
        if (reflect > 0) {
            reflectRegion = "reflectOn";
        } else {
            reflectRegion = "reflectOff";
        }
        if (manaDrain > 0) {
            manaDrainRegion = "manaDrainOn";
        } else {
            manaDrainRegion = "manaDrainOff";
        }
        if (bombResistant) {
            bombResistantRegion = "bombResistanceOn";
        } else {
            bombResistantRegion = "bombResistanceOff";
        }

        Image firstStrike_icon = new Image(manager.getUi().findRegion(firstStrikeRegion));
        Image endure_icon = new Image(manager.getUi().findRegion(endureRegion));
        Image penetration_icon = new Image(manager.getUi().findRegion(penetrationRegion));
        Image regeneration_icon = new Image(manager.getUi().findRegion(regenerationRegion));
        Image reflect_icon = new Image(manager.getUi().findRegion(reflectRegion));
        Image manaDrain_icon = new Image(manager.getUi().findRegion(manaDrainRegion));
        Image bombResistant_icon = new Image(manager.getUi().findRegion(bombResistantRegion));

        String expectedDamageMessage = String.valueOf(expectedDamage);

        Label penetration_value = new Label(String.valueOf(penetration), labelSkin);
        Label reflect_value = new Label(String.valueOf(reflect), labelSkin);
        Label manaDrain_value = new Label(String.valueOf(manaDrain), labelSkin);
        Label regeneration_value = new Label(String.valueOf(regeneration), labelSkin);

        Label attack_value = new Label(String.valueOf(attack), labelSkin);
        Label defense_value = new Label(String.valueOf(defense), labelSkin);
        Label health_value = new Label(String.valueOf(health), labelSkin);
        Label gold_value = new Label(String.valueOf(gold), labelSkin);
        Label exp_value = new Label(String.valueOf(exp), labelSkin);

        if (expectedDamage == -99999999) {
            expectedDamageMessage = Message.CANT_ATTACK;
        }

        Label damage_value = new Label(String.valueOf(expectedDamageMessage), labelSkin);

        int imageWidth = (int) image.getWidth();
        int iconWidth = (int) attack_icon.getWidth();

        tableSkills.add(penetration_icon).width(iconWidth);
        tableSkills.add(penetration_value).width(20).padLeft(3);
        tableSkills.add(reflect_icon).width(iconWidth);
        tableSkills.add(reflect_value).width(20).padLeft(3);
        tableSkills.add(firstStrike_icon).width(iconWidth);
        tableSkills.add(bombResistant_icon).width(iconWidth).padLeft(5);
        tableSkills.row();
        tableSkills.add(regeneration_icon).width(iconWidth);
        tableSkills.add(regeneration_value).width(20).padLeft(3);
        tableSkills.add(manaDrain_icon).width(iconWidth);
        tableSkills.add(manaDrain_value).width(20).padLeft(3);
        tableSkills.add(endure_icon).width(iconWidth);

        table.defaults().pad(5, 5, 5, 0);
        table.add(image).width(imageWidth);
        table.add(attack_icon).width(iconWidth);
        table.add(attack_value).width(25);
        table.add(defense_icon).width(iconWidth);
        table.add(defense_value).width(25);
        table.add(health_icon).width(iconWidth);
        table.add(health_value).width(25);
        table.add(gold_icon).width(iconWidth);
        table.add(gold_value).width(25);
        table.add(exp_icon).width(iconWidth);
        table.add(exp_value).width(25);
        table.add(tableSkills).width(140);
        table.add(damage_icon).width(iconWidth);
        table.add(damage_value).width(60);

        int zone = Const.zone(data.getCurrentFloor());
        TextureAtlas atlas = new TextureAtlas("atlasPredictBackground.pack");

        table.setBackground(new SpriteDrawable(new Sprite(atlas.findRegion("predictTable", zone))) {
        });

        return table;
    }

    private TextButton createButton() {
        int zone = Const.zone(data.getCurrentFloor());
        TextButton button = new TextButton(Message.DIALOG_CONFIRM, AtlasManager.getInstance().getMediumButtonsSkin().get(zone - 1));
        button.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                        main.getStateMachine().changeState(GameState.IN_PLAY);
                    }
                }
        );
        return button;
    }

    public Game getMain() {
        return main;
    }
}
