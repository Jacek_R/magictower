package userInterface.gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.Game;

import control.ControlUseSkill;
import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import userInterface.Ui;

public class UseSkill extends Ui {

    private AtlasManager atlas;
    private ControlUseSkill control;
    private Game main;

    private Image image_instant_strike;
    private Image image_protect;
    private Image image_bless;
    private Image image_vampire;
    private Image image_cleanse;
    private Image image_bomb;
    private Image image_key;
    private Image image_potion;
    private Image image_mana;

    private Label cost_instant_strike;
    private Label cost_protect;
    private Label cost_bless;
    private Label cost_cleanse;
    private Label cost_vampire;

    private Label amount_bomb;
    private Label amount_key;
    private Label amount_potion;
    private Label amount_mana;

    private Label name_instant_strike;
    private Label name_protect;
    private Label name_bless;
    private Label name_cleanse;
    private Label name_vampire;
    private Label name_bomb;
    private Label name_potion;
    private Label name_key;
    private Label name_mana;

    private TextButton use_instant_strike;
    private TextButton use_bless;
    private TextButton use_protect;
    private TextButton use_vampire;
    private TextButton use_cleanse;
    private TextButton use_bomb;
    private TextButton use_key;
    private TextButton use_potion;
    private TextButton cancel;

    private Table table;
    private Table skill_instant_strike;
    private Table skill_cleanse;
    private Table skill_vampire;
    private Table skill_protect;
    private Table skill_bless;
    private Table item_bomb;
    private Table item_key;
    private Table item_potion;
    private Table cancel_and_status;


    public UseSkill(final Game main) {
        super(main);
        super.labelSkin(10, 10);

        this.main = main;
        table = new Table();
        atlas = AtlasManager.getInstance();

        stage.addActor(table);
        table.setFillParent(true);
        int zone = Const.zone(data.getCurrentFloor());
        table.setBackground(atlas.getScreenBackgrounds().get(zone - 1));

        table.align(Align.center);
        table.defaults().pad(5, 5, 5, 5);

        control = new ControlUseSkill(this);

        skill_instant_strike = new Table();
        skill_cleanse = new Table();
        skill_vampire = new Table();
        skill_protect = new Table();
        skill_bless = new Table();
        item_bomb = new Table();
        item_key = new Table();
        item_potion = new Table();
        cancel_and_status = new Table();

        createLabels();
        createButtons();
        createImages();
        createScene();

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    private void createScene() {
        skill_bless.add(name_bless);
        skill_bless.row();
        skill_bless.add(image_bless).align(Align.center);
        skill_bless.row();
        skill_bless.add(cost_bless);
        skill_bless.row();
        skill_bless.add(use_bless);

        skill_cleanse.add(name_cleanse);
        skill_cleanse.row();
        skill_cleanse.add(image_cleanse).align(Align.center);
        skill_cleanse.row();
        skill_cleanse.add(cost_cleanse);
        skill_cleanse.row();
        skill_cleanse.add(use_cleanse);

        skill_instant_strike.add(name_instant_strike);
        skill_instant_strike.row();
        skill_instant_strike.add(image_instant_strike).align(Align.center);
        skill_instant_strike.row();
        skill_instant_strike.add(cost_instant_strike);
        skill_instant_strike.row();
        skill_instant_strike.add(use_instant_strike);

        skill_protect.add(name_protect);
        skill_protect.row();
        skill_protect.add(image_protect).align(Align.center);
        skill_protect.row();
        skill_protect.add(cost_protect);
        skill_protect.row();
        skill_protect.add(use_protect);

        skill_vampire.add(name_vampire);
        skill_vampire.row();
        skill_vampire.add(image_vampire).align(Align.center);
        skill_vampire.row();
        skill_vampire.add(cost_vampire);
        skill_vampire.row();
        skill_vampire.add(use_vampire);

        item_bomb.add(name_bomb);
        item_bomb.row();
        item_bomb.add(image_bomb).align(Align.center);
        item_bomb.row();
        item_bomb.add(amount_bomb);
        item_bomb.row();
        item_bomb.add(use_bomb);

        item_key.add(name_key);
        item_key.row();
        item_key.add(image_key).align(Align.center);
        item_key.row();
        item_key.add(amount_key);
        item_key.row();
        item_key.add(use_key);

        item_potion.add(name_potion);
        item_potion.row();
        item_potion.add(image_potion).align(Align.center);
        item_potion.row();
        item_potion.add(amount_potion);
        item_potion.row();
        item_potion.add(use_potion);

        cancel_and_status.add(name_mana);
        cancel_and_status.row();
        cancel_and_status.add(image_mana).align(Align.center);
        cancel_and_status.row();
        cancel_and_status.add(amount_mana);
        cancel_and_status.row();
        cancel_and_status.add(cancel).align(Align.bottom);

        table.add(skill_bless, skill_cleanse, skill_instant_strike);
        table.row();
        table.add(skill_protect, cancel_and_status, skill_vampire);
        table.row();
        table.add(item_bomb, item_key, item_potion);
    }

    public void updateInfoInViews() {
        int zone = Const.zone(data.getCurrentFloor());
        table.setBackground(atlas.getScreenBackgrounds().get(zone - 1));

        skill_instant_strike.clearChildren();
        skill_cleanse.clearChildren();
        skill_vampire.clearChildren();
        skill_protect.clearChildren();
        skill_bless.clearChildren();
        item_bomb.clearChildren();
        item_key.clearChildren();
        item_potion.clearChildren();
        cancel_and_status.clearChildren();

        table.clearChildren();

        createButtons();
        createScene();

        amount_bomb.setText(Message.AMOUNT + data.getItems().get(Const.BOMB));
        amount_key.setText(Message.AMOUNT + data.getItems().get(Const.MASTER_KEY));
        amount_potion.setText(Message.AMOUNT + data.getItems().get(Const.POTION));
        amount_mana.setText(Message.AMOUNT + data.getStats().get(Const.MANA));

        image_bless.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.BLESS, Const.REGION_BLESS))));
        image_cleanse.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.CLEANSE, Const.REGION_CLEANSE))));
        image_protect.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.PROTECT, Const.REGION_PROTECT))));
        image_instant_strike.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE))));
        image_vampire.setDrawable(new SpriteDrawable(new Sprite(findImage(Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE))));
    }

    private void createLabels() {
        cost_bless = new Label(Message.COST + Const.BLESS_COST, labelSkin);
        cost_cleanse = new Label(Message.COST + Const.CLEANSE_COST, labelSkin);
        cost_instant_strike = new Label(Message.COST + Const.INSTANT_STRIKE_COST, labelSkin);
        cost_protect = new Label(Message.COST + Const.PROTECT_COST, labelSkin);
        cost_vampire = new Label(Message.COST + Const.VAMPIRE_COST, labelSkin);

        name_bless = new Label(Message.NAME_BLESS, labelSkin);
        name_cleanse = new Label(Message.NAME_CLEANSE, labelSkin);
        name_instant_strike = new Label(Message.NAME_INSTANT_STRIKE, labelSkin);
        name_protect = new Label(Message.NAME_PROTECT, labelSkin);
        name_vampire = new Label(Message.NAME_VAMPIRE_EDGE, labelSkin);

        name_bomb = new Label(Message.NAME_BOMB, labelSkin);
        name_key = new Label(Message.NAME_MASTER_KEY, labelSkin);
        name_potion = new Label(Message.NAME_POTION, labelSkin);
        name_mana = new Label(Message.MANA, labelSkin);

        amount_bomb = new Label(Message.AMOUNT + data.getItems().get(Const.BOMB), labelSkin);
        amount_key = new Label(Message.AMOUNT + data.getItems().get(Const.MASTER_KEY), labelSkin);
        amount_potion = new Label(Message.AMOUNT + data.getItems().get(Const.POTION), labelSkin);
        amount_mana = new Label(Message.AMOUNT + data.getStats().get(Const.MANA), labelSkin);
    }

    private void createImages() {

        image_bomb = new Image(atlas.getUi().findRegion(Const.REGION_BOMB));
        image_key = new Image(atlas.getUi().findRegion(Const.REGION_MASTER_KEY));
        image_potion = new Image(atlas.getUi().findRegion(Const.REGION_POTION));
        image_mana = new Image(atlas.getUi().findRegion("manaLarge2"));

        image_instant_strike = new Image(findImage(Const.INSTANT_STRIKE, Const.REGION_INSTANT_STRIKE));
        image_bless = new Image(findImage(Const.BLESS, Const.REGION_BLESS));
        image_cleanse = new Image(findImage(Const.CLEANSE, Const.REGION_CLEANSE));
        image_vampire = new Image(findImage(Const.VAMPIRE_EDGE, Const.REGION_VAMPIRE_EDGE));
        image_protect = new Image(findImage(Const.PROTECT, Const.REGION_PROTECT));
    }

    private TextureAtlas.AtlasRegion findImage(int skill, String skillRegion) {
        TextureAtlas.AtlasRegion region;
        if (data.getSkills().get(skill)) {
            region = atlas.getUi().findRegion(skillRegion + "On");
        } else {
            region = atlas.getUi().findRegion(skillRegion + "Off");
        }
        return region;
    }


    private TextButton makeButton(String message, Skin skin, ClickListener listener) {
        TextButton button = new TextButton(message, skin);
        button.addListener(listener);
        return button;
    }

    private void createButtons() {
        int zone = Const.zone(data.getCurrentFloor());
        Skin skin = atlas.getMediumButtonsSkin().get(zone - 1);

        use_instant_strike = makeButton(Message.USE, skin, control.getInstantStrike());
        use_bless = makeButton(Message.USE, skin, control.getBless());
        use_bomb = makeButton(Message.USE, skin, control.getBomb());
        use_cleanse = makeButton(Message.USE, skin, control.getCleanse());
        use_key = makeButton(Message.USE, skin, control.getKey());
        use_vampire = makeButton(Message.USE, skin, control.getVampire());
        use_protect = makeButton(Message.USE, skin, control.getProtect());
        use_potion = makeButton(Message.USE, skin, control.getPotion());
        cancel = makeButton(Message.DIALOG_CANCEL, skin, control.getCancel());
    }

    public Game getMain() {
        return main;
    }

    public Label getAmountMana() {
        return amount_mana;
    }

    public Label getAmount_potion() {
        return amount_potion;
    }

    public Label getAmount_key() {
        return amount_key;
    }

    public Label getAmount_bomb() {
        return amount_bomb;
    }
}
