package userInterface.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Game;

import java.util.ArrayList;

import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.Ui;
import utils.DataManager;
import utils.Encrypter;

public class SaveGame extends Ui {

    private Table table;
    private Game main;
    private Label message;
    private TextButton close;

    private ArrayList<Integer> slots;

    public SaveGame(Game main) {
        super(main);
        super.labelSkin(10, 10);
        this.main = main;

        table = new Table();
        table.defaults().pad(5, 5, 5, 5);
        table.setFillParent(true);
        int zone = Const.zone(data.getCurrentFloor());
        table.setBackground(AtlasManager.getInstance().getScreenBackgrounds().get(zone - 1));

        createSlots();

        message = new Label(Message.SAVE_MESSAGE, labelSkin);
        close = new TextButton(Message.DIALOG_CLOSE, AtlasManager.getInstance().getLargeButtonsSkin().get(zone - 1));
        close.addListener(closeListener());

        createTable();

        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    private ClickListener closeListener() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                main.getStateMachine().changeState(GameState.IN_PLAY);
            }
        };
    }

    public void updateInfoInViews() {
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();

        table.clearChildren();
        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));
        close = new TextButton(Message.DIALOG_CLOSE, manager.getLargeButtonsSkin().get(zone - 1));
        close.addListener(closeListener());
        createTable();
    }

    private void createTable() {
        table.add(message).colspan(2);
        table.row();
        createButtons();
        table.add(close).colspan(2);
    }

    private void createButtons() {
        int x = 0;
        int zone = Const.zone(data.getCurrentFloor());
        TextButton button;
        while (x < slots.size()) {
            button = new TextButton("", AtlasManager.getInstance().getLargeButtonsSkin().get(zone - 1));
            button.setName(String.valueOf(x + 1));

            if (!prefExist(String.valueOf(x + 1), PlayerData.date)) {
                button.setText(Message.EMPTY);
            } else {
                String s = "";
                try {
                    s = Encrypter.decrypt(DataManager.loadPreferences(String.valueOf(x + 1), PlayerData.date));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
                button.setText(s);
            }

            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                    data.saveGame(String.valueOf(event.getListenerActor().getName()));
                    main.getStateMachine().changeState(GameState.IN_PLAY);
                }
            });
            if (x % 2 == 0) {
                table.row();
            }
            table.add(button);
            x++;
        }
        table.row();
    }

    private boolean prefExist(String prefname, String keyInPrefName) {
        Preferences pref = Gdx.app.getPreferences(prefname);
        String x = pref.getString(keyInPrefName);
        return !x.isEmpty();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    private void createSlots() {
        slots = new ArrayList<Integer>();
        int numberOfSlots = 10;
        int x = 0;

        while (x < numberOfSlots) {
            slots.add(x);
            x++;
        }
    }
}
