package userInterface.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.Game;

import globalVariables.Const;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.Ui;

public class MainMenu extends Ui {

    private final static String NEW_GAME_BUTTON = "New game";
    private final static String LOAD_GAME_BUTTON = "Load game";
    private final static String OPTIONS_BUTTON = "Options";
    private final static String EXIT_BUTTON = "Exit";
    private static final String CREDITS_BUTTON = "Credits";

    public MainMenu(Game main) {
        super(main);
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();
        TextButton btn_newGame;
        btn_newGame = new TextButton(NEW_GAME_BUTTON, manager.getLargeButtonsSkin().get(zone - 1));
        btn_newGame.addListener(newGame());

        TextButton btn_loadGame;
        btn_loadGame = new TextButton(LOAD_GAME_BUTTON, manager.getLargeButtonsSkin().get(zone - 1));
        btn_loadGame.addListener(loadGame());

        TextButton btn_options;
        btn_options = new TextButton(OPTIONS_BUTTON, manager.getLargeButtonsSkin().get(zone - 1));
        btn_options.addListener(options());

        TextButton btn_credits;
        btn_credits = new TextButton(CREDITS_BUTTON, manager.getLargeButtonsSkin().get(zone - 1));
        btn_credits.addListener(credits());

        TextButton btn_exitGame;
        btn_exitGame = new TextButton(EXIT_BUTTON, manager.getLargeButtonsSkin().get(zone - 1));
        btn_exitGame.addListener(exitGame());

        Table table;
        table = new Table();
        table.setFillParent(true);
        table.align(Align.center);
        table.defaults().padTop(5);
        table.defaults().padBottom(5);
        table.defaults().padLeft(10);
        table.defaults().padRight(10);
        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));

        table.add(btn_newGame);
        table.row();
        table.add(btn_loadGame);
        table.row();
        table.add(btn_options);
        table.row();
        table.add(btn_credits);
        table.row();
        table.add(btn_exitGame);

        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    private ClickListener newGame() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                data.createInitialData();
                data.loadMap(data.getCurrentFloor());
                main.getStateMachine().changeState(GameState.IN_PLAY);
            }
        };
    }

    private ClickListener loadGame() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                main.getStateMachine().changeState(GameState.LOAD_GAME);
            }
        };
    }

    private ClickListener options() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                main.getStateMachine().changeState(GameState.OPTIONS);
            }
        };
    }

    private ClickListener credits() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                main.getStateMachine().changeState(GameState.CREDITS);
            }
        };
    }

    private ClickListener exitGame() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                Gdx.app.exit();
            }
        };
    }
}
