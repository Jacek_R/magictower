package userInterface.menus;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.Game;

import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import music.SoundEffect;
import userInterface.Ui;

public class Help extends Ui {

    private Table buttonsTable;
    private Table table;

    private Table basics;
    private Table gamePlay;
    private Table stats;
    private Table playerSkills;
    private Table monsterSkills;
    private Table items;
    private Table traps;
    private Table debuffs;
    private Table interfaceTbl;

    private TextButton close;
    private TextButton debuffsButton;
    private TextButton trapsButton;
    private TextButton basicsButton;
    private TextButton statsButton;
    private TextButton gameplayButton;
    private TextButton playerSkillsButton;
    private TextButton monsterSkillsButton;
    private TextButton itemsButton;
    private TextButton interfaceButton;

    public Help(Game main) {
        super(main);
        this.main = main;

        super.labelSkin(10, 10);

        table = new Table();
        buttonsTable = new Table();

        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();
        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));
        table.setFillParent(true);

        createButtons();

        basicsTable();
        statsTable();
        gamePlayTable();
        playerSkillsTable();
        playerSkillsTable();
        monsterSkillsTable();
        itemsTable();
        trapsTable();
        debuffsTable();
        interfaceTable();

        buttonsTable.defaults().padTop(5).padLeft(5);
        createButtonsTable();
        createScene();

        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    private void createButtonsTable() {
        buttonsTable.add(basicsButton);
        buttonsTable.row();
        buttonsTable.add(gameplayButton);
        buttonsTable.row();
        buttonsTable.add(statsButton);
        buttonsTable.row();
        buttonsTable.add(itemsButton);
        buttonsTable.row();
        buttonsTable.add(playerSkillsButton);
        buttonsTable.row();
        buttonsTable.add(monsterSkillsButton);
        buttonsTable.row();
        buttonsTable.add(trapsButton);
        buttonsTable.row();
        buttonsTable.add(debuffsButton);
        buttonsTable.row();
        buttonsTable.add(interfaceButton);
    }

    private void createScene() {
        table.add(buttonsTable).align(Align.left).colspan(1);
        table.add(this.basics).padLeft(5).expandX().colspan(1);
        table.row();
        table.add(close).padTop(5).colspan(2).align(Align.center);
    }

    public void updateInfoInViews() {
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();
        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));
        buttonsTable.clearChildren();
        table.clearChildren();
        createButtons();
        createButtonsTable();
        createScene();
    }

    private void createButtons() {
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();
        Skin skin = manager.getMediumButtonsSkin().get(zone - 1);

        basicsButton = makeButton("Basics", skin, basics());
        statsButton = makeButton("Stats", skin, stats());
        gameplayButton = makeButton("Gameplay", skin, gamePlay());
        playerSkillsButton = makeButton("Player skills", skin, playerSkills());
        monsterSkillsButton = makeButton("Monster skills", skin, monsterSkills());
        itemsButton = makeButton("Items", skin, items());
        trapsButton = makeButton("Traps", skin, traps());
        debuffsButton = makeButton("Debuffs", skin, debuffs());
        interfaceButton = makeButton("Interface", skin, interfaceListener());
        close = makeButton(Message.DIALOG_CANCEL, skin, close());
    }

    private TextButton makeButton(String message, Skin skin, ClickListener listener) {
        TextButton button = new TextButton(message, skin);
        button.addListener(listener);
        return button;
    }

    private void trapsTable() {
        traps = new Table();
        AtlasManager manager = AtlasManager.getInstance();
        Label label = new Label("In the tower you will encounter various traps. They are activeted when you step on a tile with a trap.\n" +
                "You can protect yourself from a trap with skill protect. If this skill is in effect, then a trap will not be triggered\n" +
                "", labelSkin);

        Label curseDesc = new Label("Curse - inflicts curse debuff", labelSkin);
        Label spearDesc = new Label("Spear - deals damage to the player", labelSkin);
        Label silenceDesc = new Label("Silence - inflicts silence debuff", labelSkin);
        Label flameDesc = new Label("Flame - inflicts burn debuff", labelSkin);
        Label magicFlameDesc = new Label("Magic flame - decreases mana points", labelSkin);
        Label poisonDesc = new Label("Poison - inflicts poison debuff", labelSkin);
        Label leftDesc = new Label("Left - if you standing on this trap, you can only move left from here", labelSkin);
        Label rightDesc = new Label("Right - if you standing on this trap, you can only move right from here", labelSkin);
        Label upDesc = new Label("Up - if you standing on this trap, you can only move up from here", labelSkin);
        Label downDesc = new Label("Down - if you standing on this trap, you can only move down from here", labelSkin);

        Image curse = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("curse"))));
        Image spear = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("spear"))));
        Image silence = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("silence"))));
        Image flame = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("flame"))));
        Image magicFlame = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("magicFlame"))));
        Image poison = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("poison"))));
        Image left = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("left"))));
        Image right = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("right"))));
        Image up = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("up"))));
        Image down = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("down"))));

        traps.defaults().colspan(1).align(Align.left).pad(0, 5, 5, 5).height(40);

        traps.add(label).colspan(2);
        traps.row();

        traps.add(curse);
        traps.add(curseDesc);
        traps.row();

        traps.add(flame);
        traps.add(flameDesc);
        traps.row();

        traps.add(magicFlame);
        traps.add(magicFlameDesc);
        traps.row();

        traps.add(poison);
        traps.add(poisonDesc);
        traps.row();

        traps.add(silence);
        traps.add(silenceDesc);
        traps.row();

        traps.add(spear);
        traps.add(spearDesc);
        traps.row();

        traps.add(left);
        traps.add(leftDesc);
        traps.row();

        traps.add(right);
        traps.add(rightDesc);
        traps.row();

        traps.add(up);
        traps.add(upDesc);
        traps.row();

        traps.add(down);
        traps.add(downDesc);
        traps.row();

    }


    private void debuffsTable() {
        debuffs = new Table();
        AtlasManager manager = AtlasManager.getInstance();
        Label label = new Label("Debuffs are negative effects caused when a player walks in the certain traps.\n" +
                "Debuff is in effect until counter reachs zero. Counter of each debuff decreases by 1 when\n" +
                "player kills a monster. When the trap is triggered, debuff is set to 3, unless player have adequate shield.\n" +
                "Then, the duration is decreased by 1. For example, if player have a shield with protection from poison,\n" +
                "then poisonous trap sets counter to 2, instead of 3. The four debuffs are: ", labelSkin);

        Label poisonDesc = new Label("Poison - halves amount of restoration received from red, green and blue potions", labelSkin);
        Label silenceDesc = new Label("Silence - prevents you from using skills", labelSkin);
        Label curseDesc = new Label("Curse - doubles damage received from monsters attacks", labelSkin);
        Label burnDesc = new Label("Burn - each monsters attack causes additional damage equal to highest floor multiplied by 2", labelSkin);

        Image poison = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("poison_On"))));
        Image silence = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("silence_On"))));
        Image curse = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("curse_On"))));
        Image burn = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("burn_On"))));

        debuffs.defaults().pad(5, 5, 5, 5).colspan(1).align(Align.left);

        debuffs.add(label).colspan(2);
        debuffs.row();

        debuffs.add(burn);
        debuffs.add(burnDesc);
        debuffs.row();

        debuffs.add(curse);
        debuffs.add(curseDesc);
        debuffs.row();

        debuffs.add(poison);
        debuffs.add(poisonDesc);
        debuffs.row();

        debuffs.add(silence);
        debuffs.add(silenceDesc);
        debuffs.row();

    }

    private void monsterSkillsTable() {
        monsterSkills = new Table();
        AtlasManager manager = AtlasManager.getInstance();
        Label label = new Label("The monsters besides basic statistics like attack or defense, can have additional skills.\n" +
                "This is a list of possible skills", labelSkin);

        Label endureDesc = new Label("Endure - resurrects monster with 1 health after it is killed", labelSkin);
        Label firstStrikeDesc = new Label("First strike - monster attacks first", labelSkin);
        Label bombResistDesc = new Label("Bomb resistance - monster can't be killed with bomb item", labelSkin);
        Label regenerationDesc = new Label("Regeneration - monster heals after each attack by specified amount", labelSkin);
        Label reflectDesc = new Label("Reflect - amount of damage received by player after each attack", labelSkin);
        Label penetrationDesc = new Label("Penetration - player defense is decreased by specified percentage", labelSkin);
        Label manaDrainDesc = new Label("Mana drain - monster attacks reduce mana by specified amount", labelSkin);

        Image endure = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("endureOn"))));
        Image firstStrike = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("firstStrikeOn"))));
        Image bombResist = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("bombResistanceOn"))));
        Image regeneration = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("regenerationOn"))));
        Image reflect = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("reflectOn"))));
        Image penetration = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("penetrationOn"))));
        Image manaDrain = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("manaDrainOn"))));

        monsterSkills.defaults().colspan(1).align(Align.left).pad(5, 5, 5, 5);

        monsterSkills.add(label).colspan(2);
        monsterSkills.row();

        monsterSkills.add(bombResist);
        monsterSkills.add(bombResistDesc);
        monsterSkills.row();

        monsterSkills.add(endure);
        monsterSkills.add(endureDesc);
        monsterSkills.row();

        monsterSkills.add(firstStrike);
        monsterSkills.add(firstStrikeDesc);
        monsterSkills.row();

        monsterSkills.add(manaDrain);
        monsterSkills.add(manaDrainDesc);
        monsterSkills.row();

        monsterSkills.add(penetration);
        monsterSkills.add(penetrationDesc);
        monsterSkills.row();

        monsterSkills.add(reflect);
        monsterSkills.add(reflectDesc);
        monsterSkills.row();

        monsterSkills.add(regeneration);
        monsterSkills.add(regenerationDesc);
        monsterSkills.row();

    }

    private void itemsTable() {
        items = new Table();
        AtlasManager manager = AtlasManager.getInstance();
        Label label = new Label("In the game there are some items you can get. Some of them are mandatory to progress.\n" +
                "But all of them are helpful. They can be divided in three categories.\n" +
                "1 - Items which take effect right after picking them.\n" +
                "2 - Items which are used automatically in right situation.\n" +
                "3 - Items that are used only when you wish so.\n" +
                "The power of potions, keys, swords, armors and shields is stronger in the higher zones.", labelSkin);

        Label keys = new Label("Keys are used to open the doors. You need a key in the same color as a door, to open it.", labelSkin);
        Label potions = new Label("Potions are used to raise your health or mana.\nBlue potions add mana, red adds health and green raise both of them.", labelSkin);
        Label gems = new Label("Gems raise your statistics. \nRed raise attack, blue raise defense, and green adds both statistics. ", labelSkin);
        Label shields = new Label("Shields add to your defense, and add some resistance to debuffs.", labelSkin);
        Label swords = new Label("Swords raise your attack power.", labelSkin);
        Label armors = new Label("Armors raise your defense power.", labelSkin);
        Label bombs = new Label("You can use a bomb to instantly kill adjacent monsters. \nYou don't receive experience or gold for this.", labelSkin);
        Label masterKeys = new Label("You can use a key to open all bronze door on the current floor.", labelSkin);
        Label potionUsable = new Label("You can use a potion to raise your health equal to maximum floor multiplied by 20.", labelSkin);

        Image shieldSilence = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("shieldSilence"))));
        Image shieldBurn = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("shieldFire"))));
        Image shieldUltimate = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("shieldUltimate"))));
        Image shieldPoison = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("shieldPoison"))));
        Image shieldCurse = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("shieldCurse"))));
        Image sword1 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("swordOne"))));
        Image sword2 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("swordTwo"))));
        Image sword3 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("swordThree"))));
        Image sword4 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("swordFour"))));
        Image sword5 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("swordFive"))));
        Image armor1 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("armorOne"))));
        Image armor2 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("armorTwo"))));
        Image armor3 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("armorThree"))));
        Image armor4 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("armorFour"))));
        Image armor5 = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("armorFive"))));
        Image bomb = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("bomb"))));
        Image masterKey = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("masterKey"))));
        Image potion = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("potionItem"))));
        Image gemRed = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("gemRed"))));
        Image gemBlue = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("gemBlue"))));
        Image gemGreen = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("gemGreen"))));
        Image potionRedS = new Image(new SpriteDrawable(new Sprite(manager.getPotions().findRegion("potionRedSmall"))));
        Image potionRedL = new Image(new SpriteDrawable(new Sprite(manager.getPotions().findRegion("potionRedLarge"))));
        Image potionBlueS = new Image(new SpriteDrawable(new Sprite(manager.getPotions().findRegion("potionBlueSmall"))));
        Image potionBlueL = new Image(new SpriteDrawable(new Sprite(manager.getPotions().findRegion("potionBlueLarge"))));
        Image potionGreenS = new Image(new SpriteDrawable(new Sprite(manager.getPotions().findRegion("potionGreenSmall"))));
        Image potionGreenL = new Image(new SpriteDrawable(new Sprite(manager.getPotions().findRegion("potionGreenLarge"))));
        Image keyRed = new Image(new SpriteDrawable(new Sprite(manager.getKeys().findRegion("keyRed"))));
        Image keySilver = new Image(new SpriteDrawable(new Sprite(manager.getKeys().findRegion("keySilver"))));
        Image keyGold = new Image(new SpriteDrawable(new Sprite(manager.getKeys().findRegion("keyGold"))));
        Image keyBronze = new Image(new SpriteDrawable(new Sprite(manager.getKeys().findRegion("keyBronze"))));

        items.defaults().pad(2, 2, 2, 2).colspan(1).align(Align.left);
        items.add(label).colspan(7);
        items.row();

        items.add(potionRedS);
        items.add(potionBlueS);
        items.add(potionGreenS);
        items.add(potionRedL);
        items.add(potionBlueL);
        items.add(potionGreenL);
        items.add(potions);
        items.row();

        items.add(sword1);
        items.add(sword2);
        items.add(sword3);
        items.add(sword4);
        items.add(sword5);
        items.add(swords).colspan(2);
        items.row();

        items.add(armor1);
        items.add(armor2);
        items.add(armor3);
        items.add(armor4);
        items.add(armor5);
        items.add(armors).colspan(2);
        items.row();

        items.add(shieldBurn);
        items.add(shieldCurse);
        items.add(shieldPoison);
        items.add(shieldSilence);
        items.add(shieldUltimate);
        items.add(shields).colspan(2);
        items.row();

        items.add(keyBronze);
        items.add(keySilver);
        items.add(keyGold);
        items.add(keyRed);
        items.add(keys).colspan(3);
        items.row();

        items.add(gemRed);
        items.add(gemBlue);
        items.add(gemGreen);
        items.add(gems).colspan(4);
        items.row();

        items.add(bomb);
        items.add(bombs).colspan(6);
        items.row();

        items.add(masterKey);
        items.add(masterKeys).colspan(6);
        items.row();

        items.add(potion);
        items.add(potionUsable).colspan(6);
        items.row();
    }

    private void playerSkillsTable() {
        playerSkills = new Table();
        AtlasManager manager = AtlasManager.getInstance();
        Label label = new Label("Player can use five skills to make exploration of tower an easier task. You can buy skills\n" +
                "with experience points. The cost of buying skills is increasing with each unlocked ability.\n" +
                "To use a skill, you need to pay with mana points. Duration of skills don't stack.", labelSkin);

        Label protectDesc = new Label("Protect - protects you from negative effects of a trap", labelSkin);
        Label vampireDesc = new Label("Vampire edge - when you kill a monster, you restore some health equal to 1/10 of monster health", labelSkin);
        Label cleanseDesc = new Label("Cleanse - decreses counter of active debuffs by 1", labelSkin);
        Label blessDesc = new Label("Bless - increse your attack and defense by 20%", labelSkin);
        Label instantDesc = new Label("Instant strike - if monster have first strike skill, you are still attacking first", labelSkin);

        Image protect = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("protectOn"))));
        Image vampire = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("vampireOn"))));
        Image cleanse = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("cleanseOn"))));
        Image bless = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("blessOn"))));
        Image instant = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("instantOn"))));

        playerSkills.defaults().pad(5, 5, 5, 5).align(Align.left).colspan(1);

        playerSkills.add(label).colspan(2);
        playerSkills.row();

        playerSkills.add(bless);
        playerSkills.add(blessDesc);
        playerSkills.row();

        playerSkills.add(cleanse);
        playerSkills.add(cleanseDesc);
        playerSkills.row();

        playerSkills.add(instant);
        playerSkills.add(instantDesc);
        playerSkills.row();

        playerSkills.add(protect);
        playerSkills.add(protectDesc);
        playerSkills.row();

        playerSkills.add(vampire);
        playerSkills.add(vampireDesc);
        playerSkills.row();
    }

    private void interfaceTable() {
        interfaceTbl = new Table();
        interfaceTbl.defaults().pad(5, 5, 5, 5).colspan(1).align(Align.left);
        AtlasManager manager = AtlasManager.getInstance();

        Image saveGame = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("save"))));
        Image loadGame = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("load"))));
        Image help = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("help"))));
        Image options = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("options"))));
        Image prediction = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("prediction"))));
        Image fly = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("fly"))));
        Image training = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("buySkill"))));
        Image useSkills = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("skills"))));

        Label saveGameDesc = new Label("Save game - shortcut: Y\n" +
                "You can use this option to save your game in one of the 10 avaible slots.", labelSkin);
        Label loadGameDesc = new Label("Load game - shortcut: U\n" +
                "Click here if you want to load your game.", labelSkin);
        Label helpDesc = new Label("Help - shortcut: I\n" +
                "You already know what this button is doing.", labelSkin);
        Label optionsDesc = new Label("Options - shortcut: O\n" +
                "You can leave the game from here or change volume of sounds and music.", labelSkin);
        Label predictionDesc = new Label("Prediction - shortcut: H\n" +
                "This is an option used to determine how much damage your character will receive after a fight\n" +
                "with a monster. You can check monsters statistics, skills and how much gold and experience\n" +
                "you will receive after defeating it. Use it often.", labelSkin);
        Label flyDesc = new Label("Fly - shortcut: J\n" +
                "You can use a fly option to quickly travel to any floor you have already been to.\n" +
                "You can use it whenever you like, and it's free.", labelSkin);
        Label trainingDesc = new Label("Buy skills and stats - shortcut: K\n" +
                "This option allows you to spend experience points to unlock skills or raise your statistics.\n" +
                "Each purchase will raise the cost of the next. You can unlock skills in any order.\n" +
                "Also, when you raise your statistics, each next purchase will give you larger bonus.", labelSkin);
        Label useSkillsDesc = new Label("Use skills and items - shortcut: L\n" +
                "Here you can use your skills and items, if you have any.", labelSkin);

        interfaceTbl.add(saveGame);
        interfaceTbl.add(saveGameDesc);
        interfaceTbl.row();

        interfaceTbl.add(loadGame);
        interfaceTbl.add(loadGameDesc);
        interfaceTbl.row();

        interfaceTbl.add(help);
        interfaceTbl.add(helpDesc);
        interfaceTbl.row();

        interfaceTbl.add(options);
        interfaceTbl.add(optionsDesc);
        interfaceTbl.row();

        interfaceTbl.add(prediction);
        interfaceTbl.add(predictionDesc);
        interfaceTbl.row();

        interfaceTbl.add(fly);
        interfaceTbl.add(flyDesc);
        interfaceTbl.row();

        interfaceTbl.add(training);
        interfaceTbl.add(trainingDesc);
        interfaceTbl.row();

        interfaceTbl.add(useSkills);
        interfaceTbl.add(useSkillsDesc);
        interfaceTbl.row();
    }

    private void gamePlayTable() {
        gamePlay = new Table();
        AtlasManager manager = AtlasManager.getInstance();
        String string = "The basic controls in the game are very simple. You can move with the directional buttons\n" +
                "placed on the right side of the screen, with arrows on keybord or AWSD keys.\n" +
                "In the game there are a few basic types of objects you can interact with.\n";
        Label label = new Label(string, labelSkin);
        gamePlay.defaults().pad(5, 5, 5, 5).colspan(1).align(Align.left);

        Image monster = new Image(new SpriteDrawable(new Sprite(manager.getMonsters().findRegion("skeleton"))));
        Image shop = new Image(new SpriteDrawable(new Sprite(manager.getObstacles().findRegion("merchant"))));
        Image elder = new Image(new SpriteDrawable(new Sprite(manager.getObstacles().findRegion("elder"))));
        Image itemsGem = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("gemRed"))));
        Image itemsSword = new Image(new SpriteDrawable(new Sprite(manager.getItems().findRegion("swordOne"))));
        Image itemsKey = new Image(new SpriteDrawable(new Sprite(manager.getKeys().findRegion("keyBronze"))));
        Image itemsPotions = new Image(new SpriteDrawable(new Sprite(manager.getPotions().findRegion("potionRedSmall"))));
        Image door = new Image(new SpriteDrawable(new Sprite(manager.getDoors().findRegion("doorBronze", 1))));
        Image doorIron = new Image(new SpriteDrawable(new Sprite(manager.getDoors().findRegion("doorIron", 1))));
        Image stairsUp = new Image(new SpriteDrawable(new Sprite(manager.getObstacles().findRegion("stairs_up"))));
        Image stairsDown = new Image(new SpriteDrawable(new Sprite(manager.getObstacles().findRegion("stairs_down"))));
        Image trapOne = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("poison"))));
        Image trapTwo = new Image(new SpriteDrawable(new Sprite(manager.getTraps().findRegion("spear"))));

        Label monsterDesc = new Label("Monsters are main obstacles in the game. The battle with them is automatic,\n" +
                "and there are no random elements. The fight is over when yours or monsters health is reduced to 0\n" +
                "The damage is calculated by subtracting defense of the defender, from attack of the attacker.\n" +
                "The result is subtracted from defenders health. Player attacks first, unless the monster possess\n" +
                "first strike ability. Monsters can have a various skills, and they can alter the basic rules of the fight.\n" +
                "If your attack power is smaller than monsters defense - you won't be able to attack it.", labelSkin);
        Label shopDesc = new Label("In shops you can buy with gold various bonuses or items. You can buy something from\n" +
                "vendor only once. After the purchase, the shop will disappear.", labelSkin);
        Label elderDesc = new Label("Elders will provide you with various tips, hints, or some pointless babbling.", labelSkin);
        Label itemsDesc = new Label("The items are main bonuses for your character.", labelSkin);
        Label doorDesc = new Label("To open the door, you need to possess a key with matching colour. There are four colours.\n" +
                "Bronze, silver, gold and red. Bronze keys are the most common, and red are the rarest", labelSkin);
        Label doorIronDesc = new Label("This are the special door. You can't open them with a key. Instead, you need to kill one or two\n" +
                "specific monsters. They most often are next to door, but sometimes they can be elsewhere.\n" +
                "But guards always will be on the floor with door.", labelSkin);
        Label stairsDesc = new Label("They are used to travers to different floors", labelSkin);
        Label trapDesc = new Label("Traps are triggered when you step on them, and they are always active.", labelSkin);

        gamePlay.add(label).colspan(5);
        gamePlay.row();

        gamePlay.add(monster);
        gamePlay.add(monsterDesc).colspan(4);
        gamePlay.row();

        gamePlay.add(shop);
        gamePlay.add(shopDesc).colspan(4);
        gamePlay.row();

        gamePlay.add(elder);
        gamePlay.add(elderDesc).colspan(4);
        gamePlay.row();

        gamePlay.add(itemsGem);
        gamePlay.add(itemsSword);
        gamePlay.add(itemsKey);
        gamePlay.add(itemsPotions);
        gamePlay.add(itemsDesc).expandX();
        gamePlay.row();

        gamePlay.add(door);
        gamePlay.add(doorDesc).colspan(4);
        gamePlay.row();

        gamePlay.add(doorIron);
        gamePlay.add(doorIronDesc).colspan(4);
        gamePlay.row();

        gamePlay.add(stairsDown);
        gamePlay.add(stairsUp);
        gamePlay.add(stairsDesc).colspan(3);
        gamePlay.row();

        gamePlay.add(trapOne);
        gamePlay.add(trapTwo);
        gamePlay.add(trapDesc).colspan(3);
        gamePlay.row();

    }

    private void statsTable() {
        stats = new Table();
        AtlasManager manager = AtlasManager.getInstance();
        Label label = new Label("There are six basics statistics in the game. They are:\n", labelSkin);

        Image attack = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("attackSmall"))));
        Image defense = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("defenseSmall"))));
        Image health = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("healthSmall2"))));
        Image mana = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("manaSmall2"))));
        Image experience = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("experienceSmall"))));
        Image gold = new Image(new SpriteDrawable(new Sprite(manager.getUi().findRegion("goldSmall"))));

        Label attackDescription = new Label("Attack - the amount of damage you will deal to monster in one attack", labelSkin);
        Label defenseDescription = new Label("Defense - how much of the damage from monsters attack you will block", labelSkin);
        Label healthDescription = new Label("Health - how many damage you can take. When health decrese to zero or less it is game over", labelSkin);
        Label manaDescription = new Label("Mana - you can spend this points to use skills. They can fall below zero points", labelSkin);
        Label experienceDescription = new Label("Experience - you can spend this points to buy skills and raise stats. Earned by killing monsters", labelSkin);
        Label goldDescription = new Label("Gold - you can spend it in shops to buy various things. Earned by killing monsters", labelSkin);

        stats.defaults().pad(5, 5, 5, 5).colspan(1).align(Align.left);
        stats.add(label).colspan(2);
        stats.row();

        stats.add(attack);
        stats.add(attackDescription);
        stats.row();

        stats.add(defense);
        stats.add(defenseDescription);
        stats.row();

        stats.add(health);
        stats.add(healthDescription);
        stats.row();

        stats.add(mana);
        stats.add(manaDescription);
        stats.row();

        stats.add(experience);
        stats.add(experienceDescription);
        stats.row();

        stats.add(gold);
        stats.add(goldDescription);

    }

    private void basicsTable() {
        basics = new Table();
        String string =
                "Fantasy Tower is the game inspired by old PC game \"Tower of the sorcerer\". \n" +
                        "The goal of the game is to ascend to the 50th floor, and defeat final monster.\n" +
                        "Fantasy tower is a puzzle game. There are no random elements in the game.\n" +
                        "Floors and monsters will always be the same, and there is no \"rolling dice\" involved. " +
                        "\n\nYour success depends on taking the best course of action, in the various situations.\n" +
                        "You need to manage your health, gold, items, magic points and keys to progress to the higher floors.\n" +
                        "Also, you can get stuck, so it is imperative to save often, before taking major decisions.\n" +
                        "It is possible to save the game whenever you please, so don't hesitate to do so.";

        Label textLabel = new Label(string, labelSkin);
        basics.add(textLabel);
    }

    private void changeTable(Table table) {
        this.table.clearChildren();
        this.table.add(buttonsTable).align(Align.left).colspan(1);
        this.table.add(table).padLeft(5).expandX().colspan(1);
        this.table.row();
        this.table.add(close).padTop(10).colspan(2).align(Align.center);
    }

    private ClickListener basics() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(basics);
            }
        };
    }

    private ClickListener stats() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(stats);
            }
        };
    }

    private ClickListener gamePlay() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(gamePlay);
            }
        };
    }

    private ClickListener playerSkills() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(playerSkills);
            }
        };
    }

    private ClickListener items() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(items);
            }
        };
    }

    private ClickListener monsterSkills() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(monsterSkills);
            }
        };
    }

    private ClickListener traps() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(traps);
            }
        };
    }

    private ClickListener debuffs() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(debuffs);
            }
        };
    }

    private ClickListener interfaceListener() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                changeTable(interfaceTbl);
            }
        };
    }

    private ClickListener close() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                main.getStateMachine().changeState(main.getStateMachine().getPreviousState());
            }
        };
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
