package userInterface.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Game;

import globalVariables.Const;
import globalVariables.Message;
import globalVariables.PlayerData;
import graphic.AtlasManager;
import music.BackgroundMusic;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.Ui;
import utils.DataManager;

public class Options extends Ui {

    private int musicVolumeValue;
    private int soundVolumeValue;
    private Label soundVolume;
    private Label musicVolume;
    private Label sound;
    private Label music;

    private Table table;

    private TextButton musicMinus;
    private TextButton soundMinus;
    private TextButton musicPlus;
    private TextButton soundPlus;
    private TextButton close;
    private TextButton returnToMenu;

    public Options(Game main) {
        super(main);
        this.main = main;

        super.labelSkin(10, 10);
        getOptionsPref();
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();

        table = new Table();
        table.setFillParent(true);
        stage.addActor(table);
        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));

        sound = new Label("Sounds", labelSkin);
        music = new Label("Music", labelSkin);
        soundVolume = new Label(String.valueOf(soundVolumeValue), labelSkin);
        musicVolume = new Label(String.valueOf(musicVolumeValue), labelSkin);

        createButtons();
        createScene();

        Gdx.input.setInputProcessor(stage);
    }

    private void createScene() {
        table.add(soundMinus).colspan(1).padTop(5);
        table.add(sound).padLeft(5).colspan(1).padTop(5);
        table.add(soundVolume).width(20).padLeft(5).colspan(1).padTop(5);
        table.add(soundPlus).padLeft(5).colspan(1).padTop(5);
        table.row();

        table.add(musicMinus).colspan(1).padTop(5);
        table.add(music).padLeft(5).padTop(5).colspan(1);
        table.add(musicVolume).width(20).padLeft(5).padTop(5).colspan(1);
        table.add(musicPlus).padLeft(5).colspan(1).padTop(5);
        table.row();

        table.add(close).padTop(10).colspan(2);
        table.add(returnToMenu).padTop(10).padLeft(5).colspan(2);

    }

    public void updateInfoInViews() {
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();

        table.setBackground(manager.getScreenBackgrounds().get(zone - 1));
        table.clearChildren();
        createButtons();
        createScene();
    }

    private TextButton makeButton(String message, Skin skin, ClickListener listener) {
        TextButton button = new TextButton(message, skin);
        button.addListener(listener);
        return button;
    }

    private void createButtons() {
        int zone = Const.zone(data.getCurrentFloor());
        AtlasManager manager = AtlasManager.getInstance();
        Skin skin = manager.getSmallButtonsSkin().get(zone - 1);

        soundPlus = makeButton("+", skin, plusListener(false));
        soundMinus = makeButton("-", skin, minusListener(false));
        musicPlus = makeButton("+", skin, plusListener(true));
        musicMinus = makeButton("-", skin, minusListener(true));
        returnToMenu = makeButton("Exit to main", manager.getMediumButtonsSkin().get(zone - 1), mainMenuListener());
        close = makeButton(Message.DIALOG_CLOSE, manager.getMediumButtonsSkin().get(zone - 1), closeListener());
    }

    private ClickListener plusListener(final boolean music) {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                if (music) {
                    if (musicVolumeValue < 10) {
                        musicVolumeValue++;
                        musicVolume.setText(String.valueOf(musicVolumeValue));
                        BackgroundMusic player = BackgroundMusic.getInstance();
                        player.getSongs().get(player.getCurrentlyPlayed()).setVolume(musicVolumeValue / 10f);
                    }
                } else {
                    if (soundVolumeValue < 10) {
                        soundVolumeValue++;
                        soundVolume.setText(String.valueOf(soundVolumeValue));
                        SoundEffect.getInstance().setVolume(soundVolumeValue / 10f);
                    }
                }
            }
        };
    }

    private ClickListener minusListener(final boolean music) {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                if (music) {
                    if (musicVolumeValue > 0) {
                        musicVolumeValue--;
                        musicVolume.setText(String.valueOf(musicVolumeValue));
                        BackgroundMusic player = BackgroundMusic.getInstance();
                        player.getSongs().get(player.getCurrentlyPlayed()).setVolume(musicVolumeValue / 10f);
                    }
                } else {
                    if (soundVolumeValue > 0) {
                        soundVolumeValue--;
                        soundVolume.setText(String.valueOf(soundVolumeValue));
                        SoundEffect.getInstance().setVolume(soundVolumeValue / 10f);
                    }
                }
            }
        };
    }

    private void getOptionsPref() {
        if (prefExist("options", "sound")) {
            soundVolumeValue = DataManager.loadInteger(DataManager.loadPreferences("options", "sound"));
            musicVolumeValue = DataManager.loadInteger(DataManager.loadPreferences("options", "music"));
            if (soundVolumeValue > 10 || soundVolumeValue < 0) {
                soundVolumeValue = 10;
            }
            if (musicVolumeValue > 10 || musicVolumeValue < 0) {
                soundVolumeValue = 10;
            }

        } else {
            soundVolumeValue = 10;
            musicVolumeValue = 10;
            DataManager.savePreferences("options", "sound", DataManager.save(soundVolumeValue));
            DataManager.savePreferences("options", "music", DataManager.save(musicVolumeValue));
        }
    }

    private ClickListener mainMenuListener() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                returnToMainMenu();
            }
        };
    }

    private void returnToMainMenu() {
        Gdx.input.setInputProcessor(stageForDialog);
        int zone = Const.zone(PlayerData.getInstance().getCurrentFloor());
        Dialog dialog = new Dialog("", AtlasManager.getInstance().getDialogSkinsSmall().get(zone - 1)) {
            @Override
            protected void result(Object object) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                if (((Boolean) object)) {
                    Timer timer = new Timer();
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            if (main.getStateMachine().getPreviousState() != GameState.MAIN_MENU) {
                                main.getInPlay().getDraw().getPlayer().resetPlayer();
                                data.resetData();
                            }
                            DataManager.savePreferences("options", "sound", DataManager.save(soundVolumeValue));
                            DataManager.savePreferences("options", "music", DataManager.save(musicVolumeValue));
                            main.getStateMachine().changeState(GameState.MAIN_MENU);
                        }
                    }, 0.5f);
                } else {
                    Gdx.input.setInputProcessor(stage);
                }
            }
        };
        dialog.text("Are you sure?");
        dialog.key(Input.Keys.SPACE, false);
        dialog.key(Input.Keys.ENTER, false);
        dialog.button(Message.DIALOG_CONFIRM, true);
        dialog.button(Message.DIALOG_CANCEL, false);
        dialog.show(stageForDialog);
    }

    private boolean prefExist(String prefname, String keyInPrefName) {
        Preferences pref = Gdx.app.getPreferences(prefname);
        String x = pref.getString(keyInPrefName);
        return !x.isEmpty();
    }

    private ClickListener closeListener() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                DataManager.savePreferences("options", "sound", DataManager.save(soundVolumeValue));
                DataManager.savePreferences("options", "music", DataManager.save(musicVolumeValue));
                main.getStateMachine().changeState(main.getStateMachine().getPreviousState());
            }
        };
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

}
