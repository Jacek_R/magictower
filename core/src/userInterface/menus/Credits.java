package userInterface.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.Game;

import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import music.SoundEffect;
import stateMachine.GameState;
import userInterface.Ui;

public class Credits extends Ui {

    public Credits(Game main) {
        super(main);
        this.main = main;

        super.labelSkin(10, 10);

        Table table = new Table();
        TextButton close = createCloseButton();

        Label musicSource = new Label("Background music by: \nwww.audionautix.com", labelSkin);
        Label link1 = new Label("Vinrax - http://opengameart.org/content/key-pickup", labelSkin);
        Label link2 = new Label("ViRix - http://opengameart.org/content/ui-sound-effects-pack", labelSkin);
        Label link3 = new Label("bart - http://opengameart.org/content/68-workshop-sounds", labelSkin);
        Label link4 = new Label("Kenney - http://opengameart.org/content/50-rpg-sound-effects", labelSkin);
        Label link5 = new Label("Little Robot Sound Factory - http://opengameart.org/content/horror-sound-effects-library", labelSkin);

        Label sounds = new Label("Sound Effects used in the game are created by:  ", labelSkin);

        Label graphics = new Label("Graphic tiles and icons are from: \nhttp://rltiles.sourceforge.net/ and http://game-icons.net/ ", labelSkin);
        Label animations = new Label("Player animations by wulax: \nhttp://opengameart.org/content/lpc-medieval-fantasy-character-sprites", labelSkin);
        Label blood = new Label("Blood animation by MoonStar: \nhttp://opengameart.org/content/bloodsplatter-and-bloodsplash-animation", labelSkin);

        table.add(musicSource).align(Align.left);
        table.row();
        table.add(graphics).align(Align.left).padTop(10);
        table.row();
        table.add(animations).align(Align.left).padTop(10);
        table.row();
        table.add(blood).align(Align.left).padTop(10);
        table.row();
        table.add(sounds).align(Align.left).padTop(10);
        table.row();
        table.add(link1).align(Align.left).padTop(10);
        table.row();
        table.add(link2).align(Align.left);
        table.row();
        table.add(link3).align(Align.left);
        table.row();
        table.add(link4).align(Align.left);
        table.row();
        table.add(link5).align(Align.left);
        table.row();
        table.add(close).padTop(10);

        stage.addActor(table);
        table.setFillParent(true);
        int zone = Const.zone(data.getCurrentFloor());
        table.setBackground(AtlasManager.getInstance().getScreenBackgrounds().get(zone - 1));

        Gdx.input.setInputProcessor(stage);
    }

    private TextButton createCloseButton() {
        int zone = Const.zone(data.getCurrentFloor());
        TextButton button = new TextButton(Message.DIALOG_CANCEL, AtlasManager.getInstance().getMediumButtonsSkin().get(zone - 1));
        button.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                        main.getStateMachine().changeState(GameState.MAIN_MENU);
                    }
                }
        );
        return button;
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
