package gameObjects.pickUps;

import gameObjects.Content;
import globalVariables.Const;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Gem extends Content {

    private Type type;

    public Gem() {
    }

    public Gem(Type type) {
        this.type = type;
        selectImage(type);
    }

    public enum Type {
        RED, BLUE, GREEN
    }

    private void selectImage(Type type) {
        atlasType = AtlasType.ITEMS;
        switch (type) {
            case RED:
                image = "gemRed";
                break;
            case BLUE:
                image = "gemBlue";
                break;
            case GREEN:
                image = "gemGreen";
                break;
        }
    }

    @Override
    public boolean interact(InPlay inPlay) {
        int gem_base_bonus = 1;

        switch (type) {
            case RED:
                inPlay.getAttack().setText(String.valueOf(addStat(Const.ATTACK, gem_base_bonus)));
                break;
            case BLUE:
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, gem_base_bonus)));
                break;
            case GREEN:
                inPlay.getAttack().setText(String.valueOf(addStat(Const.ATTACK, gem_base_bonus)));
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, gem_base_bonus)));
                break;
        }
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.GEM);
        return true;
    }

    public Type getType() {
        return type;
    }
}
