package gameObjects.pickUps;

import gameObjects.Content;
import globalVariables.Const;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Potion extends Content {

    private Type type;

    public Potion() {
    }

    public Potion(Type type) {
        this.type = type;
        selectImage(type);
    }

    public enum Type {
        RED_S, RED_L, BLUE_S, BLUE_L, GREEN_S, GREEN_L
    }

    private void selectImage(Type type) {
        atlasType = AtlasType.POTIONS;
        switch (type) {
            case RED_S:
                image = "potionRedSmall";
                break;
            case RED_L:
                image = "potionRedLarge";
                break;
            case BLUE_S:
                image = "potionBlueSmall";
                break;
            case BLUE_L:
                image = "potionBlueLarge";
                break;
            case GREEN_S:
                image = "potionGreenSmall";
                break;
            case GREEN_L:
                image = "potionGreenLarge";
                break;
        }
    }

    @Override
    public boolean interact(InPlay inPlay) {
        int base_bonus_sRed = 50;
        int base_bonus_sBlue = 10;
        int base_bonus_sGreenHealth = 30;
        int base_bonus_sGreenMana = 5;

        int base_bonus_lRed = 200;
        int base_bonus_lBlue = 50;
        int base_bonus_lGreenHealth = 120;
        int base_bonus_lGreenMana = 30;

        float mod = 1f;
        if (data.getBuffs().get(Const.POISONED) > 0) {
            mod = 0.5f;
        }
        switch (type) {
            case RED_S:
                inPlay.getHealth().setText(String.valueOf(addStat(Const.HEALTH, Math.round(base_bonus_sRed * mod))));
                break;
            case RED_L:
                inPlay.getHealth().setText(String.valueOf(addStat(Const.HEALTH, Math.round(base_bonus_lRed * mod))));
                break;
            case BLUE_S:
                inPlay.getMana().setText(String.valueOf(addStat(Const.MANA, Math.round(base_bonus_sBlue * mod))));
                break;
            case BLUE_L:
                inPlay.getMana().setText(String.valueOf(addStat(Const.MANA, Math.round(base_bonus_lBlue * mod))));
                break;
            case GREEN_S:
                inPlay.getHealth().setText(String.valueOf(addStat(Const.HEALTH, Math.round(base_bonus_sGreenHealth * mod))));
                inPlay.getMana().setText(String.valueOf(addStat(Const.MANA, Math.round(base_bonus_sGreenMana * mod))));
                break;
            case GREEN_L:
                inPlay.getHealth().setText(String.valueOf(addStat(Const.HEALTH, Math.round(base_bonus_lGreenHealth * mod))));
                inPlay.getMana().setText(String.valueOf(addStat(Const.MANA, Math.round(base_bonus_lGreenMana * mod))));
                break;
        }
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.POTION);
        return true;
    }

    public Type getType() {
        return type;
    }
}
