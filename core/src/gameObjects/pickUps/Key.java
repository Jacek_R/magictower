package gameObjects.pickUps;

import gameObjects.Content;
import globalVariables.Const;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Key extends Content {

    private Type type;

    public Key() {
    }

    public Key(Type type) {
        this.type = type;
        selectImage(type);
    }

    public enum Type {
        BRONZE, SILVER, GOLD, RED
    }

    private void selectImage(Type type) {
        atlasType = AtlasType.KEYS;
        switch (type) {
            case BRONZE:
                image = "keyBronze";
                break;
            case SILVER:
                image = "keySilver";
                break;
            case GOLD:
                image = "keyGold";
                break;
            case RED:
                image = "keyRed";
                break;
        }
    }

    @Override
    public boolean interact(InPlay inPlay) {
        switch (type) {
            case BRONZE:
                inPlay.getKeys_bronze().setText(String.valueOf(addKey(Const.KEY_BRONZE)));
                break;
            case SILVER:
                inPlay.getKeys_silver().setText(String.valueOf(addKey(Const.KEY_SILVER)));
                break;
            case GOLD:
                inPlay.getKeys_gold().setText(String.valueOf(addKey(Const.KEY_GOLD)));
                break;
            case RED:
                inPlay.getKeys_red().setText(String.valueOf(addKey(Const.KEY_RED)));
                break;
        }
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.KEY);
        return true;
    }

    private int addKey(int color) {
        int amount;
        amount = data.getKeys().get(color);
        data.getKeys().set(color, amount + 1);
        return amount + 1;
    }

    public Type getType() {
        return type;
    }
}
