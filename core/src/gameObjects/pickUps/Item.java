package gameObjects.pickUps;

import gameObjects.Content;
import globalVariables.Const;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Item extends Content {

    private Type type;

    public Item() {
    }

    public Item(Type type) {
        this.type = type;
        selectImage(type);
    }

    public enum Type {
        SWORD_1, SWORD_2, SWORD_3, SWORD_4, SWORD_5, SHIELD_SILENCE, SHIELD_FIRE, SHIELD_POISON, SHIELD_CURSE, SHIELD_FINAL,
        ARMOR_1, ARMOR_2, ARMOR_3, ARMOR_4, ARMOR_5, BOMB, MASTER_KEY, POTION, SCEPTER, COINS, HELMET, ORB, BOOK, RING, APPLE, STAFF
    }

    private void selectImage(Type type) {
        atlasType = AtlasType.ITEMS;
        switch (type) {
            case SWORD_1:
                image = "swordOne";
                break;
            case SWORD_2:
                image = "swordTwo";
                break;
            case SWORD_3:
                image = "swordThree";
                break;
            case SWORD_4:
                image = "swordFour";
                break;
            case SWORD_5:
                image = "swordFive";
                break;
            case SHIELD_SILENCE:
                image = "shieldSilence";
                break;
            case SHIELD_FIRE:
                image = "shieldFire";
                break;
            case SHIELD_POISON:
                image = "shieldPoison";
                break;
            case SHIELD_CURSE:
                image = "shieldCurse";
                break;
            case SHIELD_FINAL:
                image = "shieldUltimate";
                break;
            case ARMOR_1:
                image = "armorOne";
                break;
            case ARMOR_2:
                image = "armorTwo";
                break;
            case ARMOR_3:
                image = "armorThree";
                break;
            case ARMOR_4:
                image = "armorFour";
                break;
            case ARMOR_5:
                image = "armorFive";
                break;
            case BOMB:
                image = "bomb";
                break;
            case MASTER_KEY:
                image = "masterKey";
                break;
            case POTION:
                image = "potionItem";
                break;
            case SCEPTER:
                image = "scepter";
                break;
            case COINS:
                image = "goldPile";
                break;
            case HELMET:
                image = "helmet";
                break;
            case ORB:
                image = "crystal";
                break;
            case BOOK:
                image = "book";
                break;
            case RING:
                image = "ring";
                break;
            case APPLE:
                image = "apple";
                break;
            case STAFF:
                image = "staff";
                break;
        }
    }

    @Override
    public boolean interact(InPlay inPlay) {
        int sword_bonus = 10;
        int shield_bonus = 3;
        int armor_bonus = 10;

        switch (type) {
            case SWORD_1:
            case SWORD_2:
            case SWORD_3:
            case SWORD_4:
            case SWORD_5:
                inPlay.getAttack().setText(String.valueOf(addStat(Const.ATTACK, sword_bonus)));
                break;
            case SHIELD_SILENCE:
                data.getShields().set(Const.SHIELD_SILENCE, true);
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, shield_bonus)));
                break;
            case SHIELD_FIRE:
                data.getShields().set(Const.SHIELD_BURN, true);
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, shield_bonus)));
                break;
            case SHIELD_POISON:
                data.getShields().set(Const.SHIELD_POISON, true);
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, shield_bonus)));
                break;
            case SHIELD_CURSE:
                data.getShields().set(Const.SHIELD_CURSE, true);
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, shield_bonus)));
                break;
            case SHIELD_FINAL:
                data.getShields().set(Const.SHIELD_FINAL, true);
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, shield_bonus)));
                break;
            case ARMOR_1:
            case ARMOR_2:
            case ARMOR_3:
            case ARMOR_4:
            case ARMOR_5:
                inPlay.getDefense().setText(String.valueOf(addStat(Const.DEFENSE, armor_bonus)));
                break;
            case BOMB:
                addItem(Const.BOMB);
                break;
            case MASTER_KEY:
                addItem(Const.MASTER_KEY);
                break;
            case POTION:
                addItem(Const.POTION);
                break;
            default:
                break;
        }
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.ITEM);
        return true;
    }

    private void addItem(int item) {
        int amount = data.getItems().get(item);
        data.getItems().set(item, amount + 1);
    }

    public Type getType() {
        return type;
    }
}
