package gameObjects.terrain;

import gameObjects.Content;
import globalVariables.Const;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Door extends Content {

    private Type type;
    private int[] guard_1;
    private int[] guard_2;

    public enum Type {
        BRONZE, SILVER, GOLD, RED, IRON
    }

    public Door() {
    }

    public Door(Type type, int[] guard_1, int[] guard_2) {
        this.type = type;
        this.guard_1 = guard_1;
        this.guard_2 = guard_2;
        selectImage(type);
    }

    public Door(Type type) {
        this.type = type;
        selectImage(type);
    }

    private void selectImage(Type type) {
        indexInAtlas = Const.zone(data.getCurrentFloor()); //zone
        atlasType = AtlasType.DOORS;
        switch (type) {
            case BRONZE:
                image = "doorBronze";
                break;
            case SILVER:
                image = "doorSilver";
                break;
            case GOLD:
                image = "doorGold";
                break;
            case RED:
                image = "doorRed";
                break;
            case IRON:
                image = "doorIron";
                break;
        }
    }

    @Override
    public boolean interact(InPlay inPlay) {
        switch (type) {
            case BRONZE:
                inPlay.getKeys_bronze().setText(String.valueOf(removeKey(Const.KEY_BRONZE)));
                break;
            case SILVER:
                inPlay.getKeys_silver().setText(String.valueOf(removeKey(Const.KEY_SILVER)));
                break;
            case GOLD:
                inPlay.getKeys_gold().setText(String.valueOf(removeKey(Const.KEY_GOLD)));
                break;
            case RED:
                inPlay.getKeys_red().setText(String.valueOf(removeKey(Const.KEY_RED)));
                break;
        }
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.OPEN_DOOR);
        return true;
    }

    private int removeKey(int color) {
        int amount;
        amount = data.getKeys().get(color);
        data.getKeys().set(color, amount - 1);
        return amount - 1;
    }

    public Type getType() {
        return type;
    }

    public int[] getGuard_1() {
        return guard_1;
    }

    public int[] getGuard_2() {
        return guard_2;
    }
}
