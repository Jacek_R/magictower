package gameObjects.terrain;

import gameObjects.Content;
import globalVariables.Const;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Trap extends Content {

    private Type type;

    public Trap() {
    }

    public Trap(Type type) {
        this.type = type;
        selectImage(type);
    }

    public enum Type {
        DOWN, LEFT, RIGHT, UP, SPEAR, POISON, MAGIC_FLAME, CURSE, SILENCE, FLAME, PIT
    }

    private void selectImage(Type type) {
        atlasType = AtlasType.TRAPS;
        switch (type) {
            case DOWN:
                image = "down";
                break;
            case LEFT:
                image = "left";
                break;
            case RIGHT:
                image = "right";
                break;
            case UP:
                image = "up";
                break;
            case SPEAR:
                image = "spear";
                break;
            case POISON:
                image = "poison";
                break;
            case MAGIC_FLAME:
                image = "magicFlame";
                break;
            case SILENCE:
                image = "silence";
                break;
            case CURSE:
                image = "curse";
                break;
            case FLAME:
                image = "flame";
                break;
            case PIT:
                image = "pit";
                break;
        }
    }

    @Override
    public boolean interact(InPlay inPlay) {
        int snareDamage = -10;
        int magicDamage = -5;
        int debuffDuration = 3;
        boolean shield;
        boolean finalShield = data.getShields().get(Const.SHIELD_FINAL);

        switch (type) {
            case SPEAR:
                if (!checkIfProtected(inPlay)) {
                    inPlay.getHealth().setText(String.valueOf(addStat(Const.HEALTH, snareDamage)));
                }
                break;
            case MAGIC_FLAME:
                if (!checkIfProtected(inPlay)) {
                    inPlay.getMana().setText(String.valueOf(addStat(Const.MANA, magicDamage)));
                }
                break;
            case POISON:
                shield = data.getShields().get(Const.SHIELD_POISON);
                inPlay.getPoison_dur().setText(String.valueOf(activateTrap(Const.POISONED, Const.REGION_POISONED, debuffDuration, shield, finalShield, inPlay)));
                break;
            case SILENCE:
                shield = data.getShields().get(Const.SHIELD_SILENCE);
                inPlay.getSilence_dur().setText(String.valueOf(activateTrap(Const.SILENCED, Const.REGION_SILENCED, debuffDuration, shield, finalShield, inPlay)));
                break;
            case CURSE:
                shield = data.getShields().get(Const.SHIELD_CURSE);
                inPlay.getCurse_dur().setText(String.valueOf(activateTrap(Const.CURSED, Const.REGION_CURSED, debuffDuration, shield, finalShield, inPlay)));
                break;
            case FLAME:
                shield = data.getShields().get(Const.SHIELD_BURN);
                inPlay.getBurn_dur().setText(String.valueOf(activateTrap(Const.BURNED, Const.REGION_BURNED, debuffDuration, shield, finalShield, inPlay)));
                break;
            case PIT:
                break;
        }
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.TRAP);
        return false;
    }

    private int activateTrap(int debuff, String debuffRegion, int debuffDuration, boolean shield, boolean finalShield, InPlay inPlay) {
        if (checkIfProtected(inPlay)) {
            return data.getBuffs().get(debuff);
        } else {
            if (shield) {
                debuffDuration -= 1;
            }
            if (finalShield) {
                debuffDuration -= 1;
            }
            int r = inflictDebuff(debuff, debuffDuration);
            inPlay.updateImage(debuff, debuffRegion);
            return r;
        }
    }

    private boolean checkIfProtected(InPlay inPlay) {
        boolean isProtected = false;
        if (data.getBuffs().get(Const.PROTECT) > 0) {
            isProtected = true;
            int newProtectDuration = data.getBuffs().get(Const.PROTECT) - 1;
            data.getBuffs().set(Const.PROTECT, newProtectDuration);
            inPlay.updateImage(Const.PROTECT, Const.REGION_PROTECT);
            inPlay.getProtect_dur().setText(String.valueOf(newProtectDuration));
        }
        return isProtected;
    }

    public Type getType() {
        return type;
    }
}
