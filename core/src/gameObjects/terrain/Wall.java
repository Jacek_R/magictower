package gameObjects.terrain;


import gameObjects.Content;
import globalVariables.Const;

public class Wall extends Content {

    public Wall() {
        atlasType = AtlasType.WALLS;
        indexInAtlas = Const.zone(data.getCurrentFloor()); //zone
        image = "wall";
    }
}
