package gameObjects.terrain;

import gameObjects.Content;
import globalVariables.Const;
import globalVariables.Message;
import music.BackgroundMusic;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Stairs extends Content {

    private Type type;

    public Stairs() {
    }

    public Stairs(Type type) {
        this.type = type;
        selectImage(type);
    }

    public enum Type {
        UP, DOWN
    }

    private void selectImage(Type type) {
        atlasType = AtlasType.OBSTACLES;
        switch (type) {
            case UP:
                image = "stairs_up";
                break;
            case DOWN:
                image = "stairs_down";
                break;
        }
    }

    @Override
    public boolean interact(InPlay inPlay) {
        switch (type) {
            case UP:
                changeFloor(data.getCurrentFloor() + 1, inPlay, Const.zone(data.getCurrentFloor()));
                break;
            case DOWN:
                changeFloor(data.getCurrentFloor() - 1, inPlay, Const.zone(data.getCurrentFloor()));
                break;
        }
        return false;
    }

    private void changeFloor(int levelNumber, InPlay inPlay, int zone) {
        if (data.getLevels().size() < levelNumber) {
            data.getLevels().set(data.getCurrentFloor() - 1, data.getCurrentMap());
            data.setCurrentFloor(levelNumber);
            data.loadMap(levelNumber);
        } else {
            data.getLevels().set(data.getCurrentFloor() - 1, data.getCurrentMap());
            data.setCurrentMap(data.getLevels().get(levelNumber - 1));
            data.setCurrentFloor(levelNumber);
        }
        if (data.getCurrentFloor() < 10) {
            inPlay.getFloor().setText(Message.FLOOR + 0 + data.getCurrentFloor());
        } else {
            inPlay.getFloor().setText(Message.FLOOR + data.getCurrentFloor());
        }
        if (data.getCurrentFloor() > data.getMaxFloor()) {
            data.setMaxFloor(data.getCurrentFloor());
        }
        SoundEffect.getInstance().playSound(SoundEffect.Sounds.STAIRS);
        int newZone = Const.zone(data.getCurrentFloor());
        if (zone != newZone) {
            inPlay.useStairs(true);
            BackgroundMusic.getInstance().initializeSong(newZone);
        } else {
            inPlay.useStairs(false);
        }
    }

    public Type getType() {
        return type;
    }
}
