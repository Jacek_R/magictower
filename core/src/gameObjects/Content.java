package gameObjects;

import globalVariables.Const;
import globalVariables.PlayerData;
import userInterface.gameScreens.InPlay;


public class Content {

    protected String image;
    protected AtlasType atlasType;
    protected PlayerData data;
    protected int indexInAtlas;

    public enum AtlasType {EMPTY, MONSTERS, OBSTACLES, DOORS, ITEMS, WALLS, TRAPS, POTIONS, KEYS}

    public Content() {
        createPlayerDataReference();
        atlasType = AtlasType.EMPTY;
        image = "empty";
        indexInAtlas = -1;
    }

    public boolean interact(InPlay inPlay) {
        return false;
    }

    protected int addStat(int stat, int value) {
        int amount;
        int newAmount;
        amount = data.getStats().get(stat);
        newAmount = amount + value * Const.zone(data.getCurrentFloor());
        data.getStats().set(stat, newAmount);
        return newAmount;
    }

    protected int inflictDebuff(int debuff, int value) {
        int currentDebuffDuration;
        int newDebuffDuration;

        currentDebuffDuration = data.getBuffs().get(debuff);
        newDebuffDuration = value;

        if (newDebuffDuration < currentDebuffDuration) {
            data.getBuffs().set(debuff, currentDebuffDuration);
            return currentDebuffDuration;
        } else {
            data.getBuffs().set(debuff, newDebuffDuration);
            return newDebuffDuration;
        }
    }

    private void createPlayerDataReference() {
        data = PlayerData.getInstance();
    }

    public String getImage() {
        return image;
    }

    public AtlasType getAtlasType() {
        return atlasType;
    }

    public int getIndexInAtlas() {
        return indexInAtlas;
    }
}
