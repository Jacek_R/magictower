package gameObjects.actors;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import gameObjects.Content;
import gamePlay.Buy;
import globalVariables.Const;
import globalVariables.Message;
import graphic.AtlasManager;
import music.SoundEffect;
import userInterface.gameScreens.InPlay;

public class Npc extends Content {

    private Type type;
    private String message;
    private int price;
    private int amount;
    private String itemToSell;
    private int x;
    private int y;

    public Npc() {
    }

    public Npc(Type type, String message) {
        this.type = type;
        this.message = message;
        selectImage(type);
    }

    public Npc(Type type, String message, int price, int amount, String itemToSell, int x, int y) {
        this.type = type;
        this.message = message;
        this.price = price;
        this.amount = amount;
        this.itemToSell = itemToSell;
        this.x = x;
        this.y = y;
        selectImage(type);
    }

    public enum Type {
        MERCHANT, ELDER
    }

    private void selectImage(Type type) {
        atlasType = AtlasType.OBSTACLES;
        switch (type) {
            case MERCHANT:
                image = "merchant";
                break;
            case ELDER:
                image = "elder";
                break;
        }
    }

    private int checkWhatIsSelled(String nameKey) {
        int number = -1;
        if (nameKey.equals("bronze")) {
            number = 1;
        } else if (nameKey.equals("silver")) {
            number = 2;
        } else if (nameKey.equals("gold")) {
            number = 3;
        } else if (nameKey.equals("red")) {
            number = 4;
        } else if (nameKey.equals("attack")) {
            number = 5;
        } else if (nameKey.equals("defense")) {
            number = 6;
        } else if (nameKey.equals("health")) {
            number = 7;
        } else if (nameKey.equals("mana")) {
            number = 8;
        } else if (nameKey.equals("experience")) {
            number = 9;
        } else if (nameKey.equals("bomb")) {
            number = 10;
        } else if (nameKey.equals("key")) {
            number = 11;
        } else if (nameKey.equals("potion")) {
            number = 12;
        } else if (nameKey.equals("gold")) {
            number = 13;
        }
        return number;
    }

    @Override
    public boolean interact(final InPlay inPlay) {
        final int zone = Const.zone(data.getCurrentFloor());
        final Dialog dialog = new Dialog("", AtlasManager.getInstance().getDialogSkinsLarge().get(zone - 1)) {
            public void result(Object object) {
                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                if (object.equals(true)) {
                    if (data.getStats().get(Const.GOLD) >= price) {
                        Buy buy = new Buy();
                        buy.item(checkWhatIsSelled(itemToSell), amount, price, inPlay);
                        data.getCurrentMap()[x][y] = new Content();
                        SoundEffect.getInstance().playSound(SoundEffect.Sounds.BUY);
                        SpriteDrawable drawable = new SpriteDrawable(new Sprite(new Texture("empty.png")));
                        inPlay.getDraw().getImages()[x][y].setDrawable(drawable);
                    } else {
                        Dialog dial = new Dialog("", AtlasManager.getInstance().getDialogSkinsLarge().get(zone - 1)) {
                            @Override
                            protected void result(Object object) {
                                SoundEffect.getInstance().playSound(SoundEffect.Sounds.CLICK);
                            }
                        };
                        dial.text(Message.DIALOG_NO_GOLD);
                        dial.button(Message.DIALOG_CONFIRM, false);
                        dial.show(inPlay.getStageForDialog());
                    }
                }
            }
        };

        switch (type) {
            case MERCHANT:
                dialog.text(message);
                dialog.button(Message.DIALOG_BUY, true);
                dialog.button(Message.DIALOG_CANCEL, false);
                dialog.key(Input.Keys.SPACE, false);
                dialog.key(Input.Keys.ENTER, false);
                dialog.show(inPlay.getStageForDialog());
                break;
            case ELDER:
                dialog.text(message);
                dialog.button(Message.DIALOG_CONFIRM, false);
                dialog.key(Input.Keys.SPACE, false);
                dialog.key(Input.Keys.ENTER, false);
                dialog.show(inPlay.getStageForDialog());
                break;
        }
        return false;
    }

    public Type getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public int getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public String getItemToSell() {
        return itemToSell;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
