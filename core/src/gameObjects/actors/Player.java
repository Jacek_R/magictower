package gameObjects.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;

public class Player {

    private static final String ATLAS_NAME = "atlasPlayerAnimations.pack";

    private int animation;
    private ArrayList<Animation> animations;
    private float time;
    private TextureRegion frame;
    private Direction dir;
    private boolean changeAnimation;
    private boolean attacking;
    private boolean dying;
    private boolean animate;

    public enum Direction {LEFT, RIGHT, UP, DOWN}

    public Player() {
        dir = Direction.DOWN;
        changeAnimation = false;
        attacking = false;
        dying = false;
        animate = true;
        animation = /*createAnimation();*/0;
        animations = new ArrayList<Animation>();

        animations.add(setAnimation("moveLeft"));
        animations.add(setAnimation("moveRight"));
        animations.add(setAnimation("moveUp"));
        animations.add(setAnimation("moveDown"));
        animations.add(setAnimation("attackLeft"));
        animations.add(setAnimation("attackRight"));
        animations.add(setAnimation("attackUp"));
        animations.add(setAnimation("attackDown"));
        animations.add(setAnimation("death"));
    }

    public void resetPlayer() {
        dir = Direction.DOWN;
        changeAnimation = false;
        attacking = false;
        dying = false;
        animate = true;
        animation = /*createAnimation();*/ 0;
    }

    public void update(float dt) {
        time += dt;
        if (animate) {
            frame = animations.get(animation).getKeyFrame(time);
        }
        if (changeAnimation) {
            time = 0f;
            animation = chooseAnimation();
            changeAnimation = false;
        }
        if (animations.get(animation).isAnimationFinished(time)) {
            if (attacking) {
                attacking = false;
                animation = chooseAnimation();
            }
            if (dying) {
                frame = animations.get(animation).getKeyFrames()[5];
                animate = false;
            }
            time = 0f;
        }
    }

    private int chooseAnimation() {
        int animationName = -1;
        if (!attacking && !dying) {
            switch (dir) {
                case LEFT:
                    animationName = 0;
                    break;
                case RIGHT:
                    animationName = 1;
                    break;
                case UP:
                    animationName = 2;
                    break;
                case DOWN:
                    animationName = 3;
                    break;
            }
        } else if (attacking && !dying) {
            switch (dir) {
                case LEFT:
                    animationName = 4;
                    break;
                case RIGHT:
                    animationName = 5;
                    break;
                case UP:
                    animationName = 6;
                    break;
                case DOWN:
                    animationName = 7;
                    break;
            }
        } else {
            animationName = 8;
        }
        return animationName;
//        Array<TextureAtlas.AtlasRegion> regions = new TextureAtlas(ATLAS_NAME).findRegions(animationName);
//        return new Animation(0.1f, regions, Animation.PlayMode.LOOP);
    }

    private Animation setAnimation(String animationName) {
        Array<TextureAtlas.AtlasRegion> regions = new TextureAtlas(ATLAS_NAME).findRegions(animationName);
        return new Animation(0.1f, regions, Animation.PlayMode.LOOP);
    }

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction dir) {
        this.dir = dir;
    }

    public void setChangeAnimation(boolean changeAnimation) {
        this.changeAnimation = changeAnimation;
    }

    public TextureRegion getFrame() {
        return frame;
    }

    public void setAttacking(boolean attacking) {
        this.attacking = attacking;
    }

    public void setDying(boolean dying) {
        this.dying = dying;
    }
}
