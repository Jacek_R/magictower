package gameObjects.actors;

import gameObjects.Content;
import globalVariables.Const;
import userInterface.gameScreens.InPlay;

public class Monster extends Content {

    private Type type;
    private int attack;
    private int defense;
    private int health;
    private int experience;
    private int gold;
    private boolean firstStrike;
    private boolean endure;
    private float penetration;
    private int regeneration;
    private int reflect;
    private int manaDrain;
    private boolean bombResistant;

    public Monster() {
    }

    public Monster(Type type) {
        this.type = type;
        createMonster(type);
    }

    public enum Type {
        SLIME_GREEN, SLIME_RED, BAT, PRIEST, GOLEM, SKELETON, SKELETON_WARRIOR, SKELETON_BOSS,
        SNAKE_BLACK, BEE, MOSQUITO, GNOLL, SPIDER, OGRE, TROLL, DRAGON_GREEN, SKELETON_DOG,
        SKELETON_FISH, SKELETON_LARGE, SKELETON_SNAKE, LICH, SKELETON_HYDRA, LICH_POWER,
        SKELETON_DRAGON, IMP, SNAKE_RED, EFREET, DRAGON_RED, SALAMANDER, BALRUG, FIEND, SPHINX,
        FLESH_MASS, ABOMINATION, EYE, ABOMINATION_LARGE, EYE_RED, EYES_ORB, GUARDIAN, REAPER
    }

    private void createMonster(Type type) {
        atlasType = AtlasType.MONSTERS;
        switch (type) {
            case SLIME_GREEN:
                image = "greenSlime";
                createStats(18, 1, 35, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SLIME_RED:
                image = "slimeRed";
                createStats(20, 2, 45, 1, 2);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case BAT:
                image = "bat";
                createStats(38, 3, 35, 2, 3);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case PRIEST:
                image = "priest";
                createStats(32, 8, 60, 3, 5);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case GOLEM:
                image = "mudGolem";
                createStats(48, 22, 50, 7, 12);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON:
                image = "skeleton";
                createStats(42, 6, 50, 4, 6);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_WARRIOR:
                image = "skeletonWarrior";
                createStats(52, 12, 55, 5, 8);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_BOSS:
                image = "skeletonBoss";
                createStats(65, 15, 100, 18, 30);
                createSkills(false, false, 0f, 0, 0, 0, true);
                break;
            case SNAKE_BLACK:
                image = "blackSnake";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case BEE:
                image = "bee";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case MOSQUITO:
                image = "mosquito";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case GNOLL:
                image = "gnoll";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SPIDER:
                image = "spider";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case OGRE:
                image = "ogre";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case TROLL:
                image = "troll";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case DRAGON_GREEN:
                image = "greenDragon";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_DOG:
                image = "skeletonDog";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_FISH:
                image = "skeletonFish";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_LARGE:
                image = "skeletonLarge";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_SNAKE:
                image = "skeletonSnake";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case LICH:
                image = "lich";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_HYDRA:
                image = "skeletonHydra";
                createStats(100, 130, 800, 60, 45);
                createSkills(false, false, 10f, 50, 10, 0, false);
                break;
            case LICH_POWER:
                image = "powerLich";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SKELETON_DRAGON:
                image = "skeletonDragon";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case IMP:
                image = "imp";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SNAKE_RED:
                image = "snakeLava";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case EFREET:
                image = "efreet";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case DRAGON_RED:
                image = "dragonFire";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SALAMANDER:
                image = "salamander";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case BALRUG:
                image = "balrug";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case FIEND:
                image = "fiend";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case SPHINX:
                image = "sphinx";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case FLESH_MASS:
                image = "livingFlesh";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case ABOMINATION:
                image = "abominationSmall";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case EYE:
                image = "eye";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case ABOMINATION_LARGE:
                image = "abomination";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case EYE_RED:
                image = "eyeRed";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case EYES_ORB:
                image = "eyesOrb";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case GUARDIAN:
                image = "guardian";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
            case REAPER:
                image = "reaper";
                createStats(1, 1, 1, 1, 1);
                createSkills(false, false, 0f, 0, 0, 0, false);
                break;
        }
    }

    private void createStats(int atk, int def, int hp, int exp, int gld) {
        attack = atk;
        defense = def;
        health = hp;
        experience = exp;
        gold = gld;
    }

    private void createSkills(boolean firstStrike, boolean endure, float penetration, int regeneration, int reflect, int manaDrain, boolean bombResistant) {
        this.firstStrike = firstStrike;
        this.endure = endure;
        this.penetration = penetration;
        this.regeneration = regeneration;
        this.reflect = reflect;
        this.manaDrain = manaDrain;
        this.bombResistant = bombResistant;
    }

    @Override
    public boolean interact(InPlay inPlay) {
        boolean removeMonster = inPlay.getFight().battle(this, inPlay);
        inPlay.getHealth().setText(String.valueOf(data.getStats().get(Const.HEALTH)));
        inPlay.getGold().setText(String.valueOf(data.getStats().get(Const.GOLD)));
        inPlay.getExperience().setText(String.valueOf(data.getStats().get(Const.EXP)));
        return removeMonster;
    }

    public Type getType() {
        return type;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public int getHealth() {
        return health;
    }

    public int getExperience() {
        return experience;
    }

    public int getGold() {
        return gold;
    }

    public boolean isFirstStrike() {
        return firstStrike;
    }

    public boolean isEndure() {
        return endure;
    }

    public boolean isBombResistant() {
        return bombResistant;
    }

    public float getPenetration() {
        return penetration;
    }

    public int getRegeneration() {
        return regeneration;
    }

    public int getReflect() {
        return reflect;
    }

    public int getManaDrain() {
        return manaDrain;
    }
}
